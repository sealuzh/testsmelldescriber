\section{Introduction}
\label{sec:introduction}

Continuous Delivery (CD) is an agile software development practice where code changes are
automatically built and tested on dedicated server machines and finally released to production in
short release cycles  \cite{Humble:2010}. In this context, the complexity of modern
software systems is growing fast and software developers need to continuously update their source
code \cite{Lehman1980b}. Hence,  continuous software changes occur under time pressure and this often
results in the introduction of the so-called technical debt \cite{Cunningham93}, with  negative consequences on  maintenance and evolution tasks.

With the \textit{Agile} movement, software testing not only plays an important role in ensuring the quality of  large  software systems (e.g., detecting software defects  in different target environments \cite{fowler2006continuous}),  it also highly influences  the effectiveness of the whole software development process \cite{HiltonFSE17,HiltonTHMD16} and the efficiency of the (instantiated) continuous delivery pipeline \cite{Duvall2011}.  
%,Vassallo:2016}.   
As consequence, a relevant part of developers' time and effort goes into the production  and maintenance of test suites, accounting up to 50\% of the whole software development costs \cite{Brooks1975,DBLP:series/springer/MoonenDZB08}. 
%To ensure a high  software quality an overall  high quality of test suites is required \cite{Meszaros10,KhanLJA18,PerezMN17,HeimdahlG04}. 
However, writing and maintaining high quality test suites is perceived by developers as a very time consuming and error-prone task, which in practice often lead to
% Code smells indicate areas in the code which are potential causes of bugs and errors as they are 
\textit{``poor design and implementation choices"} \cite{tufano2}. 
%the introduction of the so called test smells \cite{}. 
 In addition, 
 %the application to 
 rapid release schedules lead to even less time for software testing by developers, thus complicating the software quality assurance task and narrowing  the  testing scope \cite{Mantyla2015}.  

Automated test generation tools have been widely researched with the goal of reducing the cost of testing activities \cite{FraEMSE:2015,Panichella2015, ricca, tonella}, to elevate the quality of available test suites \cite{RafiMPM12}
%. The use of such tools provide important advantages to developers as they help to reduce the time and cost of the testing process \cite{Cadar:2006,FraEMSE:2015,Panichella2015, ricca, tonella}, 
and to find violations of automated oracles \cite{PanichellaPBZG16,jcrasher, FraEMSE:2015, meyer, pacheco}. 
%In addition, automatically generated test suites have shown to increase developers confidence in the quality of the system, improving the overall product quality \cite{rafi}.  
However, the quality of both manual and generated tests are affected by the presence of the so-called test smells \cite{Deur01a,GarousiK18,BavotaQOLB15} which, as reported by recent empirical studies, substantially affect  the maintainability of test code \cite{PalombaNPOL16} and the effectiveness of software quality assurance tasks \cite{PanichellaPBZG16,BavotaQOLB15}. For this reason, researchers developed automated tools to detect these smells (e.g., \cite{TsantalisC09b})  during continuous integration and delivery processes \cite{BakotaHSLF14,SzokeNFFG15}. 

Despite the availability of these automated solutions, few tools have been proposed to effectively support developers during the \textit{identification and removal} of test smells \cite{GreilerDS13,KhomhPG99a}.  We argue that, while evolving software systems, developers would benefit from tools that help them evolve/update test suites (e.g., to sync them with the new version of the system) or repairing them by removing eventual test and code smells from production and test code \cite{Deur01a,TsantalisC09b,BavotaQOLB15}.  We believe that particularly important for this task is to provide to developers the right amount of information about the \textit{location of "smells"} \cite{Fowler02}, for which refactoring activities are desirable. 
  
  This paper builds on the finding that comprehensibility and maintainability of test cases is a key factor to optimize in the context of both automated and manual generated tests \cite{DakaRF17,Ermira2015,Deur01a,PalombaNPOL16,PanichellaPBZG16,BavotaQOLB15}. We believe that developers would benefit from an approach that helps them to enhance and complement available test cases, by highlighting possible regions of the code where bugs are present or bad design decisions are applied. 

\textbf{Paper contribution. } To handle this problem, this paper proposes an approach, coined \textit{TestSmellDescriber},  which is designed to automatically generate, for each individual test, test case summaries of the portion of code  that is affected by structural \cite{TsantalisC09b,Deur01a,BavotaQOLB15} and textual  \cite{PalombaPLOZ16}  tests smells, thereby enhancing developers' awareness on test suites quality.  These summaries along with methods recommending the required refactoring operations are directly introduced as comments in the test code. This design enables developers to improve their code.
%The results of the smell detection will then be used to generate descriptions detailing the characteristic of the smell, along with smell information in regards to the whole project. In addition to the smell descriptions a textual description will be displayed denoting how the code can be improved, \ie refactorings. These descriptions will be displayed to the user using the \emph{TestDescriber} method to augment information as comments into the source code. The injection of the description at the direct cause of the smell will bring awareness on the quality of tests and will enable developers to improve their code.
%This tool generates descriptions of the code which is exercised by the test cases, while also delivering information related to the coverage each case reached the code. 

We argue that this approach can complement the current techniques around  test smells analysis and detection \cite{Deur01a,PalombaPLOZ16,TsantalisC09b}. 
In particular, we believe that combining test/code smells analysis \cite{TsantalisC09b,Deur01a,PalombaPLOZ16}  and summarization techniques \cite{MorenoM17}  does not only help
developers to have a better awareness of test suites quality, but it can 
also be highly beneficial to support developers improving  test suites maintainability. 
This leads us to the first research question:

%\begin{quote}
%\textbf{RQ1:} \textit{How to efficiently integrate and present test smells information in test case summaries?}
%\end{quote}
%
%Current tools for test smells analysis are  not immediately consumable by developers as they generate as output metrics that are often not easy to interpret or to leverage to perform test suites refactoring operations. Hence, developers have to manually check the assertions to correctness and possibly modify or add new tests to improve overall test suite quality. 
%We derive three further sub-research questions from RQ$_1$ and the proposed questions by Moreno et al.
%
%RQ1-a: What information to include in the Test Smell summaries?
%
%RQ1-b: How much information to include in the Test Smell summaries?
%
%RQ1-c: How to generate Test Smell summaries and present them in the Test Case summaries?
%
%
%


\begin{quote}
\textbf{RQ$_1$:} \textit{Are test case summaries enriched by test smell information considered useful by developers?}
\end{quote}

Current tools for test smells analysis are  not immediately consumable or actionable by developers as they generate as output metrics that are often not easy to interpret or to leverage to perform the required refactoring operations.
%test suites
 Hence, developers have to manually check the assertions to verify their correctness and possibly modify or add new tests to improve the overall test suite quality.  We believe that our approach has the potential to impact developers' ability to 
 %improve test suite quality during 
 perform maintenance and evolution tasks. This leads us to our second research question: 

%RQ1-a: To what extent do test case summaries help developers to better understand test case quality?
%
%RQ1-b: Are the test case summaries adequate, concise and expressive?
%
%RQ1-c: How do test case summaries impact the developers perception of test cases?


\begin{quote}
\textbf{RQ$_2$:} \textit{How do test smell summaries impact developers' ability to improve test suite quality?}
\end{quote}

The contributions of our paper are summarized as follows:
\begin{itemize}
%\vspace{-2.5mm}
\item We introduce \textit{TestSmellDescriber}, a novel approach to automatically generate, for each individual test, natural language summaries of the portion of code  that is affected by structural and textual smells, to enhance developers' awareness on test suites quality.
%\vspace{-2.5mm}
\item We report on an empirical study we conducted involving 21 participants from  industry and academia to investigate the impact of provided test smell summaries on (i) developers ability to improve/evolve test suites and the (ii) developer perception of test suites quality.

%\vspace{-2.5mm}
\item We make publicly available a replication package\footnote{ \fontsize{6}{6}\selectfont \url{https://github.com/spanichella/TestSmellDescriber-ReplicationPackage}}\footnote{ \fontsize{6}{6}\selectfont \url{https://doi.org/10.5281/zenodo.1251242}}  with (i) material and working data sets of our study, (ii) complete results of the survey; and (iii) the leveraged raw-data for replication purposes.
\item We make publicly available  (in our replication package) also the TestSmellDescriber research prototype  for replication purposes. Instruction on how to install and run the tool is provided together with a working example of data to run it.
%\vspace{-1mm}
\end{itemize}


%In evaluating our approach we found that (1) developers find twice as many test smells, and (2) test smell summaries significantly improve developers' awareness on test suites quality, which is considered particularly useful by developers.
 


%In recent work \cite{Panichella:2016} \emph{TestDescriber} has been developed, a tool that helps developers to better understand automatically generated test cases. The tool generates summaries of the code which is exercised by the test cases, while also delivering information regarding the coverage each case reaches in the code. The tool helps developers to better detect and find bugs, and significantly improves the comprehensibility, thereby leverages the usability of automatically generated test cases for the developer.\\
%Due to the fact that developers still prefer manually testing their code over automatic testing methods \cite{Panichella:2016}, we want to complement and improve automatically as well as manually written test cases, by indicating possible areas of bugs in the test code.
%We are looking to achieve this by detecting test and code smells in the test cases under investigation.
%Code smells indicate areas in the code which are potential causes of bugs and errors as they are "symptoms of poor design and implementation choices" \cite{tufano2}. Code smells can also potentially lead to an increase in change- and fault-proneness, and to a decrease of software understandability or maintainability \cite{yann2}. However, bad smells can also be specific to test suites and test cases. So have van Deursen \etal created a catalog of test smells in \cite{Deur01a} and a collection of test refactorings to remove those smells. If your test runs fine while only one person is testing the code, but fails if more people run them, the test code smells and is affected by \textit{Test Run War}. The cause of that failure is most likely caused by interference of resources that are used by others \cite{Deur01a}. When a test requires external resources to run, it smells. A \textit{Mystery Guest} leads to tests no longer being self contained, while it also renders the documentational use of a test code useless and introduces hidden dependencies \cite{Deur01a}. \\
%The detection of those and other test smells enables the developer to asses and leverage the quality of test cases.
%To detect smells in the code we will use tools with the ability to automatically detect smells by conducting a statical analysis.
%The results of the smell detection will then be used to generate descriptions detailing the characteristic of the smell, along with smell information in regards to the whole project.
%In addition to the smell descriptions a textual description will be displayed denoting how the code can be improved, \ie refactorings.
%These descriptions will be displayed to the user using the \emph{TestDescriber} method to augment information as comments into the source code.
%The injection of the description at the direct cause of the smell will bring awareness on the quality of tests and will enable developers to improve their code.

%Moreover, several code smell detectors have been proposed
%[21], [22]: the detectors mainly differ in the underlying
%algorithm (e.g., metric-based [23]-[26] vs search-based techniques [27], [28]) and for the specific metric types considered
%(e.g., product metrics [23], [24] vs process metrics [26]).
%Despite the good performance shown by the detectors, recent
%studies highlight a number of important limitations threatening
%adoption of the detectors in practice [21], [29]. In the first
%place, code smells detected by existing approaches can be
%subjectively perceived and interpreted by developers [30], [31].
%Secondly, the agreement between the detectors is low [32],
%meaning that different tools can identify the smelliness of
%different code elements. Last, but not least, most of the current
%detectors require the specification of thresholds that allow
%them to distinguish smelly and non-smelly instances [21]: as a
%consequence, the selection of thresholds strongly influence the
%detectors' performance.


