% !TEX root = main.tex
The goal of this step is to provide to the software developer a higher-level view of which portion of the CUT each test case is going to test. To generate this view,  TestDescriber extracts natural language phrases from the underlying covered statements by implementing the well known Software Word Usage Model (SWUM) proposed by Hill {\em et al.}~\cite{hill:icse2009}. The basic idea of SWUM is that \textit{actions}, \textit{themes}, and any \textit{secondary arguments} can be derived from an arbitrary portion of code by making assumptions about different Java naming conventions, and using these assumptions to link linguistic information to programming language structure and semantics. Indeed, method signatures (including class name, method name, type, and formal parameters) and field signatures (including class name, type, and field name) usually contain \textit{verbs}, \textit{nouns}, and \textit{prepositional phrases} that can be expanded in order to generate readable natural language sentences. For example, \textit{verbs} in method names are considered by SWUM as the \textit{actions} while the \textit{theme} (i.e., subjects and objects) can be found in the rest of the name, the formal parameters, and then the class name.

%\vspace{-3mm}
\textbf{Pre-processing}. Before identifying the linguistic elements composing the covered statements of the CUT, we split the identifier names into component terms using the Java camel case convention~\cite{hill:icse2009, Sridhara:icpc2010}, which splits words based on capital letters, underscores, and numbers. Then, we expand abbreviations in identifiers and type names using  both (i) an external dictionary of common short forms for English words~\cite{Runeson:icse2007} and (ii) a more sophisticated technique called \textit{contextual-based expansion}~\cite{Hill:msr2008}, that searches the most appropriate expansion for a given abbreviation (contained in class and method identifiers).

%that searches for the most appropriate expansion for a given abbreviation over different parts of class and method body text (contextual-based expansion).

%\vspace{-3mm}
\textbf{Part-of-speech tagging}. Once the main terms are extracted from the identifier names, TestDescriber uses \textit{LanguageTool}~\footnote{ \fontsize{7}{7}\selectfont \url{https://github.com/languagetool-org/languagetool}}, a Part-of-speech (POS) tagger to derive which terms are \textit{verbs} (actions), \textit{nouns} (themes) and \textit{adjectives}. Specifically, \textit{LanguageTool} is an open-source Java library that provides a plethora of linguistic tools (e.g., spell checker, POS tagger, translator, etc.) for more than 20 different languages. The output of the POS tagging is then used to determine whether the names (of method or attribute) should be treated as Noun Phrases (NP),  Verb Phrases (VP), and Prepositional Phrases (PP)~\cite{hill:icse2009}. According to the type of phrase, we used a set of heuristics similar to the ones used by Hill {\em et al.}~\cite{hill:icse2009} and \textit{Sridhara} {\em et al.}~\cite{Sridhara:icpc2010} to generate natural language sentences using the pre-processed and POS tagged variables, attributes and signature methods.
%\andy{You say the technique is similar? How is it different then?}

%\vspace{-3mm}
\textbf{Summary Generation}. Starting from the noun, verb and prepositional phrases, TestDescriber applies a template-based strategy~\cite{Sridhara:icpc2010,McBurney:icpc2014} to generate summaries. This strategy consists of using pre-defined templates of natural language sentences that are filled with the output of SWUM, i.e., the pre-processed and tagged source code elements in covered statements. TestDescriber creates three different types of summaries at different levels of abstractions:  (i)
a general description of the CUT, which is generated during a specific sub-step of the Summary Generation called \textit{Class Level  Summarization}; (ii) a brief summary of the structural code coverage scores achieved by each individual JUnit test method; (iii) a fine grained description of the statement composing each JUnit test method in order to describe the flow of operations performed to test the CUT. These fine-grained descriptions are generated during two different sub-steps of the Summary Generation: the \textit{Fine-grained Statements Summarization} and the \textit{Branch Covered Summarization}. The first sub-step provides a summary for the statements in the JUnit test methods, while the latter describes the if-statements traversed in the executed path of the CUT. 

%\vspace{-3mm}
\textbf{Class Level  Summarization}. The focus of this step is to give to a tester a quick idea of the responsibility of the class under test. The generated summary is especially useful when the class under test is not well commented/documented.  To this end we implemented an approach similar to the one proposed by Moreno {\em et al.} in~\cite{MorenoIcpc2013} for summarizing Java classes. Specifically, Moreno {\em et al.} defined a heuristics based approach for describing the class behavior based on the most relevant methods, the superclass and class interfaces, and the role of the class within the system. Differently, during the \textit{Class Level Summarization} we focus on the single CUT by considering only its interface and its attributes, while a more detailed description of its methods and its behaviour is constructed later during the sub-step \textit{Fine-grained Statements Summarization}. Specifically, during this sub-step are considered only the lines \textit{executed by each test case} using the coverage information as base data to describe the CUT behavior. %Specifically, our the class level summary is obtained by filling a natural language template using the pre-processed and POS tagged terms extracted from the class name and from the declared class attributes. 
Figure~\ref{fig:example} shows an example of summary (in orange) generated during the \textit{Class Level Summarization} phase for the class \verb"Option.java". With this summary the developer has the possibility to have a quick understanding of the CUT without reading all of its lines of code. 

%\vspace{-3mm}
\textbf{Test Method  Summarization}. This step is responsible for generating a general description of the statement coverage scores achieved by each JUnit test method. %This description was conceived to give a quick view of what a test method is going to test.  We designed the initial version of this summarizer as a generator of summaries containing (i) information about the test method coverage, (ii) a detailed description of the assertions and instructions contained in the test method. The purpose of this description was to describe line by line what an instruction and an assertion test of  a given class. However, interviewing a group of four testers they suggested us to provide in the general description of a test method only the information related to the test method coverage. Then, they suggested to put the descriptions related to the instructions/assertions as comments in line in the test method. Basically, they believe that this way to present the summaries helps to improve the readability of a test case, as well as, reduce the risk to have a too large \textit{summary} of a test method.  In order to generate the in line comments in the test method we designed two specific summarizers: (i) \textit{Fine-grained Statements Descriptor}, (ii) \textit{Branch Covered Summarizer} (described in details in the next sections). Thus, the Test Method  Summarizer generates a natural language description about  the test method coverage, relying on a pre-defined \textit{template}. In this way, one sentence is generated for each test method. For example, a description of a test method is reported as {\em ``OVERVIEW: The test case $<$test method name$>$ covers around XX\% (low/high percentage) of statements in $<$class name$>$''}.  The following box shows an example of an automatically generated summary for the test method \textit{test0} which test some methods of the class {\em org.magee.math.Rational} considered in our study:
This description is extracted by leveraging the coverage information provided by Cobertura to fill a pre-defined \textit{template}. An example of summary generated by TestDescriber for describing the coverage score is depicted in Figure~\ref{fig:example} (in yellow): before each JUnit test method (\verb"test0" in the example) TestDescriber adds a comment regarding the percentage of statements covered by the given test method independently from all the other test methods in \verb"TestOption". This type of description allows to identify the contribution of each test method to the final structural coverage score. 
 In the future we plan to complement the statement coverage describing further coverage criteria (e.g. branch or mutation coverage).
%$For example, a description of a test method is reported as {\em ``OVERVIEW: The test case $<$test method name$>$ covers around XX\% (low/high percentage) of statements in $<$class name$>$''}.  The following box shows an example of an automatically generated summary for the test method \textit{test0} which test some methods of the class {\em org.magee.math.Rational} considered in our study:

%\fbox{\begin{minipage}{24.8em}
%\textit{
%	/** OVERVIEW: The test case "test0" covers around \\
%	 * 7.0\% (low percentage) of statements in "Rational"\\ 
%	 **/
%}
 %\end{minipage}}
%The computed code coverage is obtained, in the previous step, by Cobertura by computing the overlap of the line of code executed (LOCE) by a test case and the total lines of code of the original class and weighting this value with with the total lines of code of that class. Since Cobertura makes available  the coverage value only for the entire test suite, we used the output of Cobertura (i.e., lines of code executed) as input to a parser that computes the values of coverage for each test method. 



\begin{figure}[tb]
\centering
 \vspace{2pt}
 {
 \fontsize{6}{6}\selectfont
% \resizebox{0.5\textwidth}{!}{\begin{minipage}{\textwidth}
\begin{minipage}[c]{\linewidth}
%\scriptsize
\vspace{2pt}
\begin{Verbatim}[frame=lines, commandchars=\\\{\}]
 1| \textcolor{orange}{/** The main class under test is Option. It describes} \\
 2| \textcolor{orange}{ * a single option and maintains information regarding:} \\
 3| \textcolor{orange}{ *  - the option;}\\
 4| \textcolor{orange}{ *  - the long option;}\\
 5| \textcolor{orange}{ *  - the argument name;}\\ 
 6| \textcolor{orange}{ *  - the description of the option; } \textcolor{blue}{3.a}  \\ 
 7| \textcolor{orange}{ *  - whether it has required;}\\
 8| \textcolor{orange}{ *  - whether it has optional argument;}\\
 9| \textcolor{orange}{ *  - the number of arguments;}\\
10|\textcolor{orange}{  *  - the type, the values and the separator of the option;**/}
11| public class TestOption \{
12| \textcolor{yellow}{/** OVERVIEW: The test case "test0" covers around 3.0\%} \textcolor{blue}{3.b}
13| \textcolor{yellow}{ * (low percentage) of statements in "Option" **/}
14| @Test
15| public void test0() throws Throwable \{
16| \textcolor{green}{// The test case instantiates an "Option" with option} \textcolor{blue}{3.c}
17| \textcolor{green}{// equal to "", and description equal to "1W|^".}
18| Option option0 = new Option("", "1W|^");	
19| \textcolor{green}{// Then, it tests:}
20| \textcolor{green}{// 1)  whether the description of option0 is equal to} \textcolor{blue}{3.c}
21| \textcolor{green}{// "1W|^";}
22|  assertEquals("1W|^", option0.getDescription());
23| \textcolor{green}{// 2) whether the key of option0 is equal to "";} \textcolor{blue}{3.c}
24| \textcolor{red}{// The execution of the method call used in the assertion} \textcolor{blue}{3.d}
25| \textcolor{red}{// implicitly covers the following 1 conditions:}
26| \textcolor{red}{// - the condition "option equal to null" is FALSE;}
27|  assertEquals("", option0.getKey());
28|  \}
29| \}
\end{Verbatim}
 \end{minipage}
}
		      \vspace{-5mm}
\caption{Example of summary generated by TestDescriber for a JUnit test method exercising the class \textit{Options.java}}
\label{fig:example}
		      \vspace{-4mm}
\end{figure} 

\textbf{Fine-grained Statement Summarization}.  As describ-ed in Section \ref{sec:TScoverage}  TestDescriber extracts the fine-grained list of \textit{code elements} (e.g. methods, attributes, local variables) composing each statement of the CUT covered by each JUnit test method. This information is provided as input to the \textit{Fine-grained Statements Summarization} phase, thus, TestDescriber performs the following three steps: (i)   parses all the instructions contained in a test method; (ii) it uses the SWUM methodology for each instruction and determines which kind of operation the considered statement is performing (e.g. if it declares a variable, it uses a constructor/method of the class, it uses specific assertions etc.) and which part of the code is executed; and (iii) it generates a set of customized natural-language sentences depending on the selected kind of instructions. To perform the first two steps, it assigns each statement to one of the following categories:
\begin{itemize}
\vspace{-1mm}
\item \textit{Constructor of the class}. A constructor typically implies the instantiation of an object, which is the implicit \textit{action}/\textit{verb}, with some properties (parameters). In this case, our descriptor links the constructor call to its corresponding declaration in the CUT to map \textit{formal} and \textit{actual} parameters. Therefore, pre-processing and POS tagger are performed to identify the verb, noun phrase and adjectives from the constructor signature. These linguistic elements are then used to fill specific natural language templates for constructors. Figure~\ref{fig:example} contains an example of a summary generated to describe the constructor \verb"Option(String, String)", i.e., the lines 16 and 17 (highlighted in green).
\vspace{-2mm}
\item \textit{Method calls}. A method implements an operation and typically begins with a verb~\cite{hill:icse2009} which defines the main \textit{action} while the method caller and the parameters determine \textit{theme} and \textit{secondary arguments}. Again, the linguistic elements identified after pre-processing and POS tagging are used to fill natural language templates specific for method calls. More precisely, the summarizer is able to notice if the result of a method call is assigned as value to a local variable (assignment statement), thus, it adapts the description depending on the specific context. For particular methods, such as getters and setters, it uses ad-hoc templates that differ from the templates used for more general methods. 
\vspace{-2mm}
\item \textit{Assertion statements}. This step defines the test oracle and enables to test whether the CUT behaves as intended. In this case the name of an assertion  method (e.g. {\em assertEquals}, {\em assertFalse}, {\em notEquals} etc) defines the type of test, while the input parameters represent respectively (i) the expected and (ii) the actual behavior. Therefore, the template for an assertion statement is defined by the (pre-processed) assertion name itself and the value(s) passed (and verified) as parameter(s) to the assertion. Figure~\ref{fig:example} reports two examples of descriptions generated for assertion methods where one of the input parameters is a method call, e.g., , \verb"getKey()" (the summary is reported in line 23 and highlighted in green).
\vspace{-1mm}
\end{itemize}

\textbf{Branch Coverage Summarization}. When a test meth-od contains method/constructor calls, it is common that the test execution covers some if-conditions (branches) in the body of the called method/constructor. Thus, TestDescriber, after the \textit{Fine-grained Statements Summarization} step, enriches the standard method call description with a summary describing the Boolean expressions of the \verb"if" condition. %It also highlights the \textit{branch traversed} by the executed test specifying the boolean value ({\em TRUE} or {\em FALSE}) obtained by evaluating the condition.  
Therefore, during the \textit{Branch Coverage Summarization} step TestDescriber generates a natural language description for the tested \verb"if" condition. When an \verb"if" condition is composed of multiple Boolean expressions combined via Boolean operators, we generate natural language sentences for the individual expressions and combine them. Thus, during the \textit{Branch Coverage Summarization}, we  adapt the descriptions when an if-condition contains  calls to other methods of the CUT.  In the previous example reported in Figure~\ref{fig:example}, when executing the method call \verb"getKey()" (line 27) for the object \verb"option0", the test method \verb"test0" covers the false branch of the if-condition \verb"if (opt == null)", i.e., it verifies that \verb"option0" is not \verb"null".  In Figure~\ref{fig:example} the lines 24, 25 and 26, (highlighted in red) represent the summary generated during the  \textit{Branch Coverage Summarization} for the method call \verb"getKey()".