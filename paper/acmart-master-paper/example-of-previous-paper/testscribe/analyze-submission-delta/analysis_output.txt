
R version 3.1.2 (2014-10-31) -- "Pumpkin Helmet"
Copyright (C) 2014 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> # 
> # Author: mbeller
> ###############################################################################
> 
> 
> library(data.table)
data.table 1.9.4  For help type: ?data.table
*** NB: by=.EACHI is now explicit. See README to restore previous behaviour.
> library(ggplot2)
> library(ggthemes)
> library(effsize)
> library(plyr)
> 
> ### ggplot configuration
> ggplot.defaults <- 
+ 		#theme(axis.text.x = element_text(size = 15)) +
+ 		#theme(axis.text.y = element_text(size = 15)) +
+ 		theme_few() +
+ 		#theme(legend.key = element_blank()) +
+ 		#theme(axis.title.x = element_blank(), axis.title.y = element_blank())  +
+ 		theme(axis.text.x = element_text(size = 20)) + 
+ 		theme(axis.text.y = element_text(size = 20)) +
+ 		theme(axis.title.x = element_text(size = 18, vjust = 1)) +
+ 		theme(axis.title.y = element_text(size = 18, vjust = 1)) +
+ 		theme(legend.text = element_text(size = 16))  +
+ 		theme(legend.title = element_text(size = 18))
> ggplot.small.defaults <-
+ 		ggplot.defaults + theme(axis.title.x = element_text(size = 18, vjust = -0.5)) +
+ 		theme(axis.title.y = element_text(size = 18, hjust = 0.5, vjust = 1.5))
> 
> 
> plot.location <- function(filename) {
+ 	file.path("../figs", filename)
+ }
> 
> save.plot <- function(filename) {
+ 	ggsave(plot.location(filename))
+ }
> 
> save.pdf <- function(plot, filename, width = 7, height = 7) {
+ 	pdf(plot.location(filename), height = height, width = width)
+ 	print(p)
+ 	dev.off()
+ }
> 
> ### Visualize function
> 
> visualize_data <- function(dist1, dist2) {
+ 	boxplot(dist1$N..BUG, dist2$N..BUG)
+ 	boxplot(dist1$time_in_s, dist2$time_in_s)
+ }
> 
> 
> ### Actual data analysis
> 
> data <- data.table(read.csv('full_results.csv', numerals=c("no.loss")))
> 
> # Convert time to something sane
> # Note: myData$TIME_h is not actually hours, it is minutes! Similarly, myData$TIME_min is seconds, not minutes! 
> data$time_in_s <- data$TIME_h * 60 + data$TIME_m
> data$match <- data$Task == data$SummaryFor
> 
> 
> #### Basic statistics
> warnIfNormal <- function (shapiro) {
+ 	if (shapiro$p > 0.1) {
+ 		print ("Warning!!!!!! Shapiro says dist. is normal!")
+ 	}
+ }
> 
> ############################## Preamble
> # Shapiro Wilk on number of bugs
> warnIfNormal(shapiro.test(data$N..BUG))
> warnIfNormal(shapiro.test(data$Method))
> warnIfNormal(shapiro.test(data$Branch))
> warnIfNormal(shapiro.test(data$Statement))
> warnIfNormal(shapiro.test(data$Mutation))
> warnIfNormal(shapiro.test(data$diffTests))
> warnIfNormal(shapiro.test(data$diffAsserts))
> warnIfNormal(shapiro.test(data$editDist))
> warnIfNormal(shapiro.test(data$time_in_s))
> # RESULT:
> # non-normal -> use non-parametric tests
> 
> 
> summaries <- subset(data, match == T)
> withoutSummaries <- subset(data, match == F)
> ############################## RQ 1
> ### RQ1.1
> # 1 Compare bug-finding capabilities between participants
> data$match_str <- factor(data$match, levels=c(T, F), labels=c("With Summaries", "Without Summaries"))
> 
> p <- ggplot(data, aes(x=data$match_str, y=data$N..BUG)) + labs(y = "Found Bugs", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq11_summaries_boxplot.pdf")
null device 
          1 
> ## RESULT: wilcox test
> print("##################################################")
[1] "##################################################"
> print("RQ1.1")
[1] "RQ1.1"
> wilcox.test(summaries$N..BUG, withoutSummaries$N..BUG)

	Wilcoxon rank sum test with continuity correction

data:  summaries$N..BUG and withoutSummaries$N..BUG
W = 677, p-value = 0.0006138
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$N..BUG, withoutSummaries$N..BUG) :
  cannot compute exact p-value with ties
> VD.A(summaries$N..BUG, withoutSummaries$N..BUG)

Vargha and Delaney A

A estimate: 0.7522222 (large)
> 
> # 2 Compare bug-finding capabilities between tasks (directly to be used when speaking about 1)
> p <- ggplot(data, aes(x=data$Task, y=data$N..BUG)) + labs(y = "Found Bugs", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "threats_tasks_summaries.pdf")
null device 
          1 
> wilcox.test(rational$N..BUG, arrayIntList$N..BUG)
Error in wilcox.test(rational$N..BUG, arrayIntList$N..BUG) : 
  object 'rational' not found
> 
> 
> # 3 Compare bug-finding capabilities between programming experience (directly to be used when speaking about 1)
> p <- ggplot(data.frame(data), aes(x=data$Programming_exp, y=data$N..BUG)) + labs(y = "Found Bugs", x="Programming Experience") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq11_experience_summaries.pdf")
null device 
          1 
> print("##################################################")
[1] "##################################################"
> print("RQ1.1")
[1] "RQ1.1"
> wilcox.test(rational$N..BUG, arrayIntList$N..BUG)
Error in wilcox.test(rational$N..BUG, arrayIntList$N..BUG) : 
  object 'rational' not found
> # TODO: MMB do a pairwise correlation between the groups ; data[data$Programming_exp == "7-10 years"]$N..BUG
> 
> ### RQ1.2
> # 1 Compare time finishing capabilities between participants
> data$match_str <- factor(data$match, levels=c(T, F), labels=c("With Summaries", "Without Summaries"))
> 
> p <- ggplot(data, aes(x=data$match_str, y=data$time_in_s)) + labs(y = "Task Completion Time", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq12_time_summaries.pdf")
null device 
          1 
> ## RESULT: wilcox test
> print("##################################################")
[1] "##################################################"
> print("RQ1.2")
[1] "RQ1.2"
> wilcox.test(summaries$time_in_s, withoutSummaries$time_in_s)

	Wilcoxon rank sum test with continuity correction

data:  summaries$time_in_s and withoutSummaries$time_in_s
W = 338, p-value = 0.08697
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$time_in_s, withoutSummaries$time_in_s) :
  cannot compute exact p-value with ties
> VD.A(summaries$time_in_s, withoutSummaries$time_in_s)

Vargha and Delaney A

A estimate: 0.3755556 (small)
> 
> 
> ############################## RQ 2
> ### RQ2.1
> p <- ggplot(data, aes(x=data$match_str, y=data$Statement)) + labs(y = "Statement Coverage", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq21_statement_summaries.pdf")
null device 
          1 
> print("##################################################")
[1] "##################################################"
> print("RQ2.1")
[1] "RQ2.1"
> wilcox.test(summaries$Statement, withoutSummaries$Statement)

	Wilcoxon rank sum test with continuity correction

data:  summaries$Statement and withoutSummaries$Statement
W = 395, p-value = 0.4098
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$Statement, withoutSummaries$Statement) :
  cannot compute exact p-value with ties
> VD.A(summaries$Statement, withoutSummaries$Statement)

Vargha and Delaney A

A estimate: 0.4388889 (negligible)
> 
> 
> p <- ggplot(data, aes(x=data$match_str, y=data$Method)) + labs(y = "Method Coverage", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq21_method_summaries.pdf")
null device 
          1 
> wilcox.test(summaries$Method, withoutSummaries$Method)

	Wilcoxon rank sum test with continuity correction

data:  summaries$Method and withoutSummaries$Method
W = 379, p-value = 0.2877
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$Method, withoutSummaries$Method) :
  cannot compute exact p-value with ties
> VD.A(summaries$Method, withoutSummaries$Method)

Vargha and Delaney A

A estimate: 0.4211111 (small)
> 
> p <- ggplot(data, aes(x=data$match_str, y=data$Branch)) + labs(y = "Branch Coverage", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq21_branch_summaries.pdf")
null device 
          1 
> print("##################################################")
[1] "##################################################"
> print("RQ2.1")
[1] "RQ2.1"
> wilcox.test(summaries$Branch, withoutSummaries$Branch)

	Wilcoxon rank sum test with continuity correction

data:  summaries$Branch and withoutSummaries$Branch
W = 391.5, p-value = 0.3694
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$Branch, withoutSummaries$Branch) :
  cannot compute exact p-value with ties
> VD.A(summaries$Branch, withoutSummaries$Branch)

Vargha and Delaney A

A estimate: 0.435 (negligible)
> 
> 
> p <- ggplot(data, aes(x=data$match_str, y=data$Mutation)) + labs(y = "Mutation Score", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq21_mutation_summaries.pdf")
null device 
          1 
> print("##################################################")
[1] "##################################################"
> print("RQ2.1")
[1] "RQ2.1"
> wilcox.test(summaries$Mutation, withoutSummaries$Mutation)

	Wilcoxon rank sum test with continuity correction

data:  summaries$Mutation and withoutSummaries$Mutation
W = 427, p-value = 0.7393
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$Mutation, withoutSummaries$Mutation) :
  cannot compute exact p-value with ties
> VD.A(summaries$Mutation, withoutSummaries$Mutation)

Vargha and Delaney A

A estimate: 0.4744444 (negligible)
> 
> ### RQ2.2
> p <- ggplot(data, aes(x=data$match_str, y=data$diffTests)) + labs(y = "# of changed Tests", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq22_diff_tests.pdf")
null device 
          1 
> print("##################################################")
[1] "##################################################"
> print("RQ2.2 diffTests")
[1] "RQ2.2 diffTests"
> wilcox.test(summaries$diffTests, withoutSummaries$diffTests)

	Wilcoxon rank sum test with continuity correction

data:  summaries$diffTests and withoutSummaries$diffTests
W = 428, p-value = 0.7192
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$diffTests, withoutSummaries$diffTests) :
  cannot compute exact p-value with ties
> VD.A(summaries$diffTests, withoutSummaries$diffTests)

Vargha and Delaney A

A estimate: 0.4755556 (negligible)
> 
> p <- ggplot(data, aes(x=data$match_str, y=data$diffAsserts)) + labs(y = "# of changed Asserts", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq22_diff_asserts.pdf")
null device 
          1 
> print("##################################################")
[1] "##################################################"
> print("RQ2.2 diffAsserts")
[1] "RQ2.2 diffAsserts"
> wilcox.test(summaries$diffAsserts, withoutSummaries$diffAsserts)

	Wilcoxon rank sum test with continuity correction

data:  summaries$diffAsserts and withoutSummaries$diffAsserts
W = 436, p-value = 0.8363
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$diffAsserts, withoutSummaries$diffAsserts) :
  cannot compute exact p-value with ties
> VD.A(summaries$diffAsserts, withoutSummaries$diffAsserts)

Vargha and Delaney A

A estimate: 0.4844444 (negligible)
> 
> p <- ggplot(data, aes(x=data$match_str, y=data$editDist)) + labs(y = "Edit Distance Change", x="") + 
+ 		geom_boxplot(outlier.colour="red", outlier.size=4) + ggplot.defaults
> save.pdf(p, "rq22_edit_distance.pdf")
null device 
          1 
> print("##################################################")
[1] "##################################################"
> print("RQ2.2 editDistance")
[1] "RQ2.2 editDistance"
> wilcox.test(summaries$editDist, withoutSummaries$editDist)

	Wilcoxon rank sum test with continuity correction

data:  summaries$editDist and withoutSummaries$editDist
W = 459.5, p-value = 0.8941
alternative hypothesis: true location shift is not equal to 0

Warning message:
In wilcox.test.default(summaries$editDist, withoutSummaries$editDist) :
  cannot compute exact p-value with ties
> VD.A(summaries$editDist, withoutSummaries$editDist)

Vargha and Delaney A

A estimate: 0.5105556 (negligible)
> 
> 
> ## Threat
> 
> # ProgrammingExp
> programmingExp <- count(data, "Programming_exp")
> programmingExp$freq <- programmingExp$freq/2
> programmingExp$rel_freq <- programmingExp$freq/sum(programmingExp$freq)
> programmingExp
  Programming_exp freq   rel_freq
1       >10 years    1 0.03333333
2       1-2 years    1 0.03333333
3       3-6 years   20 0.66666667
4      7-10 years    8 0.26666667

