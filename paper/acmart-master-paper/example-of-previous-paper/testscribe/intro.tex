% !TEX root = main.tex

Software testing is a key activity of software development and software
quality assurance in particular. However, it is also expensive, with
overall testing consuming as much as 50\% of overall project
effort~\cite{Brooks1975,DBLP:series/springer/MoonenDZB08}, and programmers spending a
quarter of their work time on developer testing~\cite{beller2015developerstest}.

% %\vspace{-2mm}
Several search-based techniques and tools~\cite{Csallner:2004, Pacheco:2007, FraserA13} have been proposed to reduce the time developers need to spend on testing by automatically generating a (possibly minimal) set of test cases 
with respect to a specific test coverage criterion~\cite{Ricca:2001,Cavarra02usinguml,Tonella:2004,Harman:2010,Fraser:2010,Wang:2015, FraserA13, MOSA:2015}. These research efforts produced important results: automatic test case generation allows developers to
(i) reduce the time and cost of the testing
process~\cite{Cavarra02usinguml, Shengbo:2007, Wang:2015, Baresi:2010}; to (ii) achieve higher code coverage when compared to the coverage obtained through manual testing~\cite{Cadar:2006, MOSA:2015, FraEMSE:2015, Ricca:2001, Tonella:2004};
to (iii) find violations of automated oracles (e.g. undeclared
exceptions)~\cite{Csallner:2004,Leeuwen:2007,FraEMSE:2015,Pacheco:2007}.
 
% %\vspace{-2mm}
Despite these undisputed advances, creating test cases manually is still prevalent in software development. This is partially due to the fact that professional developers perceive generated test cases as hard to understand and  difficult to maintain~\cite{Rojas:2015,Ermira2015}. Indeed,  recent studies~\cite{Fraser:study2013, Fraser:study2014} reported that developers spend up to 50\% of their time in understanding and analyzing the output of automatic tools. %, examining each test for correctness. 
As a consequence, automatically generated tests \textit{do not improve the ability of developers to detect faults} when compared to manual testing~\cite{Fraser:study2013, Fraser:study2014,Ceccato:2015}.  
% %\vspace{-2mm}
Recent research has challenged the assumption that structural coverage is the only goal to optimize~\cite{Afshan:icst2013,Xuan:fse2014}, showing that when systematically improving the readability of the code composing the generated tests, developers tend to prefer the improved tests and were able to perform maintenance tasks in less time (about 14\%) and at the same level of accuracy~\cite{Ermira2015}. However, there is no empirical evidence that such readability improvements produce tangible results in terms of the number of bugs actually found by developers. 

 %%\vspace{-2mm}
This paper builds on the finding that readability of test cases is a key factor to optimize in the context of automated test generation. However, we conjecture that the quality of the code composing the generated test cases (e.g., input parameters,  assertions, etc.) is not the only factor affecting their comprehensibility. 
For example, consider the unit test \verb"test0"\footnote{ \fontsize{6}{6}The test case has been generated using Evosuite~
\cite{FraserA13}.} in Figure~\ref{fig:motivating}, which was automatically generated for the target class \verb"Option"\footnote{ \fontsize{7}{7}The class \textit{Option} has 
been extracted from the \textit{apache commons} library}. From a bird's-eye view, 
the code of the test is pretty short and simple: it contains a constructor and two 
assertions calling \verb"get" methods. However, it is difficult to tell, without reading the contents of the target class, (i) 
what is the behavior under test, (ii) whether the generated assertions are correct, (iii) which if-conditions are eventually 
traversed when executing the test (coverage).  Thus, we need a solution that helps developers to quickly understand both tests and code covered.
\begin{figure}[t]
\centering
 %\vspace{2pt}
 {
 \fontsize{7}{7}\selectfont
% \resizebox{0.5\textwidth}{!}{\begin{minipage}{\textwidth}
\begin{minipage}[c]{\linewidth}
%\scriptsize
%\vspace{2pt}
\begin{Verbatim}[frame=lines, commandchars=\\\{\}]
1| public class TestOption \{
2|
3| @Test
4| public void test0() throws Throwable \{
5|    Option option0 = new Option("", "1W|^");	
6|    assertEquals("1W|^", option0.getDescription());
7|    assertEquals("", option0.getKey());
8|  \}
9| \}
\end{Verbatim}
 \end{minipage}
}
\vspace{-5mm}
\caption{Motivating example}
\label{fig:motivating}
\vspace{-6mm}
\end{figure} 

%\vspace{-2mm}
\textbf{Paper contribution.} 
To handle this problem, our paper proposes an approach, coined TestDescriber, which is
designed to automatically generate summaries of the portion of code
exercised by each individual test case to provide a dynamic view of
the class under test (CUT). We argue that applying summarization techniques to test cases does not only help
developers to have a better understanding of the code under test, but it can 
also be highly beneficial to support developers during bug fixing tasks, improving their bug 
fixing performance. This leads us to the first research question:

\vspace{-2.5mm}
\begin{quote}
 \fontsize{9}{9}\selectfont
\textbf{RQ1:} \textit{How do test case summaries impact the number of bugs fixed by developers?}
\end{quote}
\vspace{-2.5mm}

Automatically generated tests are not immediately consumable since the assertions might reflect an incorrect behavior if the target class is faulty. Hence, developers should manually check the assertions for correctness and possibly add new tests if they think that some parts of the target classes are not tested. This leads us to our 
second research question: 

\vspace{-2.5mm}
\begin{quote}
 \fontsize{9}{9}\selectfont
\textbf{RQ2:} \textit{How do test case summaries impact developers to change test cases in terms of structural and mutation coverage?}
\end{quote}
\vspace{-2.5mm}

The contributions of our paper are summarized as follows:
\begin{itemize}
\vspace{-2.5mm}
\item we introduced \textit{TestDescriber} a novel approach to automatically generate natural language 
summaries of JUnit test cases and the portion of the target classes they are going to test; 
\vspace{-2.5mm}
\item we conducted an empirical study involving 30 human participants from both industry and academia to investigate the impact of test summaries on the number of bugs that can be fixed by developers when assisted by automated test generation tools;
\vspace{-2.5mm}
\item we make publicly available a replication package\footnote{ \fontsize{6}{6}\selectfont \url{http://dx.doi.org/10.5281/zenodo.45120}} with (i) material and working data sets of our study, (ii) complete results of the survey; and (iii) rawdata for replication purposes and to support future studies.
\vspace{-1mm}
\end{itemize}

%To  the best of our knowledge this is the first work that investigates the impact of \textit{test 
%case summaries} on a bug fixing scenario, which involves but is not limited to code comprehension. Differently, 
%most of previous studies on source code summarization have been evaluated by simply surveying human 
%participants about the quality of the provided summaries~\cite{Binkley2013, Sridharaphd:2012, 
%Sridhara:icpc2010,McBurney:icpc2014, HaiducAMM10, MorenoIcpc2013}.


%\textbf{Paper structure}:
%Section \ref{sec:TestDescriber} presents the approach we defined and
%techniques we used to generate human oriented summaries of JUnit test
%cases. Section \ref{sec:study} reports the dataset and the evaluation
%methods we employed. Section \ref{sec:results} presents and discusses
%the results of the study while Section \ref{sec:discussion} discuss
%the results of the qualitative analysis and the main lesson
%learned. Section \ref{sec:threats} deals with the threats that could
%affect the validity of our work.  Section \ref{sec:related}
%illustrates the related literature and Section \ref{sec:conclusion}
%concludes the paper outlining future research directions.

