% !TEX root = main.tex

In this section, we discuss the related literature on source code summarization and readability of test cases.

 %\vspace{-2mm}
\textbf{Source Code Summarization}. Murphy's dissertation \cite{Murphy:1996} is the earliest work which 
proposes an approach to generate  summaries by analysing structural information of the source code. 
More recently, Sridhara {\em et al.}~\cite{Sridharaphd:2012} suggested to use pre-defined \textit{templates} of natural language sentences that are filled with linguistic elements (verbs, nouns, etc.) extracted from important
method signature \cite{,LuciaPOPP14,LuciaPOPP12}. Other studies used the same strategy to summarize Java methods~\cite{Sridhara:icpc2010,McBurney:icpc2014, HaiducAMM10}, parameters~\cite{Sridhara:2011}, groups of 
statements~\cite{Sridhara2:2011}, Java classes~\cite{MorenoIcpc2013},
% TODO (MMB) what are services of Java packages?
services of 
Java packages~\cite{Hammad:2016} or generating commit messages \cite{Cortes:2014}. Other reported applications are the generation of 
source code documentation/summary by mining text data from other sources of information, such as bug reports~\cite{PanichellaAPMC12},  e-mails~\cite{PanichellaAPMC12}, forum posts~\cite{Dagenais:2012} and 
question and answer site (Q\&A) discussions~\cite{Vassallo:2014,Wong:2013}. 

 %\vspace{-2mm}
However, Binkley {\em et al.}~\cite{Binkley2013} and Jones {\em et al.}~
\cite{SparckJones:2007} pointed out that the evaluation of the generated summaries 
should not be done by just answering the general question \textit{``is this a good 
summary?''}  but evaluated  \textit{``through the lens of a particular task''}. 
Stemming from these considerations, in this paper we evaluated the impact of 
automatically generated test summaries in the context of two bug fixing tasks.  In 
contrast, most previous studies on source code summarization have been evaluated 
by simply surveying human participants about the quality of the provided summaries~
\cite{Binkley2013, Sridharaphd:2012,  Sridhara:icpc2010,McBurney:icpc2014, 
HaiducAMM10, MorenoIcpc2013}.

%\vspace{-3mm}
\textbf{Test Comprehension}. The problem of improving test understandability is well 
known in literature~\cite{CornelissenCSMR20Z07}, especially in the case of test 
failures~\cite{Zhang2001, Buse:2008}. For example, 
Zhang {\em et al.}~\cite{Zhang2001} focused on failing tests and proposed a 
technique based on static slicing to generate code comments describing the failure 
and its causes. Buse {\em et al.}~\cite{Buse:2008} proposed a technique to generate 
human-readable documentation for unexpected thrown exceptions~\cite{Buse:2008}. 
However, both these two approaches require that tests fail~
\cite{Zhang2001} or throw unexpected Java exceptions~\cite{Buse:2008}. This never 
happens for automatically generated test cases, since the automatically generated 
assertions reflect the current behaviour of the class~\cite{Fraser:study2013}. 
Consequently, if the current behaviour is faulty the generated assertions do not fail 
because they reflect the incorrect behavior.

%\vspace{-3mm}
Kamimura  {\em et al.}~\cite{Kamimura2013} argued that developers might benefit from a consumable and understandable textual summary of a test case. Therefore, they proposed an initial step towards generating such summaries based on static analysis of the code composing the test cases~\cite{Kamimura2013}. From an engineering point of view, our work resumes this line of research; however, it is novel for two main reasons. First of all our approach generates summaries combining three different levels of granularity: (i) a summary of the main responsibilities of the class under test (class level); (ii) a fine-grained description of each statement composing the test case as done in the past~\cite{Kamimura2013} (test level); (iii)  a description of the branch conditions traversed in the executed path of the class under test (coverage level).
As such, our approach \emph{combines} code coverage and summarization to address the problem of describing the effect of test case execution in terms of structural coverage. Finally, we evaluate the impact of the generated tests summaries in a real scenario where developers were asked to test and fix faulty classes. 

%\vspace{-3mm}
Understandability is also widely related with the test size and number of assertions~\cite{athanasiouTSE2014}. For these reasons previous work on automatic test generation focused on (i) reducing the number of generated tests by applying a post-process minimization~\cite{FraserA13}, and (ii) reducing the number of assertions by using mutation analysis~\cite{Fraser:2010}, or splitting tests with multiple assertions~\cite{Xuan:fse2014}. To improve the readability of the code composing the generated tests, Daka {\em et al.}~\cite{Ermira2015} proposed a mutation-based post-processing technique that uses a domain-specific model for unit test readability based on human judgement. Afshan {\em et al.}~\cite{Afshan:icst2013} investigates the use of a linguistic model to generate more readable input strings. Our paper shows that summaries represent an important element for complementing and improving the readability of automatically generated test cases.
