In order to execute TestSmellDescriber in headless mode you should apply the following steps:

1. Download the TestSmellDescriber project:
	a. Pull the TestSmellDescriber project from our repository by executing 'git clone https://bitbucket.org/sealuzh/testsmellsdescriber' in the terminal.

	b. Navigate to the project within your terminal.

	b. Download the necessary modules by executing the following three commands:
		- git submodule init
		- git submodule update

2. Export TestSmellDescriber as a runnable JAR:

	a. Open your Eclipse IDE (Note: We used the following IDE: Eclipse Java EE, version Oxygen.2 Release (4.7.2)).
	
	b. Import (File > Open Projects from File System) the following projects into your IDE:
		- testsmellsdescriber\TACO-detector
		- testsmellsdescriber\TestSmellDescriberRunner
		- testsmellsdescriber\TestSmellRunner
		- testsmellsdescriber\TestSmellUtil
		- all projects within testsmellsdescriber\smelldetectioncaller\

	c. Select the class TestSmellDescriberRunner/testsmell/describer/Main.java and run it as a Java Application. The execution can be stopped immediately after. This step is required to make the class available as a launch configuration for step 2f.

	d. Open the context menu of the same class and click the item Export.

	e. Select Runnable JAR file.

	f. Set the launch configuration to "Main - TestSmellDescriberRunner" and select the desired output path in the Export destination field.

	g. Select "Package required libraries into generated JAR".

	h. Click Finish.

3. The folder containing the smell description templates has to be added to folder containing the newly generate JAR file. For that move the folder 'descriptions' found in /testsmellsdescriber/TestSmellDescriberRunner/ to the folder containing the JAR file.

4. Execute TestSmellDescriber by executing the command "java -jar TestSmellDescriber.jar" and follow the instructions on the screen.
	- TestSmellDescriber requires the following five parameters:
		a. the path to the source files of the to be examined project (e.g. "C:/apache/src"),
		b. the path to binary files of the to be examined project (e.g. "C:/apache/bin"),
		c. the package path of the to be described test class separated by '/' (e.g. "org/apache/ofbiz/base/util/string/FlexibleStringExpander.java"),
		d. the package path of the by the test class tested production class separated by '/', (e.g. "org/apache/ofbiz/base/util/string/FlexibleStringExpander.java")
		e. the bytecode file type of the binary files ("jar" or "class").

	- You can alternatively input all five information by executing "java -jar TestSmellDescriber.jar <a> <b> <c> <d> <e>".