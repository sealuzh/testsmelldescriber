# TestSmellDescriber

___
___

## Download the TestSmellDescriber project:
  1. Pull the TestSmellDescriber project from our repository by executing 'git clone https://bitbucket.org/sealuzh/testsmellsdescriber' in the terminal.
  2. Navigate to the project within your terminal.
  3. Download the necessary modules by executing the following three commands:
	- git submodule init
	- git submodule update

___
___

## Import TSD into your IDE:
  1. Open your Eclipse IDE *(Note: We used the following IDE: Eclipse Java EE, version Oxygen.2 Release (4.7.2))*.
  2. Import (File > Open Projects from File System) the following projects into your IDE:
    - testsmellsdescriber\TACO-detector
    - testsmellsdescriber\TestSmellDescriberRunner
	- testsmellsdescriber\TestSmellRunner
	- testsmellsdescriber\TestSmellUtil
	- all projects within testsmellsdescriber\smelldetectioncaller\

___
___

## Run TSD:
___

### Within the IDE:
Execute *TestSmellDescriberRunner/src/testsmell/describer/Main.java* and provide the necessary information (see below) by follow the instructions on the screen.

___

### In headless mode:

#### Export TSD:
  1. Select the class TestSmellDescriberRunner/testsmell/describer/Main.java and run it as a Java Application. The execution can be stopped immediately after. This step is required to make the class available as a launch configuration for step 4.
  2. Open the context menu of the same class and click the item Export.
  3. Select Runnable JAR file.
  4. Set the launch configuration to **Main - TestSmellDescriberRunner** and select the desired output path in the Export destination field.
  5. Select "Package required libraries into generated JAR".
  6. Click Finish.
  7. The folder containing the smell description templates has to be added to folder containing the newly generate JAR file. For that move the folder 'descriptions' found in /testsmellsdescriber/TestSmellDescriberRunner/ to the folder containing the JAR file.

You can alternatively download the executable from [here](https://bitbucket.org/sealuzh/testsmellsdescriber/downloads/TSD%20Executable.zip).

#### Run TSD:
Execute the command **java -jar TestSmellDescriber.jar** and provide the necessary information (see below) by follow the instructions on the screen.

You can alternatively input all five information by executing **java -jar .\TestSmellDescriber.jar <1> <2> <3> <4> <5>**.

___
___

## TestSmellDescriber requires the following five parameters:
  1. the path to the source files of the to be examined project (e.g. "C:/apache/src"),
  2. the path to the binary files of the to be examined project (e.g. "C:/apache/bin"),
  3. the package path of the to be described test class separated by '/' (e.g. "org/apache/ofbiz/base/util/string/FlexibleStringExpander.java"),
  4. the package path of the by the test class tested production class separated by '/', (e.g. "org/apache/ofbiz/base/util/string/FlexibleStringExpander.java"),
  5. the bytecode file type of the binary files ("jar" or "class").