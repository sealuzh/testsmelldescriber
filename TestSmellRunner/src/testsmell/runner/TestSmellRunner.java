package testsmell.runner;

import java.io.File;
import java.util.Arrays;
import java.util.Vector;

import nl.tudelft.runner.all.TestSmellDetectionRunner;
import ptidej.sad.smelldetectioncaller.SmellDetectionCaller;

public class TestSmellRunner {

	
	public static void main(String args[]) {
	}
	
	public static void testAllProjects() {
		// DECOR requires a type in which way the compiled project is. Either in .class
		// files or .jar file(s). You have to tell him what kind of files he should be
		// looking for. I use this list to find the projects with class files.
		final Vector<Integer> projectsWithClassFiles = new Vector<Integer>();
		projectsWithClassFiles.add(7); // cloudstack -
		projectsWithClassFiles.add(17); // ofbiz -
		projectsWithClassFiles.add(18); // oodt
		projectsWithClassFiles.add(32); // dataneuclus
		projectsWithClassFiles.add(35); // geode
		projectsWithClassFiles.add(48); // jpos -

		File projectDir = new File("./../TestProjects/Combined/");

		// Since listFiles() returns the folders in an unsorted array, I first sort them
		// so I can find the .class projects with "projectsWithClassFiles".
		String[] decorProjects = new String[projectDir.listFiles().length];
		for (int i = 0; i < decorProjects.length; ++i) {
			File dir = projectDir.listFiles()[i];
			if (dir.isDirectory()) {
				decorProjects[i] = dir.getAbsolutePath();
			}
		}
		Arrays.sort(decorProjects);
		
		for (int i = 0; i < decorProjects.length; ++i) {
			String projectMode = "jar";
			if (projectsWithClassFiles.indexOf(i) > -1) {
				projectMode = "class";
			}
			run("textual", decorProjects[i] + "/src/", "./../Output/Combinednewnew/");
			run("structural", decorProjects[i] + "/bin", "./../Output/Combinednewnew/", projectMode);
		}
	}
	
	/*"
	 * Run single projects with either DECOR or/and TACO
	 */
	public static void testOneProject(String projectPath, String outputPath, String fileType) {
		 run("textual", projectPath + "/src/", outputPath);
		 run("structural", projectPath + "/bin/", outputPath, fileType);
	}

	public static void run(String smellType, String inputPath, String outputPath) {
		run(smellType, inputPath, outputPath, "");
	}

	public static void run(String smellType, String inputPath, String outputPath, String mode) {
		switch (smellType) {
		case "textual":
			/*
			 * TACO
			 */
			TestSmellDetectionRunner.runner(new String[] { inputPath, outputPath });
			break;
		case "structural":
			/*
			 * DECOR
			 */
			final String[] param = new String[] { inputPath, outputPath, mode };			
			SmellDetectionCaller.main(param);
			break;
		default:
			System.out.println("Test smell type to detect not recognized. Options: structural, textual");
			System.exit(1);
		}
	}

}
