package testsmell.constructs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SmellyAttributeContainer {
	
	private String belongingClassName;
	private ArrayList<SmellyMethod> methods;
	private ArrayList<SmellyField> fields;
	
	public SmellyAttributeContainer(String belongingClassName) {
		this.belongingClassName = belongingClassName;
		this.methods = new ArrayList<>();
		this.fields = new ArrayList<>();
	}

	public void addField(String fieldName, String smellName) {
		SmellyField smellyField = getField(fieldName);
		if (smellyField == null) {
			smellyField = new SmellyField(fieldName, smellName);
			this.fields.add(smellyField);
		} else {
			smellyField.addSmell(smellName);
		}
	}
	
	public void addMethod(String methodName, Map smellSpecificValues, String smellName) {
		String equalizedParameterStyle = EntityAdapter.equalizeParameterStyle(methodName);
		SmellyMethod smellyMethod = getMethod(equalizedParameterStyle);
		if (smellyMethod != null) {
			smellyMethod.addSmell(smellName, smellSpecificValues);
		} else {
			smellyMethod = new SmellyMethod(equalizedParameterStyle, smellSpecificValues, smellName);
			this.methods.add(smellyMethod);
		}
	}
	
	public SmellyField getField(String fieldName) {
		Iterator iter = this.fields.iterator();
		SmellyField smellyField;
		while (iter.hasNext()) {
			smellyField = (SmellyField) iter.next();
			if (smellyField.getName().equals(fieldName)) {
				return smellyField;
			}
		}
		return null;
	}	
	
	public Iterator getFieldSmellsIterator(String fieldName) {
		return getField(fieldName).getSmellsIterator();
	}
	
	public SmellyMethod getMethod(String methodName) {
		Iterator iter = this.methods.iterator();
		SmellyMethod smellyMethod;
		while (iter.hasNext()) {
			smellyMethod = (SmellyMethod) iter.next();
			String currentMethodName = smellyMethod.getName();
			if (EntityAdapter.compareMethodNames(currentMethodName, methodName)) {
				return smellyMethod;
			}
		}
		return null;
	}
	
	public Iterator getMethodSmellsIterator(String methodName) {
		return getMethod(methodName).getSmellsIterator();
	}
	
	public String getMethodNamesForSmell(String smell) {
		Iterator iter = methods.iterator();
		String methodNames = new String();
		while (iter.hasNext()) {
			SmellyMethod method = (SmellyMethod) iter.next();
			if (method.getSmells().contains(smell)) {
				if (methodNames.length() > 0) {
					methodNames += ", ";
				}
				
				methodNames += removeParameterFromMethodName(method.getName());
			}
		}
		return methodNames;
 	}
	
	private String removeParameterFromMethodName(String methodName) {
		if (!methodName.contains("(")) {
			return methodName;
		}
		int parameterStartIndex = methodName.indexOf("(");
		return methodName.substring(0, parameterStartIndex);
	}
	
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		String method = attributeToString(methods.iterator(), "Smelly Methods");
		buffer.append(method);
		String field = attributeToString(fields.iterator(), "Smelly Fields");
		buffer.append(field);
		
		return buffer.toString();
	}
	
	private String attributeToString(Iterator iter, String type) {
		final StringBuffer buffer = new StringBuffer();
		if (iter.hasNext()) {
			buffer.append("\n- " + type + ":");
			while(iter.hasNext()) {
				SmellyAttribute field = (SmellyAttribute) iter.next();
				buffer.append("\n" + field.toString());
			}
		}
		return buffer.toString();
	}

}
