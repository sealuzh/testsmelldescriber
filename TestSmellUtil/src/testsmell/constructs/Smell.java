package testsmell.constructs;

import java.util.HashSet;

public class Smell {

	private Integer occurence;
	private String name;
	private HashSet<String> projects;

	public Smell(String smellName) {
		this.name = smellName;
		this.occurence = 0;
		this.projects = new HashSet<String>();
	}

	public Integer getOccurence() {
		return occurence;
	}

	public void setOccurence(Integer occurence) {
		this.occurence = occurence;
	}

	public HashSet<String> getProjects() {
		return projects;
	}

	public void setProjects(HashSet<String> projects) {
		this.projects = projects;
	}

	public void addProject(String projectName) {
		this.projects.add(projectName);
	}

	public int increaseOccurence(String projectName) {
		projects.add(projectName);
		return this.occurence++;
	}

}
