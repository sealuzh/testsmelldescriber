package testsmell.constructs;

public enum SmellInformation {
	LOC,
	MAX_LOC,
	NO_PARAMS,
	MAX_PARAMS,
	PERCENTAGE_OF_METHODS,
	CALLED_METHODS,
	PARAMETERS,
	NUMBER_OF_METHODS,
	ONLY_ONE,
	CLASS_UNDER_TEST,
	METHOD_NAMES,
	REFACTORING_NUMBER,
	SMELL_IN_CLASS_TO_ALL_IN_CLASS,
	SMELL_IN_PROJECT_TO_ALL_IN_PROJECT,
	SMELL_IN_CLASS_TO_ALL_IN_PROJECT;
	
	public String toString() {
		return this.name().toLowerCase();
	}
}
