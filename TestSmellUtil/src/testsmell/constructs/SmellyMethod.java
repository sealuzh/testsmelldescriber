package testsmell.constructs;

import java.util.List;
import java.util.Map;

public class SmellyMethod extends SmellyAttribute {

	public SmellyMethod(String methodName, Map smellSpecificValues, String smellName) {
		super(methodName, smellSpecificValues, smellName);
	}
	
}
