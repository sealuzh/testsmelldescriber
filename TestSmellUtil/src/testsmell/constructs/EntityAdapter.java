package testsmell.constructs;

public class EntityAdapter {

//	private static int startOfParametersA, startOfParametersB;
	final static String packageRegex = "(([A-Za-z]*)\\.)+";
	
	/**
	 * A parameter in DECOR consists of the full package + class name (e.g. java.lang.String),
	 * TACO only returns the class name. Here we equalize them by getting rid of the package
	 * name.
	 * @param unprocessedName
	 * @return equalizedName
	 */
	public static String equalizeParameterStyle(String unprocessedName) {
		String equalizedName = unprocessedName;
		if (equalizedName.indexOf('.') > 0) {
			equalizedName = equalizedName.replaceAll(packageRegex, "");
		}
		return equalizedName;
	}
	
	/**
	 * Different types of irregularities were found between DECOR and TACO in regards to method parameters.
	 * 1) TACO maps every sub class of Collection and overwrites the declaration (see isCollection()).
	 * 2) DECOR maps the class I is to Iterable.
	 * 3) DECOR maps org.w3c.dom.Element to w3Element.
	 * 4) Generics are mapped to type Object.
	 * @param methodA
	 * @param methodB
	 * @return
	 */
	public static boolean compareMethodNames(String methodA, String methodB) {
		boolean namesAreEqual = false;
		boolean parametersAreEqual = false;

		int startOfParametersA = getParameterStartingIndex(methodA);
		int startOfParametersB = getParameterStartingIndex(methodB);

		if (startOfParametersA > -1 && startOfParametersB > -1) {
			String[] methodNames = new String[] {
					methodA.substring(0, startOfParametersA),
					methodB.substring(0, startOfParametersB)};

			if (methodNames[0].equals(methodNames[1])) {
				namesAreEqual = true;
				String[] parametersA = getParametersAsArray(methodA);
				String[] parametersB = getParametersAsArray(methodB);
				
				parametersA = removeGenericParameters(parametersA);
				parametersB = removeGenericParameters(parametersB);
				
				if (parametersA.length == parametersB.length) {
					int index = 0;
					parametersAreEqual = true;
					
					while (index < parametersA.length && parametersAreEqual) {
						String parameterA = parametersA[index];
						String parameterB = parametersB[index];
						parametersAreEqual = parametersAreEqual(parameterA, parameterB);
						index++;
					}
				}
			}
		}
		return namesAreEqual && parametersAreEqual;
	}
	
	private static boolean parametersAreEqual(String parameterA, String parameterB) {
		return parameterA.equals(parameterB)
				|| parametersAreCollections(parameterA, parameterB)
				|| parametersAreIterables(parameterA, parameterB)
				|| parametersAreOtherSpecialCases(parameterA, parameterB);
	}
	
	private static boolean parametersAreCollections(String parameterA, String parameterB) {
		return isCollection(parameterA) && isCollection(parameterB);
	}
	
	private static boolean parametersAreIterables(String parameterA, String parameterB) {
		return isIterable(parameterA) && isIterable(parameterB);
	}
	
	private static boolean parametersAreOtherSpecialCases(String parameterA, String parameterB) {
		return isElement(parameterA) && isElement(parameterB)
				|| isGeneric(parameterA) && isGeneric(parameterB);
	}
	
	/**
	 * Borrowed from PADL and adapted. TACO maps every type of collection to java.util.Collection
	 * through SingleVariableDeclaration. Necessary to compare a Collection parameter with a
	 * Collection sub class from DECOR.
	 * Obvious implication: What if a function is overloaded with two different Collection classes?
	 * @param aTypeName
	 * @return
	 */
	private static boolean isCollection(final String aTypeDisplayName) {
		return aTypeDisplayName.equals("Collection")
				|| aTypeDisplayName.equals("AbstractList")
				|| aTypeDisplayName.equals("ArrayList")
				|| aTypeDisplayName.equals("LinkedList")
				|| aTypeDisplayName.equals("Vector")
				|| aTypeDisplayName.equals("Stack")
				|| aTypeDisplayName.equals("AbstractSequentialList")
				|| aTypeDisplayName.equals("List")
				|| aTypeDisplayName.equals("Set")
				|| aTypeDisplayName.equals("AbstractSet")
				|| aTypeDisplayName.equals("HashSet")
				|| aTypeDisplayName.equals("LinkedHashSet")
				|| aTypeDisplayName.equals("TreeSet")
				|| aTypeDisplayName.equals("SortedSet")
				|| aTypeDisplayName
					.equals("padl.creator.test.relationships.providers.VectorTest");
	}
	
	/**
	 * Class I is mapped to class Iterable in DECOR.
	 * @param aType
	 * @return
	 */
	private static boolean isIterable(final String aType) {
		return aType.equals("Iterable") || aType.equals("I");
	}
	
	/**
	 * DECOR maps org.w3c.dom.Element to w3Element.
	 * @param aType
	 * @return
	 */
	private static boolean isElement(final String aType) {
		return aType.equals("Element") || aType.equals("w3Element");
	}
	
	private static boolean isGeneric(final String aType) {
		return aType.equals("Object") || aType.length() == 1;
	}
	
	private static int getParameterStartingIndex(String methodName) {
		return methodName.indexOf("(");
	}
	
	private static String[] getParametersAsArray(String method) {
		int indexWithoutOpeningBracket = getParameterStartingIndex(method) + 1;
		int indexWithoutClosingBracket = method.length() - 1;
		String parameterSansMethodName = method.substring(
				indexWithoutOpeningBracket, indexWithoutClosingBracket);
		return parameterSansMethodName.split(", ");
	}
	
	private static String[] removeGenericParameters(String[] unprocessedParameters) {
		String[] processedParameters = new String[unprocessedParameters.length];
		for (int i = 0; i < unprocessedParameters.length; ++i) {
			String param =  unprocessedParameters[i];
			int startOfGenericParameter = param.indexOf("<");
			if (startOfGenericParameter > 0) {
				param = param.substring(0, startOfGenericParameter);
			}
			processedParameters[i] = param;
		}
		return processedParameters;
	}
	
}
