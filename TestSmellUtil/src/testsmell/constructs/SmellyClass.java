package testsmell.constructs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SmellyClass {
	
	private String className;
	private SmellyAttributeContainer smellyAttributeContainer;
	private Map<String, Integer> smellsAndOccurence;
	private int allSmellOccurencesInClass = 0;
	
	public SmellyClass(String name) {
		this.className = name;
		smellyAttributeContainer = new SmellyAttributeContainer(name);
		this.smellsAndOccurence = new HashMap<>();
	}
	
	public String getName() {
		return this.className;
	}
	
	public void addClassSmell(String smellName) {
		addSmell(smellName);
	}
	
	public void addSmell(String smellName) {
		Integer numberOfSmellyAttributes = smellsAndOccurence.get(smellName);
		if (numberOfSmellyAttributes != null) {
			smellsAndOccurence.put(smellName, numberOfSmellyAttributes + 1);
		} else {
			smellsAndOccurence.put(smellName, 1);
		}
		allSmellOccurencesInClass++;
	}
	
	public Integer getSmellOccurence(String smellName) {
		return smellsAndOccurence.get(smellName);
	}
	
	public int getNumberOfSmells() {
		return smellsAndOccurence.size();
	}
	
	public Iterator getSmellsIterator() {
		Set entrySet = smellsAndOccurence.entrySet();
		Iterator iter = entrySet.iterator();
		return iter;
	}
	
	public void addField(String fieldName, String smellName) {
		this.smellyAttributeContainer.addField(fieldName, smellName);
		addSmell(smellName);
	}
	
	public void addMethod(String methodName, Map smellSpecificValues, String smellName) {
		this.smellyAttributeContainer.addMethod(methodName, smellSpecificValues, smellName);
		addSmell(smellName);
	}
	
	public SmellyAttributeContainer getAttributes() {
		return this.smellyAttributeContainer;
	}
	
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("\nClass name: " + className);
		buffer.append("\n- All Smells: ");
		Iterator iter = getSmellsIterator();
		while (iter.hasNext()) {
			Map.Entry pair = (Map.Entry) iter.next();
			buffer.append((String) pair.getKey());
			if (iter.hasNext()) {
				buffer.append(", ");
			}
		}
		buffer.append(smellyAttributeContainer.toString());
		return buffer.toString();
	}
	
	public int getAllSmellOccurencesInClass() {
		return allSmellOccurencesInClass;
	}
	
	public String getAttributesForSmell(String smell) {
		return smellyAttributeContainer.getMethodNamesForSmell(smell);
	}
}
