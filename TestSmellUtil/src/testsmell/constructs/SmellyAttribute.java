package testsmell.constructs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SmellyAttribute {

	private String attributeName;
	private Set<String> smells;
	private Map<String, Map<SmellInformation, String>> smellsAndAttribute;
	
	public SmellyAttribute(String attributeName, String smellName) {
		initAttribute(attributeName, null, smellName);
	}
	
	public SmellyAttribute(String attributeName, Map smellSpecificValues, String smellName) {
		initAttribute(attributeName, smellSpecificValues, smellName);
	}
	
	private void initAttribute(String attributeName, Map smellSpecificValues, String smellName) {
		this.attributeName = attributeName;
		this.smells = new HashSet<>();
//		this.smells.add(smellName);
		this.smellsAndAttribute = new HashMap<>();
		
		addSmell(smellName, smellSpecificValues);
	}
	
	public String getName() {
		return attributeName;
	}
	
	public void addSmell(String smellName) {
		addSmell(smellName, null);
	}
	
	public void addSmell(String smell, Map smellSpecificValues) {
//		if (smellSpecificValues != null) {
//			addSmellInformation(smell, smellSpecificValues);
//		}
//		smellsAndAttribute.get(smell).putAll(smellSpecificValues);
//		this.smells.add(smell);
		addSmellInformation(smell, smellSpecificValues);
	}
	
	public void addSmellInformation(String smell, SmellInformation informationName, String information) {
//		smellsAndAttribute.get(smell).put(informationName, information);
		Map smellInformation = smellsAndAttribute.get(smell);
		if (smellInformation != null) {
			smellsAndAttribute.get(smell).put(informationName, information);
		} else {
			Map<SmellInformation, String> innerMap = new HashMap<>();
			smellsAndAttribute.put(smell, innerMap);
			smellsAndAttribute.get(smell).put(informationName, information);
		}
	}
	
	public void addSmellInformation(String smell, Map smellSpecificValues) {
		Map smellInformation = smellsAndAttribute.get(smell);
		if (smellInformation != null) {
			smellsAndAttribute.get(smell).putAll(smellSpecificValues);
		} else {
			smellsAndAttribute.put(smell, smellSpecificValues);
		}
	}
	
	public Iterator getSmellsIterator() {
		return smellsAndAttribute.keySet().iterator();
	}
	
	public Iterator getSmellInformation(String smell) {
		return smellsAndAttribute.get(smell).entrySet().iterator();
	}
	
	public Set getSmells() {
		return smellsAndAttribute.keySet();
	}
	
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("\t" + attributeName + ": ");
		Iterator iter = getSmellsIterator();
		while (iter.hasNext()) {
			String smellName = (String) iter.next();
			buffer.append(smellName);
			if (iter.hasNext()) {
				buffer.append(", ");
			}
		}
		return buffer.toString();
	}
}
