package testsmell.testdescriber;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;

import testsmell.util.SmellyClassContainer;
import testsmell.description.TestSmellDescriptionWrapper;

//import org.apache.xmlbeans.impl.xb.ltgfmt.TestsDocument.Tests;
//import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
//import org.eclipse.jdt.core.dom.Type;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.type.ClassOrInterfaceType;

import testsmell.testdescriber.parser.JavaFileParser;
import testsmell.testdescriber.parser.bean.ClassBean;
import testsmell.testdescriber.parser.bean.InstanceVariableBean;
import testsmell.testdescriber.parser.bean.MethodBean;
import testsmell.testdescriber.utility.FileUtils;

/**
 * Main parser for Test case Summarization..
 *
 * Ivan: Adapted for TestSmellDescriber.
 * 
 * @author Sebastiano Panichella and Annibale Panichella
 */
public class TestDescriber {

	/**
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ParseException
	 */
	public static void main(String[] args) throws IOException, InterruptedException, ParseException {

		String sourceFolder = args[0];
		String pBinFolder = args[1];
		
		List<String> classesFiles = new ArrayList<String>();
		classesFiles.add(args[2]);

		List<String> testsFiles = new ArrayList<String>();
		testsFiles.add(args[3]);

		System.out.println("Step 1: Parsing JAVA CLASSES/JAVA TESTS");
		Vector<ClassBean> productionClass = JavaFileParser.parseJavaClasses(classesFiles, sourceFolder);
		Vector<ClassBean> testClass = JavaFileParser.parseJavaClasses(testsFiles, sourceFolder);

		System.out.println("Step 4: Generating Summaries as code comments");
		ClassBean classeTest = testClass.get(0);
		ClassBean clazz = productionClass.get(0);
		SmellyClassContainer.getInstance().print();
		String currentClassPackage = classeTest.getBelongingPackage().replace("package ", "");
		currentClassPackage = currentClassPackage.substring(0, currentClassPackage.length() - 1);
		String fullClassName = currentClassPackage + "." + classeTest.getName();
		TestSmellDescriptionWrapper.setCurrentClass(fullClassName);

		System.out.println("Step 4.1: Generating a summary for each test method");
		for (MethodBean testCase: classeTest.getMethods()) {
			ClassBean classeExecuted = JavaFileParser.parseJavaClass(productionClass.get(0).getTextContent());
			testCase.setOriginalClassOfTestCase(clazz);
			testCase.setClassExecutedByTheTestCase(classeExecuted);

			
			String parameters = parseMethodParameters(testCase);
			String methodNameWithParameters = testCase.getName() + "(" + parameters + ")";
			Set methodSmells = TestSmellDescriptionWrapper.getMethodSmells(methodNameWithParameters);
			if (methodSmells != null && methodSmells.size() > 0) {
				if (methodSmells.contains("EagerTest")) { 
					TestSmellDescriptionWrapper.parseCalledMethods("EagerTest", testCase.getMethodCalls().iterator());
					TestSmellDescriptionWrapper.parseEagerTestValues(clazz.getMethods().iterator());
				}
				if (methodSmells.contains("GeneralFixture")) {
					TestSmellDescriptionWrapper.parseCalledMethods("GeneralFixture", testCase.getMethodCalls().iterator());
				}
				if (methodSmells.contains("LongParameterList")) {
					TestSmellDescriptionWrapper.passParameters(maxTwoParameters(parameters));
				}
				String formattedDescription = TestSmellDescriptionWrapper.getAttributeDescription(true);
				String newContent = replaceText(classeTest, testCase, formattedDescription);
				classeTest.setTextContent(newContent);
			}
		}

		System.out.println("Step 4.2: Generating a summary for the Class Under Test");
		// First we generate the comment


		if (TestSmellDescriptionWrapper.classIsSmelly(fullClassName)) {
			String smellyDescription = TestSmellDescriptionWrapper.getClassDescription(true);
			String classWithSmelly = classeTest.getTextContent().replace("public class ", smellyDescription + "\npublic class ");
			classeTest.setTextContent(classWithSmelly);
		} else {
			classeTest.setTextContent(classeTest.getTextContent());
		}

		// Then, we add the generated comments just before the class declaration



		// we change the name of the test class
		classeTest.setTextContent(classeTest.getTextContent().replace("public class " + classeTest.getName(), "public class " + classeTest.getName() + "withDescription"));
		// we save the new (renamed) test class
		String pathNewTextClass = sourceFolder + testsFiles.get(0).replace(".java", "withDescription.java");
		FileUtils.writeFile(pathNewTextClass, classeTest.getTextContent());
		System.out.println("GENERATED JUNIT CLASS");
		System.out.println(classeTest.getTextContent());
	}


	private static String parseMethodParameters(MethodBean method) {
		String parameters = new String();
		List parameterList = method.getParameters();
		Iterator iter = parameterList.iterator();
		while(iter.hasNext()) {
			SingleVariableDeclaration svd = (SingleVariableDeclaration) iter.next();
			Type type = svd.getType();
			String parameterName = type.toString();
			if (type.isParameterizedType()) {
				parameterName = parameterName.substring(0, parameterName.indexOf("<"));
			}
			parameters += parameterName;
			if (iter.hasNext()) {
				parameters += ", ";
			}
		}
		return parameters;
	}
	
	private static String maxTwoParameters(String parameters) {
		String[] parameterArray = parameters.split(", ");
		return parameterArray.length < 2 ? parameters : parameterArray[0] + ", " + parameterArray[1] + ", etc.";
	}

	/**
	 * This method replace the (textual) content of the first input parameter ({@link ClassBean}). Specifically,
	 * it replace the original method {@link MethodBean} with a new method (enriched with summaries)
	 *
	 * @param testClass          test class for which we want to replace the content
	 * @param testCase           test method to replace
	 * @param methodWithComments new method to add
	 * @return
	 */
	public static String replaceText(ClassBean testClass, MethodBean testCase, String methodWithComments) {
		String java = testClass.getTextContent();
		CompilationUnit cu = null;
		try {
			cu = JavaParser.parse(new ByteArrayInputStream(java.getBytes()));
			for (Node node : cu.getChildrenNodes()) {
				if (!(node instanceof ClassOrInterfaceDeclaration))
					continue;

				ClassOrInterfaceDeclaration clazz = (ClassOrInterfaceDeclaration) node;
				for (int index = 1; index < clazz.getChildrenNodes().size(); index++) {
					Node methodNode = clazz.getChildrenNodes().get(index);
					if (methodNode instanceof MethodDeclaration) {
						MethodDeclaration method = (MethodDeclaration) methodNode;
						if (method.getName().equals(testCase.getName())) {
							//	                        MethodDeclaration newMethod = convertString2MethodDeclaration(methodWithComments);
							//	                        method.setBody(newMethod.getBody());
							method.setJavaDoc(new JavadocComment(methodWithComments));
						}
					}
				}
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cu.toString();
	}

	/**
	 * This method convert a String representing a Java method in an object of the class {@link MethodDeclaration}
	 *
	 * @param method String to convert
	 * @return object of the class {@link MethodDeclaration}
	 */
	protected static MethodDeclaration convertString2MethodDeclaration(String method) {
		MethodDeclaration newMethod = null;
		String clazz = "public class wrapper {" + method + "}";
		try {
			CompilationUnit cu = JavaParser.parse(new ByteArrayInputStream(clazz.getBytes()));
			ClassOrInterfaceDeclaration classDeclaration = (ClassOrInterfaceDeclaration) cu.getChildrenNodes().get(0);
			newMethod = (MethodDeclaration) classDeclaration.getChildrenNodes().get(0);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newMethod;
	}

	public static void setSignatureAndMethodsComments(MethodBean testcase, ClassBean classe, ClassBean classeExecuted) {
		Vector<MethodBean> coveredMethods = (Vector<MethodBean>) classeExecuted.getMethods();
		for (int methodIndex = 0; methodIndex < coveredMethods.size(); methodIndex++) {
			MethodBean coveredMethod = coveredMethods.get(methodIndex);
			String methodSignature = coveredMethod.getTextContent().split("\n")[0];
			methodSignature = methodSignature.substring(0, methodSignature.indexOf(")") + 1);
			methodSignature = methodSignature.replace(",", ", ");
			coveredMethod.setSignature(methodSignature);
		}

		for (int methodIndex = 0; methodIndex < coveredMethods.size(); methodIndex++) {
			MethodBean coveredMethod = coveredMethods.get(methodIndex);
			String methodSignature = coveredMethod.getTextContent().split("\n")[0];
			methodSignature = methodSignature.substring(0, methodSignature.indexOf(")") + 1);
			methodSignature = methodSignature.replace(",", ", ");

			// if to determine if this is a constructor or a method...
			coveredMethod.setConstructor(false);//Initialization
			if (methodSignature.contains("public " + classe.getName() + "(")) {
				coveredMethod.setConstructor(true);
			}

			coveredMethod.generateMethodCommentsAnalyzingClassTextContent(classe.getTextContent());
		}
	}

	/*
	 * return the description of the class and set the imports in the java class specified as parameter
	 */
	public static String generateClassDescription(List<String> textContentExecutedOriginalClass, ClassBean classe) {
		String classDescription = "", word = "";
		StringTokenizer tokens = null;
		boolean startDescription = false, endDescription = true;
		boolean foundImport = false;
		String line = "";
		for (int i = 0; i < textContentExecutedOriginalClass.size(); i++) {
			line = textContentExecutedOriginalClass.get(i);
			// we add the imports..
			if (line.contains("import ") & startDescription == false) {
				foundImport = true;
			}
			//if we are in the beginning of the description
			if (foundImport == true & line.contains("/**")) {
				startDescription = true;
			}

			if ((line.contains("@author") | line.contains("@see")) & foundImport == true) {
				startDescription = false;
				endDescription = true;
				foundImport = false;
				//classDescription=classDescription+"\r **/";
			}
			//if we are in the beginning of the description
			if (startDescription == true) {
				classDescription = classDescription + "\r" + textContentExecutedOriginalClass.get(i);
			}

		}

		classDescription = classDescription.replace("<p>", "* ");

		tokens = new StringTokenizer(classDescription.replace("*", ""));
		if (tokens.hasMoreTokens()) {
			tokens.nextToken();
			word = tokens.nextToken();// we take the first word
			//System.out.println("word.. "+word);
			//it verifies if the first words in in third person (ends with "s")
			if (word.substring(word.length() - 1, word.length()).contains("s")) {
				classDescription = classDescription.replace("* " + word, "* " + word.toLowerCase());
			} else//if it is not in third person..
			{
				classDescription = classDescription.replace("* " + word, "* represents a " + word.toLowerCase());
			}
		}

		classDescription = classDescription.replace("/**", "/** OVERVIEW: \r * This test case tests the class <code> " + classe.getName() + " </code>, which \r");
		classDescription = classDescription + "*/";
		//use spell checker
		//        classDescription = SpellCorrector.correctSentences(classDescription);
		return (classDescription);
	}


	public static void printClassContent(ClassBean classe) {
		Vector<MethodBean> methods = null;
		MethodBean method = null;
		List<SingleVariableDeclaration> parameters = null;
		SingleVariableDeclaration parameter = null;
		Vector<MethodBean> methodsCalls = null;
		MethodBean methodCall = null;
		ArrayList<InstanceVariableBean> attributes = null;
		Vector<InstanceVariableBean> variables = null;
		InstanceVariableBean attribute = null;
		InstanceVariableBean variable = null;
		//System.out.println(classes.get(c).getTextContent());
		methods = (Vector<MethodBean>) classe.getMethods();
		List<String> classImports = null;
		System.out.println("CLASS NAME: \"" + classe.getName() + "\"");
		System.out.println("CLASS IMPORTS:");
		classImports = (ArrayList<String>) classe.getImports();
		if (classImports != null) {
			for (int i = 0; i < classImports.size(); i++) {

				System.out.println("PACKAGE NAME: " + classImports.get(i));
			}
		}
		System.out.println("CLASS ATTRIBUTES: ");
		attributes = classe.getInstanceVariables();
		if (attributes != null) {
			for (int a = 0; a < attributes.size(); a++) {
				attribute = attributes.get(a);
				System.out.println("ATTRIBUTE NAME: \"" + attribute.getName() + "\" type \"" + attribute.getType() + "\"");
			}
		}


		System.out.println("CLASS METHODS:");
		for (int m = 0; m < methods.size(); m++) {
			method = methods.get(m);
			System.out.println("METHOD NAME: " + method.getName());
			System.out.println(method.getTextContent());

			parameters = method.getParameters();
			for (int p = 0; p < parameters.size(); p++) {
				parameter = parameters.get(p);
				System.out.println("METHOD Parameter -> " + parameter.getName() + " of type \"" + parameter.getType() + "\"");
			}
			System.out.println("METHOD Return type: \"" + method.getReturnType() + "\"");
			methodsCalls = (Vector<MethodBean>) method.getMethodCalls();
			for (int p = 0; p < methodsCalls.size(); p++) {
				methodCall = methodsCalls.get(p);
				System.out.println("METHOD call to -> " + methodCall.getName());
			}
			variables = (Vector<InstanceVariableBean>) method.getUsedInstanceVariables();
			for (int v = 0; v < variables.size(); v++) {
				variable = variables.get(v);
				System.out.println("METHOD variable -> " + variable.getName());
			}

		}
		System.out.println("END Print of CLASS: \"" + classe.getName() + "\"");

	}

	public static void printMethodContent(MethodBean method) {
		List<SingleVariableDeclaration> parameters = null;
		SingleVariableDeclaration parameter = null;
		Vector<MethodBean> methodsCalls = null;
		MethodBean methodCall = null;
		Vector<InstanceVariableBean> variables = null;
		InstanceVariableBean variable = null;
		//System.out.println(classes.get(c).getTextContent());

		System.out.println("METHOD NAME: " + method.getName());
		System.out.println(method.getTextContent());

		parameters = method.getParameters();
		for (int p = 0; p < parameters.size(); p++) {
			parameter = parameters.get(p);
			System.out.println("METHOD Parameter -> " + parameter.getName() + " of type \"" + parameter.getType() + "\"");
		}
		System.out.println("METHOD Return type: \"" + method.getReturnType() + "\"");
		methodsCalls = (Vector<MethodBean>) method.getMethodCalls();
		for (int p = 0; p < methodsCalls.size(); p++) {
			methodCall = methodsCalls.get(p);
			System.out.println("METHOD call to -> " + methodCall.getName());
		}
		variables = (Vector<InstanceVariableBean>) method.getUsedInstanceVariables();
		for (int v = 0; v < variables.size(); v++) {
			variable = variables.get(v);
			System.out.println("METHOD variable -> " + variable.getName());
		}
	}

}

