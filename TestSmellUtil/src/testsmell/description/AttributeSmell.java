package testsmell.description;

public enum AttributeSmell {
	LongMethod,
	EagerTest,
	LongParameterList,
	GeneralFixture;
	
	private static final AttributeSmell[] copyOfValues = values();

    public static boolean isClassSmell(String name) {
        for (AttributeSmell value : copyOfValues) {
            if (value.name().equals(name)) {
                return true;
            }
        }
        return false;
    }
}
