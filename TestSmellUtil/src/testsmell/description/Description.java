package testsmell.description;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import testsmell.constructs.SmellInformation;

public class Description {

	private ST postClassDescription;
	private ST preClassDescription;
	private ST methodDescription;
	private ST classDescription;
	private ST longRefactoring;
	private ST shortRefactoring;
	private String currentSmell;

	public Description() {
		initGeneralDescriptions();
	}

	public Description(String smellToDescribe) {
		setSmell(smellToDescribe);
	}

	public void setSmell(String smellToDescribe) {
		initSmellDescriptions(smellToDescribe);
		currentSmell = smellToDescribe;
	}

	private void initGeneralDescriptions() {
		try {
			STGroup group = new STGroupFile("./descriptions/CommonDescriptions.stg");
			preClassDescription = group.getInstanceOf("preClassDescription");
			postClassDescription = group.getInstanceOf("postClassDescription");
		} catch (IllegalArgumentException ie) {
			System.err.println("Description template files are missing. Please include "
					+ "them in the root folder of your application.");
		}
	}

	private void initSmellDescriptions(String smellToDescribe) {
		STGroup group = null;
		try {
			group = new STGroupFile("./descriptions/" + smellToDescribe + ".stg");
			loadSmellDescriptions(group);
		} catch (IllegalArgumentException ie) {
			System.err.println("Smell description of " + smellToDescribe + " not found.");
			try {
				group = new STGroupFile("./descriptions/NoDescription.stg");
				loadSmellDescriptions(group);
			} catch (IllegalArgumentException iee) {
				System.err.println("Description template files are missing. Please include "
						+ "them in the root folder of your application.");
				System.exit(-1);
			}
		}
	}

	private void loadSmellDescriptions(STGroup group) {
		classDescription = group.getInstanceOf("classDescription");
		methodDescription = group.getInstanceOf("methodDescription");
		longRefactoring = group.getInstanceOf("longRefactoring");
		shortRefactoring = group.getInstanceOf("shortRefactoring");
	}

	public String longRefactoring(int refactoringNumber) {
		return longRefactoring(refactoringNumber, null);
	}

	public String longRefactoring(int refactoringNumber, Object smellSpecificInformation) {
		resetTemplate(this.longRefactoring);
		this.longRefactoring.add(SmellInformation.REFACTORING_NUMBER.toString(), refactoringNumber);
		//		TODO: Uncomment after adding smellSpecificInformation to templates.
		//		this.longRefactoring.add("smellSpecificInformation", smellSpecificInformation);
		return longRefactoring.render();
	}

	public String shortRefactoring(int refactoringNumber) {
		return shortRefactoring(refactoringNumber, null);
	}

	public String shortRefactoring(int refactoringNumber, Object smellSpecificInformation) {
		resetTemplate(this.shortRefactoring);
		this.shortRefactoring.add(SmellInformation.REFACTORING_NUMBER.toString(), refactoringNumber);
		//		TODO: Uncomment after adding smellSpecificInformation to templates.
		//		this.shortRefactoring.add("smellSpecificInformation", smellSpecificInformation);
		return shortRefactoring.render();
	}

	public String preClassDescription(int numberOfSmells) {
		resetTemplate(this.preClassDescription);
		this.preClassDescription.add(SmellInformation.ONLY_ONE.toString(), onlyOne(numberOfSmells));
		return this.preClassDescription.render();
	}

	public String classDescription(Iterator informationIterator) {
		resetTemplate(this.classDescription);
		while (informationIterator.hasNext()) {
			Map.Entry<SmellInformation, Object> pair = (Map.Entry) informationIterator.next();
			try {
				this.classDescription.add(pair.getKey().toString(), pair.getValue());
			} catch (IllegalArgumentException ie) {
			}
		}
		return this.classDescription.render();
	}

	public String methodDescription(Iterator informationIterator) {
		resetTemplate(this.methodDescription);
		while (informationIterator.hasNext()) {
			Map.Entry<SmellInformation, Object> pair = (Map.Entry) informationIterator.next();
			try {
				this.methodDescription.add(pair.getKey().toString(), pair.getValue());
			} catch (IllegalArgumentException ie) {
			}
		}
		
		return this.methodDescription.render();
	}

	public String postClassDescription(
			int numberOfSmellyAttributes,
			String smellInClassToAllInClass,
			String smellInProjectToAllInProject,
			String smellInClassToAllInProject) {
		resetTemplate(this.postClassDescription);
		this.postClassDescription.add(SmellInformation.ONLY_ONE.toString(), onlyOne(numberOfSmellyAttributes));
		this.postClassDescription.add(SmellInformation.SMELL_IN_CLASS_TO_ALL_IN_CLASS.toString(), smellInClassToAllInClass);
		this.postClassDescription.add(SmellInformation.SMELL_IN_PROJECT_TO_ALL_IN_PROJECT.toString(), smellInProjectToAllInProject);
		this.postClassDescription.add(SmellInformation.SMELL_IN_CLASS_TO_ALL_IN_PROJECT.toString(), smellInClassToAllInProject);
		return this.postClassDescription.render();
	}

	private boolean onlyOne(int number) {
		return number == 1;
	}

	private void resetTemplate(ST template) {
		Iterator iter = template.getAttributes().entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, Object> pair = (Map.Entry) iter.next();
			template.remove(pair.getKey());
			iter.remove();
		}
	}
	
}
