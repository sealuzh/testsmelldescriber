package testsmell.description;

public class DescriptionFormatter {

	public static String formatSmellyMethodDescription(String description) {
		String formattedDescription = "\n";
		String[] splitDescription = description.split("\n");
		for (int i = 1; i < splitDescription.length; ++i) {
			formattedDescription += "\t * " + splitDescription[i];
		}
		formattedDescription += "\n\t ";
		return formattedDescription;
	}

	public static String formatSmellyClassDescription(String description) {
		String formattedDescription = "\n/**\n";
		String[] splitDescription = description.split("\n");
		for (String newLineDescription : splitDescription) {
			formattedDescription += "* " + newLineDescription + "\n";
		}
		formattedDescription += "**/";
		return formattedDescription;
	}
}
