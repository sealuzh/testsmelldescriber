package testsmell.description;

public enum ClassSmell {
	LargeClass,
	LazyClass,
	ManyFieldAttributesButNotComplex,
	MessageChains,
	RefusedParentBequest,
	SpeculativeGenerality;
	
	private static final ClassSmell[] copyOfValues = values();

    public static boolean isClassSmell(String name) {
        for (ClassSmell value : copyOfValues) {
            if (value.name().equals(name)) {
                return true;
            }
        }
        return false;
    }
}
