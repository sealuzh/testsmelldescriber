package testsmell.description;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import testsmell.constructs.SmellInformation;
import testsmell.constructs.SmellyAttribute;
import testsmell.constructs.SmellyAttributeContainer;
import testsmell.constructs.SmellyClass;
import testsmell.testdescriber.parser.bean.MethodBean;
import testsmell.util.SmellyClassContainer;
import testsmell.util.SmellyClassParser;

public class TestSmellDescriptionWrapper {
	
	private static SmellyClass currentClass = null;
	private static SmellyAttribute currentAttribute = null;
	private static Description description = new Description();
	private static Map<String, Integer> refactoringNumbers = new HashMap<>();
	private static String calledMethods;
//	private static int numberOfCalledMethods;
//	private static double percentageOfMethods;
	
	private static Map<String, String> smellSpecificInformation = new HashMap<>();
	
	
	public static String getClassDescription(boolean formatDescription) {
		String description = getClassDescription(null);
		return formatDescription ? DescriptionFormatter.formatSmellyClassDescription(description) : description;
	}

	public static boolean classIsSmelly(String className) {
		if (currentClass == null) {
			currentClass = SmellyClassContainer.getInstance().getClass(className);
			return currentClass != null;
		}
		return true;
	}
	
	public static Set getMethodSmells(String methodName) {
		if (currentClass != null) {
			SmellyAttributeContainer attributes = currentClass.getAttributes();
			currentAttribute = attributes.getMethod(methodName);
			return currentAttribute != null ? currentAttribute.getSmells() : null;
		}
		return null;
	}
	
	public static boolean fieldIsSmelly(String fieldName) {
		if (currentClass != null) {
			SmellyAttributeContainer attributes = currentClass.getAttributes();
			currentAttribute = attributes.getField(fieldName);
			return currentAttribute != null;
		}
		return false;
	}
	
	// TODO: Pass class under test from TestDescriber.
	public static String getClassDescription(String classUnderTest) {
		String descriptionOutput = new String();
		if (currentClass != null) {
			descriptionOutput = description.preClassDescription(currentClass.getNumberOfSmells());
			descriptionOutput += getAllClassDescriptions(currentClass.getSmellsIterator(), classUnderTest);
		}
		return descriptionOutput;
	}
	
	private static String getAllClassDescriptions(Iterator smells, String classUnderTest) {
		String descriptionOutput = new String();

		int allSmellOccurencesInClass = currentClass.getAllSmellOccurencesInClass();
		int allSmellOccurencesInProject = SmellyClassParser.getAllSmellOccurencesInProject();
		
		while (smells.hasNext()) {
			Map.Entry pair = (Map.Entry) smells.next();
			
			String smellName = (String) pair.getKey();
			int thisSmellOccurrencesInClass = (int) pair.getValue();
			
			description.setSmell(smellName);
			
			descriptionOutput += getClassSmellDescription(thisSmellOccurrencesInClass, classUnderTest, smellName);
			descriptionOutput += getClassRefactoringDescription(smellName);
			descriptionOutput += getQuantitativeDescription(smellName, thisSmellOccurrencesInClass, allSmellOccurencesInClass, allSmellOccurencesInProject);
		}
		return descriptionOutput;
	}
	
	private static String getClassSmellDescription(int thisSmellOccurrencesInClass, String classUnderTest, String smellName) {
		Map<SmellInformation, Object> information = new HashMap<>();
		information.put(SmellInformation.NUMBER_OF_METHODS, Integer.toString(thisSmellOccurrencesInClass));
		information.put(SmellInformation.ONLY_ONE, thisSmellOccurrencesInClass == 1);
		information.put(SmellInformation.CLASS_UNDER_TEST, classUnderTest);
		information.put(SmellInformation.METHOD_NAMES, currentClass.getAttributesForSmell(smellName));
		return description.classDescription(information.entrySet().iterator());
	}
	
	private static String getClassRefactoringDescription(String smellName) {
		Integer refactoringNumber = addRefactoringIfNotExist(smellName);
		return description.longRefactoring(refactoringNumber);
	}
	
	private static String getQuantitativeDescription(String smellName, int thisSmellOccurrencesInClass, int allSmellOccurencesInClass, int allSmellOccurencesInProject) {
		int thisSmellOccurencesInProject = SmellyClassParser.getSmellOccurencesInProject(smellName);
		
		double smellInClassToAllInClass = calcComparison(thisSmellOccurrencesInClass, allSmellOccurencesInClass);
		double smellInProjectToAllInProject = calcComparison(thisSmellOccurencesInProject, allSmellOccurencesInProject);
		double smellInClassToAllInProject = calcComparison(thisSmellOccurrencesInClass, thisSmellOccurencesInProject);
		
		return description.postClassDescription(
				thisSmellOccurrencesInClass, doubleToString(smellInClassToAllInClass),
				doubleToString(smellInProjectToAllInProject), doubleToString(smellInClassToAllInProject));
	}
	
	private static double calcComparison(int numerator, int denominator) {
		double comparison = (double) (numerator) / denominator;
		double comparisonAsPercentage = 100 * comparison;
		return comparisonAsPercentage;
	}
	
	private static String doubleToString(double number) {
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String numberAsString = decimalFormat.format(number);
		return numberAsString;
	}
	
	public static String getAttributeDescription(boolean formatDescription) {
		if (currentAttribute != null) {
			String description = getAllAttributeDescriptions(currentAttribute.getSmellsIterator());
			return formatDescription ? DescriptionFormatter.formatSmellyMethodDescription(description) : description;
		}
		return null;
	}
	
	private static String getAllAttributeDescriptions(Iterator smellsIterator) {
		String descriptionOutput = new String();
		while (smellsIterator.hasNext()) {
			String smell = (String) smellsIterator.next();
			descriptionOutput += getAttributeDescriptionForSmell(smell);
		}
		return descriptionOutput;
	}

	
	// TODO: Pass class under test from TestDescriber.
	private static String getAttributeDescriptionForSmell(String smellName) {
		String descriptionOutput = new String();
		description.setSmell(smellName);
		int refactoringNumber = addRefactoringIfNotExist(smellName);
		descriptionOutput += description.methodDescription(currentAttribute.getSmellInformation(smellName));
		descriptionOutput += description.shortRefactoring(refactoringNumber);
		return descriptionOutput;
	}
	
	public static void setCurrentClass(String className) {
		reset();
		currentClass = SmellyClassContainer.getInstance().getClass(className);
	}
	
	private static int addRefactoringIfNotExist(String smellName) {
		Integer refactoringNumber = refactoringNumbers.get(smellName);
		if (refactoringNumber == null) {
			refactoringNumber = refactoringNumbers.size();
			refactoringNumbers.put(smellName, refactoringNumber);
		}
		return refactoringNumber;
	}
	
	private static void reset() {
		refactoringNumbers = new HashMap<>();
	}
	
	public static void parseEagerTestValues(Iterator methodsInOriginalClass) {
		Iterator iter = methodsInOriginalClass;
		int numberOfMethodsInOriginalClass = 0;
		int numberOfCalledMethods = 0;
		while (iter.hasNext()) {
			MethodBean method = (MethodBean) iter.next();
			String methodName = method.getName();
			numberOfMethodsInOriginalClass++;
			if (calledMethods.contains(methodName)) {
				numberOfCalledMethods++;
			}
		}
		double percentageOfMethods = numberOfCalledMethods * 100 / numberOfMethodsInOriginalClass;
		currentAttribute.addSmellInformation("EagerTest", SmellInformation.PERCENTAGE_OF_METHODS, doubleToString(percentageOfMethods));
		currentAttribute.addSmellInformation("EagerTest", SmellInformation.NUMBER_OF_METHODS, Integer.toString(numberOfCalledMethods));
	}
	
	public static void parseCalledMethods(String smell, Iterator methodCalls) {
		Iterator iter = methodCalls;
		calledMethods = "(";
		while (iter.hasNext()) {
			MethodBean method = (MethodBean) iter.next();
			String methodName = method.getName();
			if (!methodName.contains("assert")) {
				calledMethods += methodName;
				if (iter.hasNext()) {
					calledMethods += ", ";
				}
			}
		}
		calledMethods += ")";
		currentAttribute.addSmellInformation(smell, SmellInformation.CALLED_METHODS, calledMethods);
	}
	
	public static void passParameters(String parameters) {
		currentAttribute.addSmellInformation("LongParameterList", SmellInformation.PARAMETERS, parameters);
	}

}
