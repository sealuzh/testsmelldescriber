package testsmell.description;

public enum TestSmell {
	
	/*
	 * DECOR Code Smells
	 */
	AntiSingleton,
	BaseClassKnowsDerivedClass,
	BaseClassShouldBeAbstract,
	Blob,
	ClassDataShouldBePrivate,
	ComplexClass,
	FunctionalDecomposition,
	LargeClass,
	LazyClass,
	LongMethod,
	LongParameterList,
	ManyFieldAttributesButNotComplex,
	MessageChains,
	RefusedParentBequest,
	SpaghettiCode,
	SpeculativeGenerality,
	SwissArmyKnife,
	TraditionBreaker,
	
	/*
	 * TACO Test Smells
	 */
	GeneralFixture,
	EagerTest;
	
}
