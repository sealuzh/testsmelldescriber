package testsmell.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import testsmell.constructs.Smell;

public class SmellOccurence {

	private static SmellOccurence UniqueInstance;
	private HashMap<String, Smell> smells;
	private String FILE_NAME = "All Smell Occurences";

	public static SmellOccurence getInstance() {
		if (SmellOccurence.UniqueInstance == null) {
			SmellOccurence.UniqueInstance = new SmellOccurence();
		}
		return SmellOccurence.UniqueInstance;
	}

	private SmellOccurence() {
		this.smells = new HashMap<String, Smell>();
	}

	private Smell addSmell(String smellName) {
		Smell value = smells.get(smellName);
		if (value != null) {
			return value;
		}
		smells.put(smellName, new Smell(smellName));
		return smells.get(smellName);
	}

	public void increaseOccurence(String smellName, String projectName) {
		Smell value = addSmell(smellName);
		value.increaseOccurence(projectName);
	}

	public void printOccurence(String outputDirectory) {

		System.out.println("Printing Occurence of " + outputDirectory);
//		DataTable dataTable = new DataTable();
//		dataTable.addColumn("#OfOccurences");
//		dataTable.addColumn("#OfProjects");
//		Iterator iter = smells.entrySet().iterator();
//		while (iter.hasNext()) {
//			Map.Entry<String, Smell> pair = (Map.Entry<String, Smell>) iter.next();
//			Smell currentSmell = (Smell) pair.getValue();
//			String[] val = new String[] { currentSmell.getOccurence().toString(),
//					String.valueOf(currentSmell.getProjects().size()) };
//			dataTable.addRow((String) pair.getKey(), val);
//		}
//		String output = new String();
//		for (ArrayList<Object> v : dataTable.getTable()) {
//			for (Object s : v) {
//				if (s != null) {
//					output += (String) s + ";";
//				} else {
//					output += ";";
//				}
//			}
//			output += "\n";
//		}
		
		String output = ";#OfOccurences;#OfProjects;\n";
		Iterator iter = smells.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, Smell> pair = (Map.Entry<String, Smell>) iter.next();
			Smell currentSmell = (Smell) pair.getValue();
			output += pair.getKey() + ";";
			output += currentSmell.getOccurence().toString() + ";";
			output += currentSmell.getProjects().size() + ";";
			Iterator innerIter = currentSmell.getProjects().iterator();
			while (innerIter.hasNext()) {
				output += (String) innerIter.next() + ", ";
			}
			output += "\n";
		}
		
		CSVWriter.writeToFile(output, outputDirectory + "/../" + FILE_NAME + ".csv");
	}

}
