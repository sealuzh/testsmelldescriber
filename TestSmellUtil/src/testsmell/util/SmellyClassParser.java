package testsmell.util;

import java.util.Iterator;

import testsmell.constructs.SmellyClass;

public class SmellyClassParser {

	public static int getAllSmellOccurencesInProject() {
		int allSmellOccurencesInProject = 0;
		Iterator iter = SmellyClassContainer.getInstance().getSmellyClassesIterator();
		SmellyClass smellyClass;
		while (iter.hasNext()) {
			smellyClass = (SmellyClass) iter.next();
			allSmellOccurencesInProject += smellyClass.getAllSmellOccurencesInClass();
		}
		return allSmellOccurencesInProject;
	}
	
	public static int getSmellOccurencesInProject(String smellName) {
		int smellOccurencesInProject = 0;
		Iterator iter = SmellyClassContainer.getInstance().getSmellyClassesIterator();
		SmellyClass smellyClass;
		while (iter.hasNext()) {
			smellyClass = (SmellyClass) iter.next();
			Integer occurrence = smellyClass.getSmellOccurence(smellName);
			if (occurrence != null) {
				smellOccurencesInProject += smellyClass.getSmellOccurence(smellName);
			}
		}
		return smellOccurencesInProject;
	}
}
