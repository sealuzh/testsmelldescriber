package testsmell.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import testsmell.constructs.SmellyClass;
import testsmell.description.ClassSmell;

public class SmellyClassContainer {

	private ArrayList<SmellyClass> smellyClasses;
	private SmellyClass currentClass;
	private static SmellyClassContainer Instance = null;
	private String currentSmell;
	
	private SmellyClassContainer() {
		smellyClasses = new ArrayList<>();
	}
	
	public static SmellyClassContainer getInstance() {
		if (Instance == null) {
			Instance = new SmellyClassContainer();
		}
		
		return Instance;
	}
	
	public void addClass(String className) {
		currentClass = getClass(className);
		if (currentClass == null) {
			currentClass = new SmellyClass(className);
			smellyClasses.add(currentClass);
		}
	}
	
	public void setClassSmelly(String smellName) {
		this.currentSmell = smellName;
		if (ClassSmell.isClassSmell(currentSmell)) {
			currentClass.addClassSmell(currentSmell);
		}
	}
	
	public void addAttribute(String attributeType, String attributeName, String smellName) {
		if (attributeType.equals("method")) {
			addMethod(attributeName, null, smellName);
		} else {
			addField(attributeName, smellName);
		}
	}
	
	public void addMethod(String methodName) {
		currentClass.addMethod(methodName, null, this.currentSmell);
	}
	
	public void addField(String fieldName) {
		currentClass.addField(fieldName, this.currentSmell);
	}
	
	public void addMethod(String methodName, Map smellSpecificValues, String smellName) {
		currentClass.addMethod(methodName, smellSpecificValues, smellName);
	}
	
	public void addField(String fieldName, String smellName) {
		currentClass.addField(fieldName, smellName);
	}
	
	public SmellyClass getClass(String className) {
		Iterator iter = smellyClasses.iterator();
		SmellyClass smellyClass;
		while (iter.hasNext()) {
			smellyClass = (SmellyClass) iter.next();
			if (smellyClass.getName().equals(className)) {
				return smellyClass;
			}
		}
		return null;
	}
	
	
	public void removeLastClass() {
		smellyClasses.remove(smellyClasses.size() - 1);
		currentClass = null;
	}
	
	public void print() {
		final StringBuffer buffer = new StringBuffer();
		Iterator iter = smellyClasses.iterator();
		while (iter.hasNext()) {
			SmellyClass smellyClass = (SmellyClass) iter.next();
			buffer.append(smellyClass.toString());
		}
		System.out.println(buffer.toString());
		CSVWriter.writeToFile(buffer.toString(), "./../SmellyClasses.ini");
	}
	
	public Iterator getSmellyClassesIterator() {
		return smellyClasses.iterator();
	}

}
