package testsmell.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataTable {

	protected ArrayList<ArrayList<Object>> table;
	protected int currentRow = 0;
	protected int currentColumn = 0;

	public DataTable() {
		initialize();
	}

	public ArrayList<ArrayList<Object>> getTable() {
		return this.table;
	}

	/**
	 * Initialize the data table.
	 */
	public void initialize() {
		this.table = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> v = new ArrayList<Object>();
		v.add(null);
		this.table.add(v);
	}

	/**
	 * Removes the last added column.
	 */
	public void removeLastColumn() {
		int index = this.table.get(0).size() - 1;
		for (int i = 0; i < this.table.size(); ++i) {
			this.table.get(i).remove(index);
		}
	}

	/**
	 * Removes the last added row.
	 */
	public void removeLastRow() {
		this.table.remove(this.table.size() - 1);
	}

	/**
	 * Set the value at the specified cell.
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @param value
	 */
	protected void setCell(int rowIndex, int columnIndex, Object value) {
		this.table.get(rowIndex).set(columnIndex, value);
	}
	
	protected void addCell(int rowIndex, int columnIndex, Object value) {
		Integer sum = Integer.valueOf((String) value);
		Object oldValue = this.table.get(rowIndex).get(columnIndex);
		if (oldValue != null) {
			Integer oldValueAsNumber = Integer.valueOf((String) oldValue);
			sum += oldValueAsNumber;
		}
		setCell(rowIndex, columnIndex, sum.toString());
	}
	
	public Object getCurrentColumnHeader() {
		return this.table.get(currentRow).get(0);
	}
	
	public Object getCurrentRowHeader() {
		return this.table.get(0).get(currentColumn);
	}
	
	public void setCurrentCell(Object value) {
		setCell(currentRow, currentColumn, value);
	}
	
	public Object getCurrentCell() {
		return this.table.get(currentRow).get(currentColumn);
	}

	/**
	 * Initialize a new column. Sets the name of the column.
	 * 
	 * @param columnName
	 */
	private void initializeColumn(String columnName) {
		this.table.get(0).add(columnName);
		for (int i = 1; i < this.table.size(); ++i) {
			this.table.get(i).add(null);
		}
	}

	/**
	 * Initialize a new row. Sets the name of the row.
	 * 
	 * @param rowName
	 */
	private void initializeRow(Object rowName) {
		ArrayList<Object> newArrayList = new ArrayList<Object>();
		newArrayList.add(rowName);
		for (int i = 1; i < this.table.get(0).size(); ++i) {
			newArrayList.add(null);
		}
		this.table.add(newArrayList);
	}

	/**
	 * Tries to add row to the table. If a value is declared, the row might already
	 * exist in the table.
	 * 
	 * @param rowName
	 */
	public boolean addRow(String rowName, Object value) {
		// class -> smell
		// also means class hasnt been added yet
		if (value == null) {
			initializeRow(rowName);
			currentRow = table.size() - 1;
			return false;
		} else {
			// smell -> class
			// class could have been added
			// class is smelly
			currentRow = -1;
			for (int i = 1; i < this.table.size(); ++i) {
				if (this.table.get(i).get(0).equals(rowName)) {
					currentRow = i;
					addCell(currentRow, this.table.get(0).size() - 1, value);
					break;
				}
			}
			if (currentRow == -1) {
				initializeRow(rowName);
				currentRow = this.table.size() - 1;
				setCell(currentRow, this.table.get(0).size() - 1, value);
			}
			
			return currentRow > -1;
		}
	}

	/**
	 * Adds a new row to the table.
	 * 
	 * @param rowName
	 */
	public boolean addRow(String rowName) {
		return addRow(rowName, null);
	}

	/**
	 * Tries to add column to the table. If a value is declared, the column might
	 * already exist in the table.
	 * 
	 * @param columnName
	 */
	public boolean addColumn(String columnName, Object value) {
		// smell -> class
		if (value == null) {
			initializeColumn(columnName);
			currentColumn = this.table.get(0).size() - 1;
			return false;
		} else {
			// class -> smell
			// smell could have been added
			// smell has smelly class
			currentColumn = -1;
			for (int i = 1; i < this.table.get(0).size(); ++i) {
				if (this.table.get(0).get(i).equals(columnName)) {
					currentColumn = i;
					addCell(this.table.size() - 1, currentColumn, value);
					break;
				}
			}
			if (currentColumn == -1) {
				initializeColumn(columnName);
				currentColumn = this.table.get(0).size() - 1;
				setCell(this.table.size() - 1, currentColumn, value);
			}
			
			return currentColumn > -1;
		}
	}

	/**
	 * Adds a new column to the table.
	 * 
	 * @param columnName
	 */
	public boolean addColumn(String columnName) {
		return addColumn(columnName, null);
	}
	
	public void print() {
		String output = new String();
		Iterator iter = this.table.iterator();
		while(iter.hasNext()) {
			List list = (List) iter.next();
			Iterator innerIter = list.iterator();
			while(innerIter.hasNext()) {
				Object innerElement = innerIter.next();
				if (innerElement == null) {
					output += ";";
				} else {
					output += innerElement.toString();
				}
				output += ";";
			}
			output += "\n";
		}
		System.out.println(output);
	}

}
