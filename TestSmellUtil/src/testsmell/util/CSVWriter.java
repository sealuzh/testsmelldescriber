package testsmell.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class CSVWriter {

	/**
	 * Writes to a CSV file.
	 * 
	 * @param pContent
	 *            - content to write to the CSV file
	 * @param anOutputFile
	 *            - file path
	 */
	public static void writeToFile(String pContent, String anOutputFile) {
		try {
			File file = new File(anOutputFile);
			FileWriter fstream = new FileWriter(file);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(pContent);
			out.close();
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
	}
}
