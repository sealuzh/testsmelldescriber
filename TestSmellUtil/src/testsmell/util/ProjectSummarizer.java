package testsmell.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import testsmell.constructs.SmellyAttributeContainer;
import testsmell.constructs.SmellyClass;

public class ProjectSummarizer extends DataTable {

	private static ProjectSummarizer Instance;
	private ArrayList<Integer> numberOfSmellsPerClass;
	private ArrayList<Integer> numberOfClassesPerSmell;
	private int totalAmountOfSmellsFound;
	private ArrayList<ArrayList<Object>> table;
	
	private ProjectSummarizer() {
	}

	public static ProjectSummarizer getInstance() {
		if (ProjectSummarizer.Instance == null) {
			ProjectSummarizer.Instance = new ProjectSummarizer();
		}

		return ProjectSummarizer.Instance;
	}

	public static void createInstance() {
		ProjectSummarizer.Instance = new ProjectSummarizer();
	}
	

	/**
	 * Initiates the printing of the summary to a CSV file.
	 * 
	 * @param projectName
	 *            - name of the project
	 * @param outputDirectory
	 *            - directory where to save the file
	 */
	public void printSummary(final String projectName, final String outputDirectory) {
		String output = sumCountsAndProduceOutput();
		CSVWriter.writeToFile(output, outputDirectory + "/Summary for " + projectName + ".csv");
	}
	
	public void parse() {
		prepareCount();
		sumCounts();
		calculateTotalAmountOfSmells();
	}
	
	private int getSmellIndex(String smellName) {
		List list = getTable().get(0);
		for (int i = 1; i < list.size(); ++i) {
			if (list.get(i).equals(smellName)) {
				return i;
			}
		}
		return -1;
	}
	
	private int getClassIndex(String smellName) {
		List list = getTable();
		for (int i = 1; i < list.size(); ++i) {
			List innerList = (List) list.get(i);
			if (innerList.get(0).equals(smellName)) {
				return i;
			}
		}
		return -1;
	}
	
	public double calcSmellOccurence(String smellName) {
		int smellIndex = getSmellIndex(smellName);
		int smellOccurence = numberOfSmellsPerClass.get(smellIndex - 1);
		return (double) smellOccurence / totalAmountOfSmellsFound;
	}
	
	public double calcClassOccurence(String className) {
		int classIndex = getClassIndex(className);
		int classOccurence = numberOfClassesPerSmell.get(classIndex - 1);
		return (double) classOccurence / totalAmountOfSmellsFound;
	}
	
	private void sumCounts() {
		for (int i = 0; i < table.size(); ++i) {
			for (int j = 0; j < table.get(i).size(); ++j) {
				String currentElement = (String) table.get(i).get(j);
				if (i > 0 && j > 0) {
					currentElement = currentElement == null ? "0" : "1";
					if (Integer.parseInt(currentElement) != 0) {
						numberOfSmellsPerClass.set(i - 1, numberOfSmellsPerClass.get(i - 1) + 1);
						numberOfClassesPerSmell.set(j - 1, numberOfClassesPerSmell.get(j - 1) + 1);
					}
				}
			}
		}
	}
	
	private void prepareCount() {
		numberOfSmellsPerClass = new ArrayList<Integer>();
		numberOfClassesPerSmell = new ArrayList<Integer>();

		table = getTable();

		for (int i = 0; i < table.get(0).size() - 1; ++i) {
			numberOfClassesPerSmell.add(0);
		}

		for (int i = 0; i < table.size() - 1; ++i) {
			numberOfSmellsPerClass.add(0);
		}
	}
	
	private void calculateTotalAmountOfSmells() {
		totalAmountOfSmellsFound = 0;
		for (int i = 0; i < numberOfClassesPerSmell.size(); ++i) {
			Integer currentCount = numberOfClassesPerSmell.get(i);
			totalAmountOfSmellsFound += currentCount;
		}
	}

	/**
	 * Summarizes the number of occurrences per smell and number of smell
	 * occurrences per class. Writes the summary to a String.
	 * 
	 * @return output - summary of table
	 */
	private String sumCountsAndProduceOutput() {
		String output = new String();
		numberOfSmellsPerClass = new ArrayList<Integer>();
		numberOfClassesPerSmell = new ArrayList<Integer>();

		ArrayList<ArrayList<Object>> table = getTable();

		for (int i = 0; i < table.get(0).size() - 1; ++i) {
			numberOfClassesPerSmell.add(0);
		}

		for (int i = 0; i < table.size() - 1; ++i) {
			numberOfSmellsPerClass.add(0);
		}

		for (int i = 0; i < table.size(); ++i) {
			for (int j = 0; j < table.get(i).size(); ++j) {
				String currentElement = (String) table.get(i).get(j);
				if (i == 0 || j == 0) {
					output += currentElement + ";";
				} else {
					currentElement = currentElement == null ? "0" : currentElement;
					output += currentElement + ";";
					int currentElementAsNumber = Integer.parseInt(currentElement);
					if (currentElementAsNumber != 0) {
						numberOfSmellsPerClass.set(i - 1, numberOfSmellsPerClass.get(i - 1) + currentElementAsNumber);
						numberOfClassesPerSmell.set(j - 1, numberOfClassesPerSmell.get(j - 1) + currentElementAsNumber);
					}
				}
			}
			if (i == 0) {
				output += "Total;";
			} else {
				output += numberOfSmellsPerClass.get(i - 1).toString();
			}
			output += "\n";
		}
		output += ";";

		/*
		 * Bottom part of the table showing the number of classes found to have the
		 * smell. Sums the total amount.
		 */
		Integer totalSum = 0;
		for (int i = 0; i < numberOfClassesPerSmell.size(); ++i) {
			Integer currentCount = numberOfClassesPerSmell.get(i);
			output += currentCount.toString() + ";";
			totalSum += currentCount;
		}
		output += totalSum.toString() + ";\n\n";

		/*
		 * Small table depicting the number of smells found per class
		 */
		output += ";NumberOfSmells;\n";
		for (int i = 0; i < numberOfSmellsPerClass.size(); ++i) {
			output += table.get(i + 1).get(0) + ";" + numberOfSmellsPerClass.get(i).toString() + ";" + "\n";
		}
		output += ";" + totalSum.toString();

		return output;
	}

}
