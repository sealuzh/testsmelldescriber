package testsmell.describer;

import java.util.Scanner;

import testsmell.runner.TestSmellRunner;
import testsmell.testdescriber.TestDescriber;
import testsmell.util.SmellyClassContainer;

public class Main {
	
	public static void main(String args[]) {		
		if (args.length == 5) {
			execute(args);
		} else {
			requestInformation();
		}
	}
	
	private static void requestInformation() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please provide the following information.");
		
		System.out.println("Please enter the path to the source files of the to be examined project.");
		final String projectSourcePath = scanner.nextLine();
		
		System.out.println("Please enter the path to the binary files of the to be examined project.");
		final String projectBinaryPath = scanner.nextLine();

		System.out.println("Please enter the package path of the to be described test class seperated by '/'.");
		final String testFile = scanner.nextLine();
		
		System.out.println("Please enter the package path of the by the test class tested production class seperated by '/'.");
		final String classFile = scanner.nextLine();
		
		System.out.println("Please enter the bytecode file type of the binary files (\"jar\" or \"class\").");
		final String binaryType = scanner.nextLine();
		
		scanner.close();
		
		execute(new String[] { projectSourcePath, projectBinaryPath, testFile, classFile, binaryType });
	}
	
	private static void execute(String args[]) {
		final String projectSourcePath = args[0];
		final String projectBinaryPath = args[1];
		final String testFile = args[2];
		final String classFile = args[3];
		final String binaryType = args[4];
		
		TestSmellRunner.run("textual", projectSourcePath, "../../TestSmellOutput/");
		TestSmellRunner.run("structural", projectBinaryPath, "../../TestSmellOutput/", binaryType);
		SmellyClassContainer.getInstance().print();
		
		try {
			TestDescriber.main(new String[] { projectSourcePath, projectBinaryPath, classFile, testFile });	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
