package nl.tudelft.runner.complexComponentsDetection;

import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.LongMethodRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.structuralMetrics.CKMetrics;

public class LongMethodDetectionRunner {

	public static void main(String args[]) {
		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert("/Users/fabiopalomba/Desktop/ant");
		LongMethodRule detectionRule = new LongMethodRule();
		String outputFile = "package-name;class-name;method-name;loc;probability\n";

		for(PackageBean packageBean: packages) {
			for(ClassBean classBean: packageBean.getClasses()) {
				for(MethodBean methodBean: classBean.getMethods()) {
					
					System.out.println(packageBean.getName()+";"+classBean.getName()+";"+methodBean.getName()+";" 
							+ CKMetrics.getLOC(methodBean)+";"+detectionRule.getLongMethodProbability(methodBean));
					
					outputFile+=packageBean.getName()+";"+classBean.getName()+";"+methodBean.getName()+";" 
							+ CKMetrics.getLOC(methodBean)+";"+detectionRule.getLongMethodProbability(methodBean)+"\n";
					
					
				}
			}
		}

		FileUtility.writeFile(outputFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/ant-results-longMethod.csv");
	}
}