package nl.tudelft.runner.complexComponentsDetection;

import java.util.Vector;

import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.PromiscuousPackageRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.structuralMetrics.CKMetrics;

public class PromiscuousPackageDetectionRunner {

	public static void main(String args[]) {
		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert("/Users/fabiopalomba/Desktop/ant");
		PromiscuousPackageRule detectionRule = new PromiscuousPackageRule();
		String outputFile = "package-name;classes;probability\n";

		for(PackageBean packageBean: packages) {
			if(CKMetrics.getNumberOfClasses(packageBean) > 10) {
				outputFile+=packageBean.getName()+";"+CKMetrics.getNumberOfClasses(packageBean)+";" 
						+detectionRule.getPromiscuousPackageProbability(packageBean)+"\n";
			}
		}

		FileUtility.writeFile(outputFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/ant-results-promiscuousPackage.csv");
	}
}