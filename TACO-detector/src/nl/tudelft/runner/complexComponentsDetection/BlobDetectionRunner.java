package nl.tudelft.runner.complexComponentsDetection;

import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.BlobRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.structuralMetrics.CKMetrics;

public class BlobDetectionRunner {

	public static void main(String args[]) {
		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert("/Users/fabiopalomba/Desktop/ant");
		BlobRule detectionRule = new BlobRule();
		String outputFile = "package-name;class-name;methods;loc;lcom;probability\n";
		
		for(PackageBean packageBean: packages) {
			for(ClassBean classBean: packageBean.getClasses()) {
				outputFile+=packageBean.getName()+";"+classBean.getName()+";"+CKMetrics.getNOM(classBean)+";" 
						+ CKMetrics.getLOC(classBean)+";"+CKMetrics.getLCOM(classBean)+";"+detectionRule.getBlobProbability(classBean)+"\n";
			}
		}
		
		FileUtility.writeFile(outputFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/ant-results-blob.csv");
	}
}
