package nl.tudelft.runner.testSmellDetection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.smellynessMetricProcessing.SmellynessMetric;
import nl.tudelft.testMutation.TestCaseMutation;
import nl.tudelft.testMutation.TestMutationUtilities;

public class EagerTestDetectionRunner {

	public static void main(String args[]) {
		TestCaseMutation filter = new TestCaseMutation();
		TestMutationUtilities utilities = new TestMutationUtilities();
		SmellynessMetric smellyness=new SmellynessMetric();

		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert("/Users/fabiopalomba/Desktop/ant");
		ArrayList<ClassBean> testSuites = utilities.getTestCases(packages);

		String csvFile="test-case-package-name;test-case-name;smellyness\n";

		System.out.println(testSuites.size() + " test cases found.");

		int index=1;
		for(ClassBean testSuite: testSuites) {
			System.out.println(index + "/" + testSuites.size());
			
			
			for(MethodBean testCase: testSuite.getMethods()) {
				String mutatedTest = filter.alterTestCase(testCase, packages);
				
				try {

					csvFile+=testSuite.getBelongingPackage()+";"+testCase.getName()+";"+smellyness.computeSmellyness(mutatedTest)+"\n";

				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			
			index++;
		}

		FileUtility.writeFile(csvFile, "/Users/fabiopalomba/Desktop/ant-results-eagerTest.csv");
	}
}
