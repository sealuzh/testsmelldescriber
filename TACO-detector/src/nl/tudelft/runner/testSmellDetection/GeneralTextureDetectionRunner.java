package nl.tudelft.runner.testSmellDetection;

import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.testSmellDetection.GeneralTextureRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.structuralMetrics.CKMetrics;
import nl.tudelft.testMutation.TestMutationUtilities;

public class GeneralTextureDetectionRunner {

	public static void main(String args[]) {
		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert("/Users/fabiopalomba/Desktop/ant");
		GeneralTextureRule detectionRule = new GeneralTextureRule();
		String outputFile = "package-name;class-name;methods;.setUp-method-loc;is-general-texture\n";

		for(PackageBean packageBean: packages) {
			for(ClassBean classBean: packageBean.getClasses()) {
				MethodBean setUpMethod = TestMutationUtilities.getSetUpMethod(classBean);

				if(setUpMethod != null) {
					System.out.println(packageBean.getName()+";"+classBean.getName()+";"+CKMetrics.getNOM(classBean)+";" 
							+ CKMetrics.getLOC(setUpMethod)+";"+detectionRule.isGeneralTexture(classBean));

					outputFile+=packageBean.getName()+";"+classBean.getName()+";"+CKMetrics.getNOM(classBean)+";" 
							+ CKMetrics.getLOC(setUpMethod)+";"+detectionRule.isGeneralTexture(classBean)+"\n";
				}
			}
		}

		FileUtility.writeFile(outputFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/ant-results-general-texture.csv");
	}
}