package nl.tudelft.runner.misplacedComponentsDetection;

import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.ComparablePackageBean;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.MisplacedClassRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.structuralMetrics.CKMetrics;
import nl.tudelft.testMutation.TestMutationUtilities;

public class MisplacedClassDetectionRunner {

	public static void main(String args[]) {
		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert("/Users/fabiopalomba/Desktop/ant");
		MisplacedClassRule detectionRule = new MisplacedClassRule();
		String outputFile = "package-name;class-name;envied-package;similarity-with-envied-class;dependecies-with-actual-package;dependecies-with-suggested-package;\n";

		for(PackageBean packageBean: packages) {
			if(! TestMutationUtilities.isTestPackage(packageBean)) {
				for(ClassBean classBean: packageBean.getClasses()) {
					ComparablePackageBean enviedPackage = detectionRule.getEnviedPackage(classBean, packages);
					double numberOfDependenciesWithActualPackage = CKMetrics.getNumberOfDependencies(classBean, packageBean);
					double numberOfDependenciesWithEnviedPackage = CKMetrics.getNumberOfDependencies(classBean, enviedPackage);

					//System.out.println(packageBean.getName()+";"+classBean.getName()+";"+ enviedPackage.getName() + ";" + enviedPackage.getSimilarity() + ";" + 
						//	numberOfDependenciesWithActualPackage + ";" + numberOfDependenciesWithEnviedPackage);

					outputFile+=packageBean.getName()+";"+classBean.getName()+";"+ enviedPackage.getName() + ";" + enviedPackage.getSimilarity() + ";" + 
							numberOfDependenciesWithActualPackage + ";" + numberOfDependenciesWithEnviedPackage + "\n";
				}
			}
		}

		FileUtility.writeFile(outputFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/ant-results-misplacedClass.csv");
	}
}