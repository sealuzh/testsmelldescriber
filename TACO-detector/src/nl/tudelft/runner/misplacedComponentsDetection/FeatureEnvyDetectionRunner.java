package nl.tudelft.runner.misplacedComponentsDetection;

import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.ComparableClassBean;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.FeatureEnvyRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.structuralMetrics.CKMetrics;
import nl.tudelft.testMutation.TestMutationUtilities;

public class FeatureEnvyDetectionRunner {

	public static void main(String args[]) {
		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert("/Users/fabiopalomba/Desktop/ant");
		FeatureEnvyRule detectionRule = new FeatureEnvyRule();
		String outputFile = "package-name;class-name;method-name;envied-package-name;envied-class;similarity-with-envied-class;dependecies-with-actual-package;dependecies-with-suggested-package;\n";

		for(PackageBean packageBean: packages) {
			if(! packageBean.getName().contains("test")) {
				for(ClassBean classBean: packageBean.getClasses()) {
					if(classBean.getMethods().size() < 15) {
						if(! TestMutationUtilities.isTestCase(classBean)) {
							for(MethodBean methodBean: classBean.getMethods()) {

								ComparableClassBean enviedClass = detectionRule.getEnviedClass(methodBean, packages);

								double numberOfDependenciesWithActualClass = CKMetrics.getNumberOfDependencies(methodBean, classBean);

								if(enviedClass == null) {
									
									outputFile+=packageBean.getName()+";"+classBean.getName()+";"+ packageBean.getName() + ";" + classBean.getName() + ";0.666666666666;" 
											+ numberOfDependenciesWithActualClass + ";" + numberOfDependenciesWithActualClass + "\n";
									
								} else {

									double numberOfDependenciesWithEnviedClass = CKMetrics.getNumberOfDependencies(methodBean, enviedClass);

								//	System.out.println(packageBean.getName()+";"+classBean.getName()+";"+methodBean.getName()+";"+enviedClass.getBelongingPackage() + ";" + enviedClass.getName() + ";" + 
									//		enviedClass.getSimilarity() + ";" + numberOfDependenciesWithActualClass + ";" + numberOfDependenciesWithEnviedClass);

									outputFile+=packageBean.getName()+";"+classBean.getName()+";"+ enviedClass.getBelongingPackage() + ";" + enviedClass.getName() + ";" + 
											enviedClass.getSimilarity() + ";" + numberOfDependenciesWithActualClass + ";" + numberOfDependenciesWithEnviedClass + "\n";
								}

							}
						}
					}
				}
			}

			FileUtility.writeFile(outputFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/ant-results-featureEnvy.csv");
		}
	}
}