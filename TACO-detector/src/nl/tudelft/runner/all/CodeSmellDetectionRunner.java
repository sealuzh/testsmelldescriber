package nl.tudelft.runner.all;

import java.io.File;
import java.time.ZonedDateTime;
import java.util.Vector;

import nl.Config;
import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.decor.complexComponentsDetection.StructuralBlobRule;
import nl.tudelft.codeSmellDetection.decor.complexComponentsDetection.StructuralLongMethodRule;
import nl.tudelft.codeSmellDetection.decor.complexComponentsDetection.StructuralPromiscuousPackageRule;
import nl.tudelft.codeSmellDetection.decor.misplacedComponentsDetection.StructuralFeatureEnvyRule;
import nl.tudelft.codeSmellDetection.decor.misplacedComponentsDetection.StructuralMisplacedClassRule;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.BlobRule;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.LongMethodRule;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.PromiscuousPackageRule;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.FeatureEnvyRule;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.MisplacedClassRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.structuralMetrics.CKMetrics;

public class CodeSmellDetectionRunner {

	public static void main(String args[]) {
		File systemsDirectory = new File(Config.PROJECT_PATH);
		System.out.println("adsfads");
		for (File release : systemsDirectory.listFiles()) {
			if (release.isDirectory() && (!release.isHidden())) {
				String projectName = release.getName();

				System.out.println("	... Processing of " + projectName + ": " + ZonedDateTime.now() + " ...");

				Vector<PackageBean> packages = FolderToJavaProjectConverter.convert(release.getAbsolutePath());

				// Vector<PackageBean> packages = Files.getPackageBeans(Config.PROJECT_PATH);
				BlobRule blobDetectionRule = new BlobRule();
				LongMethodRule longMethodDetectionRule = new LongMethodRule();
				PromiscuousPackageRule promiscuousPackageDetectionRule = new PromiscuousPackageRule();
				FeatureEnvyRule featureEnvyDetectionRule = new FeatureEnvyRule();
				MisplacedClassRule misplacedClassDetectionRule = new MisplacedClassRule();

				StructuralBlobRule structuralBlobRule = new StructuralBlobRule();
				StructuralLongMethodRule structuraLongMethodDetectionRule = new StructuralLongMethodRule();
				StructuralPromiscuousPackageRule structuralPromiscuousPackageDetectionRule = new StructuralPromiscuousPackageRule();
				StructuralFeatureEnvyRule structuralFeatureEnvyDetectionRule = new StructuralFeatureEnvyRule();
				StructuralMisplacedClassRule structuralMisplacedClassDetectionRule = new StructuralMisplacedClassRule();

				String blobOutputFile = "package-name;class-name;methods;loc;lcom;probability;decor-output\n";
				String longMethodOutputFile = "package-name;class-name;method-name;loc;probability;decor-output\n";
				String promiscuousPackageOutputFile = "package-name;classes;probability;decor-output\n";
				String featureEnvyOutputFile = "package-name;class-name;envied-package-name;envied-class;similarity-with-envied-class;structural-output;dependecies-with-actual-package;dependecies-with-suggested-package;\n";
				String misplacedClassOutputFile = "package-name;class-name;envied-package;similarity-with-envied-class;structural-output;dependecies-with-actual-package;dependecies-with-suggested-package;\n";

				int index = 1;

				for (PackageBean packageBean : packages) {
					System.out.println(packageBean.getName());

					if (CKMetrics.getNumberOfClasses(packageBean) > 10) {
						promiscuousPackageOutputFile += packageBean.getName() + ";"
								+ CKMetrics.getNumberOfClasses(packageBean) + ";"
								+ promiscuousPackageDetectionRule.getPromiscuousPackageProbability(packageBean) + ";"
								+ structuralPromiscuousPackageDetectionRule.isPromiscuousPackage(packageBean) + "\n";
					}

					for (ClassBean classBean : packageBean.getClasses()) {
						// // System.out.println(" "+index + "/" + packageBean.getClasses().size());
						// index++;
						// System.out.println(classBean.getName());
						//
						// ComparablePackageBean enviedPackage =
						// misplacedClassDetectionRule.getEnviedPackage(classBean,
						// packages);
						// double numberOfDependenciesWithActualPackage =
						// CKMetrics.getNumberOfDependencies(classBean,
						// packageBean);
						// double numberOfDependenciesWithEnviedPackage =
						// CKMetrics.getNumberOfDependencies(classBean,
						// enviedPackage);
						//
						// misplacedClassOutputFile += packageBean.getName() + ";" + classBean.getName()
						// + ";"
						// + enviedPackage.getName() + ";" + enviedPackage.getSimilarity() + ";"
						// + structuralMisplacedClassDetectionRule.getEnviedPackage(classBean,
						// packages).getName()
						// + ";" + numberOfDependenciesWithActualPackage + ";"
						// + numberOfDependenciesWithEnviedPackage + "\n";
						//
						// for (MethodBean methodBean : classBean.getMethods()) {
						// System.out.println("insidest");
						// longMethodOutputFile += packageBean.getName() + ";" + classBean.getName() +
						// ";"
						// + methodBean.getName() + ";" + CKMetrics.getLOC(methodBean) + ";"
						// + longMethodDetectionRule.getLongMethodProbability(methodBean) + ";"
						// + structuraLongMethodDetectionRule.isLongMethod(methodBean) + "\n";
						//
						// ComparableClassBean enviedClass =
						// featureEnvyDetectionRule.getEnviedClass(methodBean,
						// packages);
						// double numberOfDependenciesWithActualClass =
						// CKMetrics.getNumberOfDependencies(methodBean,
						// classBean);
						// double numberOfDependenciesWithEnviedClass =
						// CKMetrics.getNumberOfDependencies(methodBean,
						// enviedClass);
						//
						// if (enviedClass != null) {
						// featureEnvyOutputFile += packageBean.getName() + ";" + classBean.getName() +
						// ";"
						// + enviedClass.getBelongingPackage() + ";" + enviedClass.getName() + ";"
						// + enviedClass.getSimilarity() + ";"
						// + structuralFeatureEnvyDetectionRule.getEnviedClass(methodBean, packages)
						// .getName()
						// + ";" + numberOfDependenciesWithActualClass + ";"
						// + numberOfDependenciesWithEnviedClass + "\n";
						// }
						//
						// }

						blobOutputFile += packageBean.getName() + ";" + classBean.getName() + ";"
								+ CKMetrics.getNOM(classBean) + ";" + CKMetrics.getLOC(classBean) + ";"
								+ CKMetrics.getLCOM(classBean) + ";" + blobDetectionRule.getBlobProbability(classBean)
								+ ";" + structuralBlobRule.isBlob(classBean) + "\n";
					}
				}

				File projectDirectory = new File(Config.OUTPUT_PATH + projectName);
				if (!projectDirectory.exists())
					projectDirectory.mkdirs();

				FileUtility.writeFile(blobOutputFile,
						projectDirectory.getAbsolutePath() + "/" + projectName + "-results-blob.csv");
				FileUtility.writeFile(longMethodOutputFile,
						projectDirectory.getAbsolutePath() + "/" + projectName + "-results-longMethod.csv");
				FileUtility.writeFile(promiscuousPackageOutputFile,
						projectDirectory.getAbsolutePath() + "/" + projectName + "-results-promiscuousPackage.csv");
				FileUtility.writeFile(featureEnvyOutputFile,
						Config.OUTPUT_PATH + projectName + "-results-featureEnvy.csv");
				FileUtility.writeFile(misplacedClassOutputFile,
						Config.OUTPUT_PATH + projectName + "-results-misplacedClass.csv");

				System.out.println("		" + ZonedDateTime.now());
			}
		}
	}
}