package nl.tudelft.runner.all;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

//import org.eclipse.jdt.core.dom.ParameterizedType;
import java.lang.reflect.ParameterizedType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.decor.testSmellDetection.StructuralEagerTestRule;
import nl.tudelft.codeSmellDetection.decor.testSmellDetection.StructuralGeneralTextureRule;
import nl.tudelft.codeSmellDetection.taco.testSmellDetection.EagerTestRule;
import nl.tudelft.codeSmellDetection.taco.testSmellDetection.GeneralTextureRule;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.preprocessing.FolderToJavaProjectConverter;
import nl.tudelft.smellynessMetricProcessing.SmellynessMetric;
import nl.tudelft.structuralMetrics.CKMetrics;
import nl.tudelft.testMutation.TestCaseMutation;
import nl.tudelft.testMutation.TestMutationUtilities;
import testsmell.util.SmellOccurence;
import testsmell.util.SmellyClassContainer;
import testsmell.util.ProjectSummarizer;

public class TestSmellDetectionRunner {

	public static String getFileName(final String dir) {
		String temp0 = dir.replace('\\', '/');
		String[] temp = temp0.split("/");
		String fileName = temp[temp.length - 2].length() > 0 ? temp[temp.length - 2] : temp[temp.length - 3];
		if (fileName.lastIndexOf('/') == fileName.length() - 1) {
			fileName = fileName.substring(0, fileName.length() - 2);
		}
		return fileName;
	}

	public static void runner(String args[]) {
		final String projectName = getFileName(args[0]);
		final File systemsDirectory = new File(args[0]);
		final String outputDirectory = args[1] + projectName;
		ProjectSummarizer.createInstance();
		
		for (File release : systemsDirectory.listFiles()) {
			if (release.isDirectory() && (!release.isHidden())) {
				long startTime = System.currentTimeMillis();
				System.out.println(release.getName());
				Vector<PackageBean> packages = FolderToJavaProjectConverter.convert(release.getAbsolutePath());

				TestCaseMutation filter = new TestCaseMutation();
				TestMutationUtilities utilities = new TestMutationUtilities();
				SmellynessMetric smellynessMetric = new SmellynessMetric();
				ArrayList<ClassBean> testSuites = utilities.getTestCases(packages);

				System.out.println(release.getName() + ": " + testSuites.size());
				
				// For setup method
				GeneralTextureRule generalTextureRule = new GeneralTextureRule();
				EagerTestRule eagerTestRule = new EagerTestRule();
//				StructuralGeneralTextureRule structuralGeneralTextureRule = new StructuralGeneralTextureRule();
				// For rest
//				StructuralEagerTestRule structuralEagerTestRule = new StructuralEagerTestRule();

				String generalTextureOutputFile = "package-name;class-name;methods;setUp-method-loc;is-general-texture;structural-output\n";
				String eagerTestOutputFile = "test-case-package-name;test-case-name;smellyness;structural-output\n";

				for (ClassBean testSuite : testSuites) {
					System.out.println(testSuite.getName());
					boolean hasSmell = false;

					String className = testSuite.getBelongingPackage() + "." + testSuite.getName();

					SmellyClassContainer.getInstance().addClass(className);
					ProjectSummarizer.getInstance().addRow(className);

					MethodBean setUpMethod = TestMutationUtilities.getSetUpMethod(testSuite);

					
					if (setUpMethod != null) {
						// cosineSimilarity.computeSimilarity(document1, document2) < 0.1 - returns true
						boolean isGeneralTexture = generalTextureRule.isGeneralTexture(testSuite);
						// getLOC(setUpMethod) > 15
//						boolean isStructuralTexture = structuralGeneralTextureRule.isGeneralTexture(testSuite);

						generalTextureOutputFile += testSuite.getBelongingPackage() + ";" + testSuite.getName() + ";"
								+ CKMetrics.getNOM(testSuite) + ";" + CKMetrics.getLOC(setUpMethod) + ";"
								+ isGeneralTexture + ";" + "\n";

						hasSmell = isGeneralTexture;
						if (isGeneralTexture) {
							String methodWithParameters = appendParametersToMethodName(setUpMethod);
							String smellName = "GeneralFixture";
							ProjectSummarizer.getInstance().addColumn(smellName, "1");
							SmellyClassContainer.getInstance().addMethod(methodWithParameters, null, smellName);
							SmellOccurence.getInstance().increaseOccurence(smellName, projectName);
						}
//						if (isStructuralTexture) {
//							String smellName = "StructuralFixture";
//							ProjectSummarizer.getInstance().addColumn(smellName, "1");
//							SmellyClasses.getInstance().addMethod(setUpMethod.getName(), smellName);
//							SmellOccurence.getInstance().increaseOccurence(smellName, projectName);
//						}
					}

					for (MethodBean testCase : testSuite.getMethods()) {
						boolean isEagerTest = eagerTestRule.isEagerTest(testCase, packages);
						eagerTestOutputFile += testSuite.getBelongingPackage() + "." + testSuite.getName() + ";"
								+ testCase.getName() + ";" + isEagerTest + "\n";

						if (!hasSmell) {
							hasSmell = isEagerTest;
						}
						if (isEagerTest) {
							String smellName = "EagerTest";
							String methodWithParameters = appendParametersToMethodName(testCase);
							ProjectSummarizer.getInstance().addColumn(smellName, "1");
							SmellyClassContainer.getInstance().addMethod(methodWithParameters, null, smellName);
							SmellOccurence.getInstance().increaseOccurence(smellName, projectName);
						}
						// Tries to get the production method that the test calls.
						// Tries to find it by name, test method name == called production method
//						String mutatedTest = filter.alterTestCase(testCase, packages);
//						try {
							// computes the similarity between test method blocks and the called production method
//							double smellyness = smellynessMetric.computeSmellyness(mutatedTest);
							// Checks if method has more than 20 lines, if so it is an eager test
//							boolean isEagerTest = structuralEagerTestRule.isEagerTest(testCase, packages);

//							eagerTestOutputFile += testSuite.getBelongingPackage() + "." + testSuite.getName() + ";"
//									+ testCase.getName() + ";" + smellyness + ";" + isEagerTest + "\n";
//
//							if (!hasSmell) {
//								hasSmell = isEagerTest;
//							}
//							if (isEagerTest) {
//								String smellName = "EagerTest";
//								ProjectSummarizer.getInstance().addColumn(smellName, "1");
//								SmellyClasses.getInstance().addMethod(testCase.getName(), smellName);
//								SmellOccurence.getInstance().increaseOccurence(smellName, projectName);
//							}
//						} catch (IOException e) {
//							e.printStackTrace();
//						}
					}
					if (!hasSmell) {
						ProjectSummarizer.getInstance().removeLastRow();
						SmellyClassContainer.getInstance().removeLastClass();
					}
				}

				File outputFile = new File(outputDirectory);
				if (!outputFile.exists())
					outputFile.mkdirs();

				FileUtility.writeFile(generalTextureOutputFile,
						outputFile.getAbsolutePath() + "/" + release.getName() + "-results-general-texture.csv");
				FileUtility.writeFile(eagerTestOutputFile,
						outputFile.getAbsolutePath() + "/" + release.getName() + "-results-eagerTest.csv");

			}
		}
		ProjectSummarizer.getInstance().printSummary(projectName, outputDirectory);
		SmellOccurence.getInstance().printOccurence(outputDirectory);
	}
	
	private static String appendParametersToMethodName(MethodBean method) {
		String methodNameWithParameters = method.getName();
		methodNameWithParameters += "(";
		List parameters = method.getParameters();
		Iterator iter = parameters.iterator();
		while(iter.hasNext()) {
			SingleVariableDeclaration svd = (SingleVariableDeclaration) iter.next();
			Type type = svd.getType();
			String parameterName = type.toString();
			if (type.isParameterizedType()) {
				parameterName = parameterName.substring(0, parameterName.indexOf("<"));
			}
			methodNameWithParameters += parameterName;
			if (iter.hasNext()) {
				methodNameWithParameters += ", ";
			}
		}
		methodNameWithParameters += ")";
		return methodNameWithParameters;
	}

	public static void main(String args[]) {
		if (args.length < 2) {
			System.err.println("[path to source files] [outputPath]");
			System.exit(1);
		}
		runner(args);
	}
}
