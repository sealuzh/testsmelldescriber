package nl.tudelft.hybridApproach.ANDOR;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Landfill {

	
	
	public static void main(String args[]) {
		File folder = new File("/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/textual-smell-detection/accuracy-evaluation/");
		
		try {
			
			for(File project: folder.listFiles()) {
				String pathToAnalyze = project.getAbsolutePath()+"/general-fixture-accuracy.xlsx";

				ArrayList<String> oracle=new ArrayList<String>();
				
				Landfill landfill = new Landfill();
				oracle = landfill.getOracle(pathToAnalyze);
				
				for(String smell: oracle) {
					if(!smell.isEmpty() && !smell.equals("."))
						System.out.println(smell);
				}
				
			}
			
		}	catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> getOracle(String pPath) {
		ArrayList<String> oracle=new ArrayList<String>();

		try {
			FileInputStream file = new FileInputStream(new File(pPath));

			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			//Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {

				Row row = rowIterator.next();
				oracle.add(row.getCell(0).getStringCellValue());// + "."+row.getCell(1).getStringCellValue());

			}

			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return oracle;
	}
}
