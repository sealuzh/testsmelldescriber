package nl.tudelft.hybridApproach.ANDOR;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ANDOR {

	
	
	public static void main(String args[]) {
		File folder = new File("/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/textual-smell-detection/accuracy-evaluation/");
		
		try {
			
			for(File project: folder.listFiles()) {
				String pathToAnalyze = project.getAbsolutePath()+"/feature-envy-accuracy.xlsx";

				ArrayList<String> oracle=new ArrayList<String>();
				ArrayList<String> taco=new ArrayList<String>();
				ArrayList<String> structuralApproach=new ArrayList<String>();

				ArrayList<String> andCombined=new ArrayList<String>();
				ArrayList<String> orCombined=new ArrayList<String>();

				ANDOR andOr = new ANDOR();
				oracle = andOr.getOracle(pathToAnalyze);
				taco = andOr.getTACOoutput(pathToAnalyze);
				structuralApproach = andOr.getJDOutput(pathToAnalyze);

				andCombined = andOr.combineUsingAND(taco, structuralApproach);
				orCombined = andOr.combineUsingOR(taco, structuralApproach);

				//System.out.println("Computing OR");
				//andOr.computeAccuracy(orCombined, oracle);

				//System.out.println("Computing AND");
				andOr.computeAccuracy(andCombined, oracle);
			}
			
		}	catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void computeAccuracy(ArrayList<String> pCombination, ArrayList<String> pOracles) {
		double truePositives=0.0;
		double falsePositives=0.0;
		boolean found=false;
		
		for(String oracle: pOracles) {
			
			for(String instance: pCombination) {
				
				if(oracle.equals(instance)) {
					truePositives++;
					found=true;
				}
				
			}
			
			if(found==false) 
				falsePositives++;
		}		
		
		double precision = (truePositives/(truePositives+falsePositives));
		double recall = (truePositives/pOracles.size());
		double fMeasure = 2*((precision*recall)/(precision+recall));
		
		System.out.println(precision);
		
		//System.out.println(recall);
		
		//System.out.println("	F-Measure: " + fMeasure);
		
	}
	
	
	public ArrayList<String> combineUsingAND(ArrayList<String> pTACO, ArrayList<String> pJD) {
		ArrayList<String> andCombined=new ArrayList<String>();

		for(String taco: pTACO) {
			for(String jd: pJD) {

				if(taco.equals(jd)) {
					if(! andCombined.contains(taco)) {
						if(! taco.isEmpty()) {
							if(! taco.equals("method")) 
								andCombined.add(taco);
						}
					}
				}

			}
		}

		return andCombined;
	}

	public ArrayList<String> combineUsingOR(ArrayList<String> pTACO, ArrayList<String> pJD) {
		ArrayList<String> orCombined=new ArrayList<String>();

		for(String taco: pTACO) {
			if(!orCombined.contains(taco)) {
				if(! taco.isEmpty()) {
					if(! taco.equals("method")) 
						orCombined.add(taco);
				}
			}
		}

		for(String jd: pJD) {
			if(!orCombined.contains(jd)) {
				if(! jd.isEmpty()) {
					if(! jd.equals("method")) 
						orCombined.add(jd);
				}
			}
		}

		return orCombined;
	}

	public ArrayList<String> getJDOutput(String pPath) {
		ArrayList<String> oracle=new ArrayList<String>();

		try {
			FileInputStream file = new FileInputStream(new File(pPath));

			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(2);

			int header=0;

			//Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {

				if(header>0) { 
					Row row = rowIterator.next();
					oracle.add(row.getCell(0).getStringCellValue());
				} else header++;
			}

			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return oracle;
	}

	public ArrayList<String> getTACOoutput(String pPath) {
		ArrayList<String> oracle=new ArrayList<String>();

		try {
			FileInputStream file = new FileInputStream(new File(pPath));

			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(1);

			//Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {

				Row row = rowIterator.next();
				oracle.add(row.getCell(0).getStringCellValue());

			}

			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return oracle;
	}

	public ArrayList<String> getOracle(String pPath) {
		ArrayList<String> oracle=new ArrayList<String>();

		try {
			FileInputStream file = new FileInputStream(new File(pPath));

			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			//Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {

				Row row = rowIterator.next();
				oracle.add(row.getCell(0).getStringCellValue());

			}

			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return oracle;
	}
}
