package nl.tudelft.testMutation;

import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;

public class TestSuiteMutation {

	public String alterTestSuite(ClassBean pTestSuite, Vector<PackageBean> pSystem) {
		String mutatedTestCase = "public class " + pTestSuite.getName() + " extends TestCase {\n";

		for(MethodBean methodBean: pTestSuite.getMethods()) {

			//System.out.println("public void " + methodBean.getName() + "{");
			String[] methodLines = methodBean.getTextContent().split("\n");

			mutatedTestCase += methodLines[0] + "\n";

			for(MethodBean calledMethodBean: methodBean.getMethodCalls()) {
				MethodBean method = TestMutationUtilities.getProductionMethodBy(calledMethodBean.getName(), pSystem);

				// <method> can be null since the method can call an external method.
				if(method != null) {
					String[] calledMethodLines = method.getTextContent().split("\n");

					for(int i=1; i<calledMethodLines.length-1; i++) {
						mutatedTestCase += calledMethodLines[i]+"\n";
					}
					mutatedTestCase+="_____\n";
				}
			}
		}

		mutatedTestCase += "}\n}";

		return mutatedTestCase;
	}
}