package nl.tudelft.testMutation;

import java.util.Vector;

import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;

public class TestCaseMutation {

	public String alterTestCase(MethodBean pTestCase, Vector<PackageBean> pSystem) {
		String mutatedTestCase = "";

		//System.out.println("public void " + methodBean.getName() + "{");
		String[] methodLines = pTestCase.getTextContent().split("\n");

		mutatedTestCase += methodLines[0] + "\n";

		for(MethodBean calledMethodBean: pTestCase.getMethodCalls()) {
			MethodBean method = TestMutationUtilities.getProductionMethodBy(calledMethodBean.getName(), pSystem);

			// <method> can be null since the method can call an external method.
			if(method != null) {
				String[] calledMethodLines = method.getTextContent().split("\n");

				for(int i=1; i<calledMethodLines.length-1; i++) {
					mutatedTestCase += calledMethodLines[i]+"\n";
				}
				mutatedTestCase+="_____\n";
			}
		}

		mutatedTestCase += "}";

		return mutatedTestCase;
	}
}