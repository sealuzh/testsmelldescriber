package nl.tudelft.testMutation;

import java.util.ArrayList;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;

public class TestMutationUtilities {

	public static MethodBean getSetUpMethod(ClassBean pTestSuite) {

		for (MethodBean pTestCase : pTestSuite.getMethods()) {
			if (pTestCase.getName().equals("setUp"))
				return pTestCase;
		}

		return null;
	}

	public static boolean isTestCase(ClassBean pClass) {
		if (pClass.getTextContent().contains(" extends TestCase")) {
			return true;
		} else if (pClass.getTextContent().contains("@Test")) {
			return true;
		}

		return false;
	}

	public static boolean isTestPackage(PackageBean pPackage) {
		for (ClassBean classBean : pPackage.getClasses()) {
			if (classBean.getTextContent().contains(" extends TestCase")) {
				return true;
			} else if (classBean.getTextContent().contains("@Test")) {
				return true;
			}
		}
		return false;
	}

	private boolean isUnique(ClassBean classToAdd, ArrayList<ClassBean> list) {
		for (ClassBean cb : list) {
			if (cb.getName() == classToAdd.getName()) {
				return false;
			}
		}
		return true;
	}
	
	public ArrayList<ClassBean> getTestCases(Vector<PackageBean> pProject) {
		ArrayList<ClassBean> testCases = new ArrayList<ClassBean>();

		for (PackageBean packageBean : pProject) {
			for (ClassBean classBean : packageBean.getClasses()) {
				if (classBean.getTextContent().contains(" extends TestCase") && isUnique(classBean, testCases)) {
					testCases.add(classBean);
				} else if (classBean.getTextContent().contains("@Test") && isUnique(classBean, testCases)) {
					testCases.add(classBean);
				}
			}
		}

		return testCases;
	}

	public static MethodBean getProductionMethodBy(String pTestMethodCall, Vector<PackageBean> pSystem) {

		for (PackageBean packageBean : pSystem) {
			for (ClassBean classBean : packageBean.getClasses()) {
				for (MethodBean methodBean : classBean.getMethods()) {
					if (methodBean.getName().equals(pTestMethodCall))
						return methodBean;
				}
			}
		}

		return null;
	}
}