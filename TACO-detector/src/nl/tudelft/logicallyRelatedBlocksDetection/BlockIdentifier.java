package nl.tudelft.logicallyRelatedBlocksDetection;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import nl.tudelft.beans.*;
import nl.tudelft.parser.BlockVisitor;
import nl.tudelft.parser.CodeParser;

public class BlockIdentifier {

	public ArrayList<String> getBlocksByMethod(MethodBean pMethod) {
		ArrayList<String> methodBlocks = new ArrayList<String>();
		BlockVisitor blockVisitor = new BlockVisitor();

		CodeParser codeParser = new CodeParser();
		TypeDeclaration declaration = codeParser.createParser(pMethod.getTextContent(), ASTParser.K_CLASS_BODY_DECLARATIONS);

		declaration.accept(blockVisitor);

		Collection<Block> blocks = blockVisitor.getBlocks();

		int k=0;

		for(Block block : blocks) {
			if(k==0) k++;
			else methodBlocks.add(block.toString());
		}

		return methodBlocks;
	}
	
	public ArrayList<String> extractLogicallyRelatedBlocksFromMethod(String pMethodContent) {
		ArrayList<String> blocks = new ArrayList<String>();

		String[] methodLines = pMethodContent.split("\n");
		String block="";
		for(int k=0; k<methodLines.length; k++) {
			
			block+=methodLines[k];
			
			if( (k+1) < methodLines.length ) {
				if(JaccardIndex.computeAsimmetricJaccardIndex(methodLines[k], methodLines[k+1]) == 0.0) {
					blocks.add(block);
					block="";
				}
			}
		}

		return blocks;
	}
	
	public ArrayList<String> extractLogicallyRelatedBlocksFromClass(ClassBean pClass) {
		ArrayList<String> blocks = new ArrayList<String>();
		String block="";
		for(MethodBean method: pClass.getMethods()) {
			
			block+=method.getTextContent();
			
			for(MethodBean secondMethod: pClass.getMethods()) {
				
				if(method != secondMethod) {
					if(JaccardIndex.computeAsimmetricJaccardIndex(method.getTextContent(), secondMethod.getTextContent()) < 8.0) {
						blocks.add(block);
						block="";
					}
				}
			}
		}
		

		return blocks;
	}
	
	public ArrayList<String> extractLogicallyRelatedBlocksFromPackage(PackageBean pPackage) {
		ArrayList<String> blocks = new ArrayList<String>();
		String block="";
		for(ClassBean classBean: pPackage.getClasses()) {
			
			block+=classBean.getTextContent();
			
			for(ClassBean secondClassBean: pPackage.getClasses()) {
				
				if(classBean != secondClassBean) {
					if(JaccardIndex.computeAsimmetricJaccardIndex(classBean.getTextContent(), secondClassBean.getTextContent()) < 20.0) {
						blocks.add(block);
						block="";
					}
				}
			}
		}
		

		return blocks;
	}
}
