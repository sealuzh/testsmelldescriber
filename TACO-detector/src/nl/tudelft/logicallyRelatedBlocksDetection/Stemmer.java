package nl.tudelft.logicallyRelatedBlocksDetection;

public class Stemmer {

	public String stemBlock(String pBlock) {
		String stemmedBlock="";
		String[] words = pBlock.split(" ");
		
		for(String word: words) {
			String stemmedWord = this.stem(word);
			
			stemmedBlock+=stemmedWord + " ";
		}
		
		return stemmedBlock;
	}
	
	public String stem(String pWord) {
		PorterStemmer stemmer = new PorterStemmer();
		return stemmer.step5(stemmer.step4(stemmer.step3(stemmer.step2(stemmer.step1(pWord)))));
	}
}
