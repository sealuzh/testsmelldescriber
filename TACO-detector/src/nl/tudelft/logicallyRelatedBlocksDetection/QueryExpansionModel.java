package nl.tudelft.logicallyRelatedBlocksDetection;

import java.util.ArrayList;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class QueryExpansionModel {

	public String expandBlock(String pBlock) {
		String expandedBlock="";
		String[] words = pBlock.split(" ");
		
		for(String word: words) {
			ArrayList<String> expandedWord = this.expand(word);
			
			for(String w: expandedWord)
				expandedBlock+=w + " ";
		}
		
		return expandedBlock;
		
	}
	
	public ArrayList<String> expand(String pWord) {
		ArrayList<String> words = new ArrayList<String>();
		System.setProperty("wordnet.database.dir", "/usr/local/WordNet-3.0/dict");
		WordNetDatabase database = WordNetDatabase.getFileInstance();
		
		Synset[] synsets = database.getSynsets(pWord);
		
		words.add(pWord);
		
		if (synsets.length > 0) {
			ArrayList<String> synonyms = new ArrayList<String>();
			
			for (int i = 0; i < synsets.length; i++){
				String[] wordForms = synsets[i].getWordForms();
				for (int j = 0; j < wordForms.length; j++) {
					if(! synonyms.contains(wordForms[j]))
						synonyms.add(wordForms[j]);
				}
				
				for(String s: synonyms)
					words.add(s);
			} 
		}
		
		return words;
	}
}
