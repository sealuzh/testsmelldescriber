package nl.tudelft.logicallyRelatedBlocksDetection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.tudelft.fileUtilities.FileUtility;

public class IdentifierAndCommentExtractor {

	public String extractIdentifierAndComments(String pMethodContent) throws IOException {
		String identifiersAndComments="";
		
		Pattern identifiersPattern = Pattern.compile("[A-Za-z0-9]*");
		Matcher idMatcher = identifiersPattern.matcher(pMethodContent);
		
		while(idMatcher.find()) {
			String words[] = idMatcher.group().split("(?<!^)(?=[A-Z])");
			ArrayList<String> finalWords = filterWords(words);
			
			for(String word: finalWords) {
				identifiersAndComments+=word+" ";
			}
		}

		String comment = "";
		
		Pattern commentPattern = Pattern.compile("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)");
		Matcher matcher = commentPattern.matcher(pMethodContent);
		
		while(matcher.find()) {
			comment+=matcher.group();
		}
		
		identifiersAndComments+=comment;
	
		return identifiersAndComments;
	}
	
	private ArrayList<String> filterWords(String[] pWords) throws IOException {
		ArrayList<String> words = new ArrayList<String>();
		String fileContent = FileUtility.readFile("stopword.txt");
		String[] stopWords=fileContent.split("\n");
		
		for(String w: pWords) {
			boolean toRemove=false;
			
			for(String stopWord: stopWords) {
				if(w.toLowerCase().equals(stopWord.toLowerCase())) 
					toRemove=true;
			}
			
			if(toRemove==false)
				words.add(w.toLowerCase());
		}
		
		return words;
		
	}
}
