package nl.tudelft.codeSmellDetection.taco.complexComponentsDetection;

import java.io.IOException;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.smellynessMetricProcessing.SmellynessMetric;
import nl.tudelft.structuralMetrics.CKMetrics;

public class BlobRule {

	public boolean isBlob(ClassBean pClass) {
		SmellynessMetric smellyness=new SmellynessMetric();
		ComponentMutation componentMutation = new ComponentMutation();
		
		String mutatedClass = componentMutation.alterClass(pClass);
		
		try {		
			double smellynessIndex = smellyness.computeSmellyness(mutatedClass);
			
			if(smellynessIndex > 0.6)
				return true;
			
		} catch (IOException e) {
			return false;
		}
		
		return false;
	}
	
	public double getBlobProbability(ClassBean pClass) {
		SmellynessMetric smellyness=new SmellynessMetric();
		ComponentMutation componentMutation = new ComponentMutation();
		
		String mutatedClass = componentMutation.alterClass(pClass);
		
		try {		
			double similarity = smellyness.computeSmellyness(mutatedClass);
			
			if(CKMetrics.getLOC(pClass) < 500) {
				similarity -= ((similarity*85)/100);
			} 
			
			//System.out.println(similarity);
			return similarity;
			
		} catch (IOException e) {
			return 0.0;
		}
	}
}