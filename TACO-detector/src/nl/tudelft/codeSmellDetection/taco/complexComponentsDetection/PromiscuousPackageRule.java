package nl.tudelft.codeSmellDetection.taco.complexComponentsDetection;

import java.io.IOException;

import nl.tudelft.beans.PackageBean;
import nl.tudelft.smellynessMetricProcessing.SmellynessMetric;
import nl.tudelft.structuralMetrics.CKMetrics;

public class PromiscuousPackageRule {

	public boolean isPromiscuousPackage(PackageBean pPackage) {
		SmellynessMetric smellyness=new SmellynessMetric();
		ComponentMutation componentMutation = new ComponentMutation();
		
		String mutatedPackage = componentMutation.alterPackage(pPackage);
		
		try {		
			double smellynessIndex = smellyness.computeSmellyness(mutatedPackage);
			
			if(smellynessIndex > 0.6)
				return true;
			
		} catch (IOException e) {
			return false;
		}
		
		return false;
	}	
	
	public double getPromiscuousPackageProbability(PackageBean pPackage) {
		SmellynessMetric smellyness=new SmellynessMetric();
		ComponentMutation componentMutation = new ComponentMutation();
		
		String mutatedPackage = componentMutation.alterPackage(pPackage);
		
		try {		
			double similarity = smellyness.computeSmellyness(mutatedPackage);
			
			if(CKMetrics.getNumberOfClasses(pPackage) < 20) {
				similarity -= ((similarity*75)/100);
			}
			
			return smellyness.computeSmellyness(mutatedPackage);
			
		} catch (IOException e) {
			return 0.0;
		}
	}	
}