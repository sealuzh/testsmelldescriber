package nl.tudelft.codeSmellDetection.taco.complexComponentsDetection;

import java.io.IOException;
import java.util.ArrayList;

import nl.tudelft.beans.MethodBean;
import nl.tudelft.logicallyRelatedBlocksDetection.BlockIdentifier;
import nl.tudelft.smellynessMetricProcessing.SmellynessMetric;
import nl.tudelft.structuralMetrics.CKMetrics;

public class LongMethodRule {

	public boolean isLongMethod(MethodBean pMethod) {
		SmellynessMetric smellyness=new SmellynessMetric();
		BlockIdentifier blockIdentifier = new BlockIdentifier();
		ComponentMutation componentMutation = new ComponentMutation();
		
		ArrayList<String> blocks = blockIdentifier.getBlocksByMethod(pMethod);
		
		String mutatedMethod = componentMutation.alterMethod(blocks);
		
		try {		
			double smellynessIndex = smellyness.computeSmellyness(mutatedMethod);
			
			if(smellynessIndex > 0.6)
				return true;
			
		} catch (IOException e) {
			return false;
		}
		
		return false;
	}
	
	public double getLongMethodProbability(MethodBean pMethod) {
		SmellynessMetric smellyness=new SmellynessMetric();
		BlockIdentifier blockIdentifier = new BlockIdentifier();
		ComponentMutation componentMutation = new ComponentMutation();
		
		ArrayList<String> blocks = blockIdentifier.getBlocksByMethod(pMethod);
		
		String mutatedMethod = componentMutation.alterMethod(blocks);
		
		try {		
			double similarity = smellyness.computeSmellyness(mutatedMethod);
			
			if(CKMetrics.getLOC(pMethod) < 80) {
				similarity -= ((similarity*75)/100);
			} 
			
			return similarity;
			
			
		} catch (IOException e) {
			return 0.0;
		}
	}
}