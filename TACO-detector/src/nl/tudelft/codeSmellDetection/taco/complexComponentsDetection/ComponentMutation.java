package nl.tudelft.codeSmellDetection.taco.complexComponentsDetection;

import java.util.ArrayList;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;

public class ComponentMutation {

	public String alterClass(ClassBean pClass) {
		String mutatedClass = "public class " + pClass.getName() + " {\n";

		for(MethodBean methodBean: pClass.getMethods()) {
			mutatedClass+=methodBean.getTextContent();
			mutatedClass+="_____\n";
		}

		mutatedClass += "}\n}";

		return mutatedClass;
	}

	public String alterPackage(PackageBean pPackage) {
		String mutatedPackage = "";

		for(ClassBean classBean: pPackage.getClasses()) {
			mutatedPackage+=classBean.getTextContent();
			mutatedPackage+="_____\n";
		}

		return mutatedPackage;
	}

	public String alterMethod(ArrayList<String> pMethodBlocks) {
		String mutatedMethod = "";

		for(String block: pMethodBlocks) {
			mutatedMethod+=block;
			mutatedMethod+="_____\n";
		}

		return mutatedMethod;
	}
}