package nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Map.Entry;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.similarityComputation.CosineSimilarity;

public class MisplacedClassRule {

	public boolean isMisplacedClass(ClassBean pClass, Vector<PackageBean> pSystemPackages) {
		SortedMap<PackageBean, Double> similaritiesWithClass = new TreeMap<PackageBean, Double>();
		CosineSimilarity cosineSimilarity = new CosineSimilarity();

		String[] document1 = new String[2];
		document1[0] = "class";
		document1[1] = pClass.getTextContent();

		for(PackageBean packageBean: pSystemPackages) {

			String[] document2 = new String[2];
			document2[0] = "package";
			document2[1] = packageBean.getTextContent();

			try {
				similaritiesWithClass.put(packageBean, cosineSimilarity.computeSimilarity(document1, document2));
			} catch (IOException e) {
				return false;
			}
		}

		Entry<PackageBean, Double> firstRankedClass = similaritiesWithClass.entrySet().iterator().next();

		if(firstRankedClass.getKey().getName().equals(pClass.getBelongingPackage()))
			return false;

		return true;
	}

	public ComparablePackageBean getEnviedPackage(ClassBean pClass, Vector<PackageBean> pSystemPackages) {
		ArrayList<ComparablePackageBean> similaritiesWithClass = new ArrayList<ComparablePackageBean>();
		CosineSimilarity cosineSimilarity = new CosineSimilarity();

		ComparablePackageBean actualPackage = new ComparablePackageBean();

		String[] document1 = new String[2];
		document1[0] = "class";
		document1[1] = pClass.getTextContent();

		for(PackageBean packageBean: pSystemPackages) {
			if(packageBean.getName().equals(pClass.getBelongingPackage())) {
				actualPackage.setName(packageBean.getName());
				actualPackage.setClasses(packageBean.getClasses());
				actualPackage.setTextContent(packageBean.getTextContent());
				
				String[] actualPackageDocument = new String[2];
				actualPackageDocument[0] = "package";
				actualPackageDocument[1] = actualPackage.getTextContent();
				
				try {
					actualPackage.setSimilarity(cosineSimilarity.computeSimilarity(document1, actualPackageDocument));
					
					if(actualPackage.getSimilarity() >= 0.4)
						return actualPackage;
				
				} catch (IOException e) {}
				
			}
		}
		
		if(actualPackage.getSimilarity() < 0.4) {
		
			for(PackageBean packageBean: pSystemPackages) {
	
				ComparablePackageBean comparablePackageBean = new ComparablePackageBean();
				comparablePackageBean.setName(packageBean.getName());
				comparablePackageBean.setClasses(packageBean.getClasses());
				comparablePackageBean.setTextContent(packageBean.getTextContent());
	
				String[] document2 = new String[2];
				document2[0] = "package";
				document2[1] = packageBean.getTextContent();
	
				try {
					comparablePackageBean.setSimilarity(cosineSimilarity.computeSimilarity(document1, document2));
					similaritiesWithClass.add(comparablePackageBean);
				} catch (IOException e) {}
			}
	
			PackageBeanComparator comparator = new PackageBeanComparator();
			Collections.sort(similaritiesWithClass, comparator);
	
			ComparablePackageBean firstRankedPackage = similaritiesWithClass.get(similaritiesWithClass.size()-1);
	
			return firstRankedPackage;
		}
		
		return actualPackage;
	}
}