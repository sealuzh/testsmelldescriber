package nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection;

import java.util.Comparator;

public class ClassBeanComparator implements Comparator<ComparableClassBean> {

	@Override
	public int compare(ComparableClassBean o1, ComparableClassBean o2) {
		
		if(o1.getSimilarity() > o2.getSimilarity())
			return 1;
		else if(o2.getSimilarity() > o1.getSimilarity())
			return -1;
		else return 0;
	}
}