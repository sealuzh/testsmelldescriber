package nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection;

import nl.tudelft.beans.PackageBean;

public class ComparablePackageBean extends PackageBean {
	private double similarity;

	public double getSimilarity() {
		return similarity;
	}

	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
}
