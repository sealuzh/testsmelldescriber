package nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection;

import nl.tudelft.beans.ClassBean;

public class ComparableClassBean extends ClassBean {
	private double similarity;

	public double getSimilarity() {
		return similarity;
	}

	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
}