package nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection;

import java.util.Comparator;

public class PackageBeanComparator implements Comparator<ComparablePackageBean> {

	@Override
	public int compare(ComparablePackageBean o1, ComparablePackageBean o2) {
		
		if(o1.getSimilarity() > o2.getSimilarity())
			return 1;
		else if(o2.getSimilarity() > o1.getSimilarity())
			return -1;
		else return 0;
	}
}