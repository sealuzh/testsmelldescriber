package nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.similarityComputation.CosineSimilarity;

public class FeatureEnvyRule {

	public boolean isFeatureEnvy(MethodBean pMethod, Vector<PackageBean> pSystemPackages) {
		SortedMap<ClassBean, Double> similaritiesWithMethod = new TreeMap<ClassBean, Double>();
		CosineSimilarity cosineSimilarity = new CosineSimilarity();
		ArrayList<ClassBean> candidateEnviedClasses = MisplacedComponentsUtilities.getCandidates(pMethod, pSystemPackages);

		String[] document1 = new String[2];
		document1[0] = "method";
		document1[1] = pMethod.getTextContent();

		for(ClassBean classBean: candidateEnviedClasses) {

			String[] document2 = new String[2];
			document2[0] = "class";
			document2[1] = classBean.getTextContent();

			try {
				similaritiesWithMethod.put(classBean, cosineSimilarity.computeSimilarity(document1, document2));
			} catch (IOException e) {
				return false;
			}
		}

		Entry<ClassBean, Double> firstRankedClass = similaritiesWithMethod.entrySet().iterator().next();

		if(firstRankedClass.getKey().getName().equals(pMethod.getBelongingClass().getName()))
			return false;

		return true;
	}

	public ComparableClassBean getEnviedClass(MethodBean pMethod, Vector<PackageBean> pSystemPackages) {
		ArrayList<ComparableClassBean> similaritiesWithMethod = new ArrayList<ComparableClassBean>(); 
		CosineSimilarity cosineSimilarity = new CosineSimilarity();
		ComparableClassBean actualClass = new ComparableClassBean();

		String[] document1 = new String[2];
		document1[0] = "method";
		document1[1] = pMethod.getTextContent();

		for(PackageBean packageBean: pSystemPackages) {
			for(ClassBean classBean: packageBean.getClasses()) {
				if(classBean.getName().equals(pMethod.getBelongingClass().getName())) {
					actualClass.setName(classBean.getName());
					actualClass.setInstanceVariables(classBean.getInstanceVariables());
					actualClass.setMethods(classBean.getMethods());
					actualClass.setImports(classBean.getImports());
					actualClass.setTextContent(classBean.getTextContent());
					actualClass.setLOC(classBean.getLOC());
					actualClass.setSuperclass(classBean.getSuperclass());
					actualClass.setBelongingPackage(classBean.getBelongingPackage());
					actualClass.setPathToFile(classBean.getPathToFile());

					String[] actualClassDocument = new String[2];
					actualClassDocument[0] = "class";
					actualClassDocument[1] = actualClass.getTextContent();

					try {
						actualClass.setSimilarity(cosineSimilarity.computeSimilarity(document1, actualClassDocument));

						if(actualClass.getSimilarity() >= 0.4)
							return actualClass;

					} catch (IOException e) {}

				}
			}
		}

		if(actualClass.getSimilarity() < 0.25) {
			ArrayList<ClassBean> candidateEnviedClasses = MisplacedComponentsUtilities.getCandidates(pMethod, pSystemPackages);

			for(ClassBean classBean: candidateEnviedClasses) {

				ComparableClassBean comparableClassBean = new ComparableClassBean();
				comparableClassBean.setName(classBean.getName());
				comparableClassBean.setInstanceVariables(classBean.getInstanceVariables());
				comparableClassBean.setMethods(classBean.getMethods());
				comparableClassBean.setImports(classBean.getImports());
				comparableClassBean.setTextContent(classBean.getTextContent());
				comparableClassBean.setLOC(classBean.getLOC());
				comparableClassBean.setSuperclass(classBean.getSuperclass());
				comparableClassBean.setBelongingPackage(classBean.getBelongingPackage());
				comparableClassBean.setPathToFile(classBean.getPathToFile());

				String[] document2 = new String[2];
				document2[0] = "class";
				document2[1] = classBean.getTextContent();

				try {
					comparableClassBean.setSimilarity(cosineSimilarity.computeSimilarity(document1, document2));
					similaritiesWithMethod.add(comparableClassBean);
				} catch (IOException e) {}
			}

			if(similaritiesWithMethod.isEmpty()) {
				return null;
			}

			ClassBeanComparator comparator = new ClassBeanComparator();
			Collections.sort(similaritiesWithMethod, comparator);
			ComparableClassBean firstRankedClass = similaritiesWithMethod.get(similaritiesWithMethod.size()-1);

			return firstRankedClass;
		}

		return actualClass;
	}
}