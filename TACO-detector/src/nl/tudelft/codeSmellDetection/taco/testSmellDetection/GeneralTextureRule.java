package nl.tudelft.codeSmellDetection.taco.testSmellDetection;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.similarityComputation.CosineSimilarity;
import nl.tudelft.structuralMetrics.CKMetrics;
import nl.tudelft.testMutation.TestMutationUtilities;

public class GeneralTextureRule {

	public boolean isGeneralTexture(ClassBean pTestSuite) {
		CosineSimilarity cosineSimilarity = new CosineSimilarity();
		MethodBean setUpMethod = TestMutationUtilities.getSetUpMethod(pTestSuite);

		HashMap<MethodBean, Double> similaritiesWithSetUpMethod = new HashMap<MethodBean, Double>();

		if (setUpMethod == null)
			return false;

		String[] document1 = new String[2];
		document1[0] = "setUpMethod";
		document1[1] = setUpMethod.getTextContent();

		for (MethodBean pTestCase : pTestSuite.getMethods()) {
			String[] document2 = new String[2];
			document2[0] = pTestCase.getName();
			document2[1] = pTestCase.getTextContent();

			try {
				// Check similarity between setup method and other methods
				similaritiesWithSetUpMethod.put(pTestCase, cosineSimilarity.computeSimilarity(document1, document2));
			} catch (IOException e) {
				return false;
			}
		}

		for (Entry<MethodBean, Double> testCase1 : similaritiesWithSetUpMethod.entrySet()) {
			if (testCase1.getValue() > 0.0) {

				for (Entry<MethodBean, Double> testCase2 : similaritiesWithSetUpMethod.entrySet()) {
					if (testCase1 != testCase2) {
						if (testCase2.getValue() > 0.0) {
							// Similarity is larger than 0.0 and it's not the setup method

							document1 = new String[2];
							document1[0] = "firstTestCase";
							document1[1] = testCase1.getKey().getTextContent();

							String[] document2 = new String[2];
							document2[0] = "secondTestCase";
							document2[1] = testCase2.getKey().getTextContent();

							try {
								// what, why now, what
								if (CKMetrics.getLOC(setUpMethod) < 10)
									return false;
								// compute similarity again?
								else if (cosineSimilarity.computeSimilarity(document1, document2) < 0.1) {
									return true;
								}
							} catch (IOException e) {
								return false;
							}
						}

					}

				}
			}
		}

		return false;
	}
}