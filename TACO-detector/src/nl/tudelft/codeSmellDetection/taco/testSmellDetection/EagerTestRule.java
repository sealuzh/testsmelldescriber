package nl.tudelft.codeSmellDetection.taco.testSmellDetection;

import java.io.IOException;
import java.util.Vector;

import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.smellynessMetricProcessing.SmellynessMetric;
import nl.tudelft.testMutation.TestCaseMutation;

public class EagerTestRule {

	public boolean isEagerTest(MethodBean pTestMethod, Vector<PackageBean> pSystemPackages) {
		TestCaseMutation filter = new TestCaseMutation();
		SmellynessMetric smellyness=new SmellynessMetric();
		
		String mutatedTest = filter.alterTestCase(pTestMethod, pSystemPackages);
		try {
			double smellynessIndex = smellyness.computeSmellyness(mutatedTest);
			
			if(smellynessIndex > 0.6)
				return true;
			
		} catch (IOException e) {
			return false;
		}
		
		return false;
	}
	
	public double getEagerTestProbability(MethodBean pTestMethod, Vector<PackageBean> pSystemPackages) {
		TestCaseMutation filter = new TestCaseMutation();
		SmellynessMetric smellyness=new SmellynessMetric();
		
		String mutatedTest = filter.alterTestCase(pTestMethod, pSystemPackages);
		try {
			return smellyness.computeSmellyness(mutatedTest);
						
		} catch (IOException e) {
			return 0.0;
		}
	}
}