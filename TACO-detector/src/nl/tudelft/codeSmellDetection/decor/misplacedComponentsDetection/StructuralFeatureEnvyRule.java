package nl.tudelft.codeSmellDetection.decor.misplacedComponentsDetection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.ClassBeanComparator;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.ComparableClassBean;
import nl.tudelft.structuralMetrics.CKMetrics;

public class StructuralFeatureEnvyRule {

	public ClassBean getEnviedClass(MethodBean pMethod, Vector<PackageBean> pSystemPackages) {
		ArrayList<ClassBean> classes = new ArrayList<ClassBean>();
		ClassBean actualClass = null;

		if(pMethod.getBelongingClass() != null) {
			for(PackageBean packageBean: pSystemPackages) {
				for(ClassBean classBean: packageBean.getClasses()) {
	
					if(classBean.getName().equals(pMethod.getBelongingClass().getName())) {
						actualClass = classBean;
					} else {
						classes.add(classBean);
					}
				}
			}
		}
		
		double numberOfDependenciesWithActualClass = CKMetrics.getNumberOfDependencies(pMethod, actualClass);

		ArrayList<ComparableClassBean> dependenciesWithMethod = new ArrayList<ComparableClassBean>();

		for(ClassBean classBean: classes) {

			ComparableClassBean comparableClassBean = new ComparableClassBean();
			comparableClassBean.setName(classBean.getName());
			comparableClassBean.setInstanceVariables(classBean.getInstanceVariables());
			comparableClassBean.setMethods(classBean.getMethods());
			comparableClassBean.setImports(classBean.getImports());
			comparableClassBean.setTextContent(classBean.getTextContent());
			comparableClassBean.setLOC(classBean.getLOC());
			comparableClassBean.setSuperclass(classBean.getSuperclass());
			comparableClassBean.setBelongingPackage(classBean.getBelongingPackage());
			comparableClassBean.setPathToFile(classBean.getPathToFile());

			double numberOfDependenciesWithCandidateEnviedClass = CKMetrics.getNumberOfDependencies(pMethod, classBean);
			comparableClassBean.setSimilarity(numberOfDependenciesWithCandidateEnviedClass);

			dependenciesWithMethod.add(comparableClassBean);

		}

		ClassBeanComparator comparator = new ClassBeanComparator();
		Collections.sort(dependenciesWithMethod, comparator);

		ComparableClassBean firstRankedClass = dependenciesWithMethod.get(dependenciesWithMethod.size()-1);

		if(numberOfDependenciesWithActualClass < firstRankedClass.getSimilarity())
			return firstRankedClass;

		else return actualClass;
	}
}
