package nl.tudelft.codeSmellDetection.decor.misplacedComponentsDetection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.ComparablePackageBean;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.PackageBeanComparator;
import nl.tudelft.structuralMetrics.CKMetrics;

public class StructuralMisplacedClassRule {

	public PackageBean getEnviedPackage(ClassBean pClass, Vector<PackageBean> pSystemPackages) {
		ArrayList<PackageBean> packages = new ArrayList<PackageBean>();
		PackageBean actualPackage = null;

		for(PackageBean packageBean: pSystemPackages) {
			if(packageBean.getName().equals(pClass.getBelongingPackage())) {
				actualPackage = packageBean;
			} else {
				packages.add(packageBean);
			}
		}

		double numberOfDependenciesWithActualPackage = CKMetrics.getNumberOfDependencies(pClass, actualPackage);
		
		ArrayList<ComparablePackageBean> dependenciesWithClass = new ArrayList<ComparablePackageBean>();
		
		for(PackageBean packageBean: packages) {

			ComparablePackageBean comparablePackageBean = new ComparablePackageBean();
			comparablePackageBean.setName(packageBean.getName());
			comparablePackageBean.setClasses(packageBean.getClasses());
			comparablePackageBean.setTextContent(packageBean.getTextContent());
			
			double numberOfDependenciesWithCandidateEnviedPackage = CKMetrics.getNumberOfDependencies(pClass, packageBean);
			comparablePackageBean.setSimilarity(numberOfDependenciesWithCandidateEnviedPackage);
			
			dependenciesWithClass.add(comparablePackageBean);

		}
		
		PackageBeanComparator comparator = new PackageBeanComparator();
		Collections.sort(dependenciesWithClass, comparator);
		
		ComparablePackageBean firstRankedPackage = dependenciesWithClass.get(dependenciesWithClass.size()-1);

		if(numberOfDependenciesWithActualPackage < firstRankedPackage.getSimilarity())
			return firstRankedPackage;
		
		else return actualPackage;
	}
}