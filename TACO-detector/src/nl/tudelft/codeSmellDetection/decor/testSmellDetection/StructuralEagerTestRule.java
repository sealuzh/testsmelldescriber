package nl.tudelft.codeSmellDetection.decor.testSmellDetection;

import java.util.Vector;

import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.structuralMetrics.CKMetrics;

public class StructuralEagerTestRule {

	public boolean isEagerTest(MethodBean pTestMethod, Vector<PackageBean> pSystemPackages) {
		
		if(CKMetrics.getLOC(pTestMethod) > 20) {
			return true;
		}
		
		return false;
	}
}