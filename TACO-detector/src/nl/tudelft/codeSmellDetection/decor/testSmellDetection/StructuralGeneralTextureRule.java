package nl.tudelft.codeSmellDetection.decor.testSmellDetection;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.structuralMetrics.CKMetrics;
import nl.tudelft.testMutation.TestMutationUtilities;

public class StructuralGeneralTextureRule {

	public boolean isGeneralTexture(ClassBean pTestSuite) {
		MethodBean setUpMethod = TestMutationUtilities.getSetUpMethod(pTestSuite);

		if(CKMetrics.getLOC(setUpMethod) > 15) {
			return true;
		}
		
		return false;
	}
}