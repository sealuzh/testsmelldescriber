package nl.tudelft.codeSmellDetection.decor.complexComponentsDetection;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.structuralMetrics.CKMetrics;

public class StructuralBlobRule {

	/*
	 * se AND allora somma 
	 * se OR prendo il maggiore.
	 * 
	 */
	
	public boolean isBlob(ClassBean pClass) {

		if(CKMetrics.getLOC(pClass) > 800) {
			if(CKMetrics.getLCOM(pClass) > 100)
				return true;
		}

		return false;
	}
}