package nl.tudelft.codeSmellDetection.decor.complexComponentsDetection;

import nl.tudelft.beans.MethodBean;
import nl.tudelft.structuralMetrics.CKMetrics;

public class StructuralLongMethodRule {

	public boolean isLongMethod(MethodBean pMethod) {
		
		if(CKMetrics.getLOC(pMethod) > 100) {
			return true;
		}
		
		return false;
	}
}