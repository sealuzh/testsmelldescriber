package nl.tudelft.codeSmellDetection.decor.complexComponentsDetection;

import nl.tudelft.beans.PackageBean;
import nl.tudelft.structuralMetrics.CKMetrics;

public class StructuralPromiscuousPackageRule {

	public boolean isPromiscuousPackage(PackageBean pPackage) {

		if(CKMetrics.getNumberOfClasses(pPackage) > 20) {
			return true;
		}

		return true;
	}	
}