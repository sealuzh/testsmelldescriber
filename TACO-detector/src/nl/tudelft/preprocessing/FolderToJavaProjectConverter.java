package nl.tudelft.preprocessing;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.parser.ClassParser;
import nl.tudelft.parser.CodeParser;

public class FolderToJavaProjectConverter {

	public static Vector<PackageBean> convert(String pPath) {
		File projectDirectory = new File(pPath);
		Vector<PackageBean> packages = new Vector<PackageBean>();

		if (projectDirectory.isDirectory()) {
			for (File subDir : projectDirectory.listFiles()) {
				if (subDir.isDirectory()) {
					System.out.println(subDir.getName());
					Vector<File> javaFiles = FolderToJavaProjectConverter.listJavaFiles(subDir);
					if (javaFiles.size() > 0) {
						for (File javaFile : javaFiles) {
							PackageBean newPackage = getClass(javaFile, packages);
							if (newPackage.getClasses().size() > 0) {
								packages.add(newPackage);
							}
						}
					}
				}
			}
		}

		for (PackageBean pb : packages) {
			String textualContent = "";
			for (ClassBean cb : pb.getClasses()) {
				textualContent += cb.getTextContent();
			}
			pb.setTextContent(textualContent);
		}

		return packages;
	}

	private static PackageBean getClass(File fn, Vector<PackageBean> packages) {
		CompilationUnit parsed;
		CodeParser codeParser = new CodeParser();
		PackageBean packageBean = new PackageBean();
		try {
			parsed = codeParser.createParser(FileUtility.readFile(fn.getAbsolutePath()));
			TypeDeclaration typeDeclaration = (TypeDeclaration) parsed.types().get(0);

			Vector<String> imports = new Vector<String>();

			for (Object importedResource : parsed.imports())
				imports.add(importedResource.toString());

			if (!FolderToJavaProjectConverter.isAlreadyCreated(parsed.getPackage().getName().getFullyQualifiedName(),
					packages)) {
				packageBean.setName(parsed.getPackage().getName().getFullyQualifiedName());

				ClassBean classBean = ClassParser.parse(typeDeclaration, packageBean.getName(), imports);
				classBean.setPathToFile(fn.getAbsolutePath());

				packageBean.addClass(classBean);
			} else {
				packageBean = FolderToJavaProjectConverter
						.getPackageByName(parsed.getPackage().getName().getFullyQualifiedName(), packages);

				ClassBean classBean = ClassParser.parse(typeDeclaration, packageBean.getName(), imports);
				classBean.setPathToFile(fn.getAbsolutePath());

				packageBean.addClass(classBean);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			// do nothing
		} catch (IndexOutOfBoundsException e) {
			// do nothing
		}
		return packageBean;
	}

	private static Vector<File> listJavaFiles(File pDirectory) {
		Vector<File> javaFiles = new Vector<File>();
		File[] fList = pDirectory.listFiles();

		if (fList != null) {
			for (File file : fList) {
				if (file.isFile()) {
					if (file.getName().contains(".java")) {
						javaFiles.add(file);
					}
				} else if (file.isDirectory()) {
					File directory = new File(file.getAbsolutePath());
					javaFiles.addAll(listJavaFiles(directory));
				}
			}
		}
		return javaFiles;
	}

	private static boolean isAlreadyCreated(String pPackageName, Vector<PackageBean> pPackages) {
		for (PackageBean pb : pPackages) {
			if (pb.getName().equals(pPackageName))
				return true;
		}

		return false;
	}

	private static PackageBean getPackageByName(String pPackageName, Vector<PackageBean> pPackages) {
		for (PackageBean pb : pPackages) {
			if (pb.getName().equals(pPackageName))
				return pb;
		}
		return null;
	}
}
