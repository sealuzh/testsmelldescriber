package nl.tudelft.preprocessing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.beans.PackageBean;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.logicallyRelatedBlocksDetection.BlockIdentifier;
import nl.tudelft.logicallyRelatedBlocksDetection.IdentifierAndCommentExtractor;
import nl.tudelft.logicallyRelatedBlocksDetection.PorterStemmer;
import nl.tudelft.logicallyRelatedBlocksDetection.QueryExpansionModel;

public class Preprocessing {

	public static void main(String args[]) {
		Preprocessing preprocessing = new Preprocessing();
		try {
			preprocessing.convertProjectIntoFiles("/Users/fabiopalomba/Documents/workspace/FeatureExtractionFromAndroidReviews", 
					"/Users/fabiopalomba/Desktop/dir");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void convertProjectIntoFiles(String pPathToFolder, String pProjectDir) throws IOException {
		IdentifierAndCommentExtractor extractor=new IdentifierAndCommentExtractor();
		PorterStemmer porterStemmer = new PorterStemmer();
		
		Vector<PackageBean> packages = FolderToJavaProjectConverter.convert(pPathToFolder);
		
		String projectName = pPathToFolder.substring(pPathToFolder.lastIndexOf("/"), pPathToFolder.length());
		
		File projectFolder = new File(pProjectDir+"/"+projectName);
		if(! projectFolder.exists()) 
			projectFolder.mkdirs();
		
		for(PackageBean packageBean: packages) {
			
			File packageFolder = new File(projectFolder + "/" + packageBean.getName());
			packageFolder.mkdirs();
			
			for(ClassBean classBean: packageBean.getClasses()) {
				String classContent = classBean.getTextContent();
				classContent=extractor.extractIdentifierAndComments(classContent);
				classContent = porterStemmer.step5(porterStemmer.step4(porterStemmer.step3(porterStemmer.step2(porterStemmer.step1(classContent)))));
				
				File classFileFolder = new File(packageFolder.getAbsolutePath() + "/" + classBean.getName()+".java");
				
				if(! classFileFolder.exists()) 
					classFileFolder.mkdirs();
				
				FileUtility.writeFile(classContent, classFileFolder.getAbsolutePath() + "/" + classBean.getName()+".java");
				
				File classFolder = new File(packageFolder.getAbsolutePath() + "/" + classBean.getName()+"/");
				System.out.println(classFolder.getAbsolutePath());
				
				if(! classFolder.exists())
					classFolder.mkdirs();
				
				for(MethodBean methodBean: classBean.getMethods()) {
					File methodFileFolder = new File(classFolder.getAbsolutePath() + "/" + methodBean.getName()+".java");
					
					if(! methodFileFolder.exists()) 
						methodFileFolder.mkdirs();
					
					String methodContent = methodBean.getTextContent();
					methodContent=extractor.extractIdentifierAndComments(methodContent);
					methodContent = porterStemmer.step5(porterStemmer.step4(porterStemmer.step3(porterStemmer.step2(porterStemmer.step1(methodContent)))));
					
					FileUtility.writeFile(methodContent, methodFileFolder.getAbsolutePath() + "/" + methodBean.getName()+".java");
					
					File methodFolder = new File(classFolder.getAbsolutePath() + "/" + methodBean.getName());
					methodFolder.mkdirs();
					methodFolder.mkdir();
					
					BlockIdentifier blockIdentifier=new BlockIdentifier();
					QueryExpansionModel model = new QueryExpansionModel();
					
					ArrayList<String> blocks = blockIdentifier.getBlocksByMethod(methodBean);
					int index = 0;
					
					for(String block: blocks) {
						
						String methodPart=extractor.extractIdentifierAndComments(block);
						methodPart = porterStemmer.step5(porterStemmer.step4(porterStemmer.step3(porterStemmer.step2(porterStemmer.step1(methodPart)))));
						methodPart = model.expandBlock(methodPart);
						
						if(index > 0) {
							File methodBlockFolder = new File(methodFolder.getAbsolutePath() + ("/block_"+(index)) );
							
							if(! methodBlockFolder.exists()) 
								methodBlockFolder.mkdirs();
							
							FileUtility.writeFile(methodPart, methodBlockFolder + "/block_"+index);
						}
						
						index++;
						
					}
				}
			}
		}
	}
	
}
