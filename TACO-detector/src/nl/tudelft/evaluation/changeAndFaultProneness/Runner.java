package nl.tudelft.evaluation.changeAndFaultProneness;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import nl.tudelft.evaluation.accuracy.codeSmellInstances.BlobCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.EagerTestCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.GeneralTextureCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.LongMethodCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.PromiscuousPackageCandidateInstance;
import nl.tudelft.evaluation.accuracy.parseResults.CSVFileParser;
import nl.tudelft.evaluation.refactoringDifferences.beans.Commit;
import nl.tudelft.evaluation.refactoringDifferences.dataExtraction.CommitExtractor;
import nl.tudelft.fileUtilities.FileUtility;

public class Runner {

	public static void main(String args[]) {
		File resultsDirectory = new File("/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/");
		String output="system;detection-technique;smell-instance;smell-type;change-proneness;fault-proneness;refactoring-proneness\n";
		CSVFileParser parser = new CSVFileParser();


		for(File systemResult: resultsDirectory.listFiles()) {
			if(systemResult.isDirectory() && (! systemResult.isHidden())) {

				CommitExtractor commitExtractor=new CommitExtractor();
				Vector<Commit> commits = commitExtractor.extractCommits(systemResult, "");

				System.out.println(systemResult.getAbsolutePath());

				if(commits.size() > 0) {
					for(File file: systemResult.listFiles()) {

						if(file.getName().contains("results-blob")) {
							ArrayList<BlobCandidateInstance> candidates = parser.getBlobCandidates(file);

							for(BlobCandidateInstance instance: candidates) {
								if(instance.getSmellynessProbability() > 0.7) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) { 
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}

									}

									output+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";BLOB;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";
								} else if(instance.isDecorOutput()) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) { 
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}
									}

									output+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";BLOB;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";
								}
							}

						} else if(file.getName().contains("results-longMethod")) {
							ArrayList<LongMethodCandidateInstance> candidates = parser.getLongMethodCandidates(file);

							for(LongMethodCandidateInstance instance: candidates) {
								if(instance.getSmellynessProbability() > 0.6) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) { 
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}
									}

									output+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";LONG-METHOD;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";
								} else if(instance.isDecorOutput()) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) { 
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}
									}

									output+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";LONG-METHOD;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";
								}
							}


						} else if(file.getName().contains("results-promiscuousPackage")) {
							ArrayList<PromiscuousPackageCandidateInstance> candidates = parser.getPromiscuousPackageCandidates(file);

							for(PromiscuousPackageCandidateInstance instance: candidates) {
								if(instance.getSmellynessProbability() > 0.7) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/";

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) {
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;

												break;
											}
										}
									}

									output+=systemResult.getName()+";TACO;"+instance.getPackageName()+";PROMISCUOUS-PACKAGE;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";

								} else if(instance.isDecorOutput()) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/";

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) { 
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;

												break;
											}
										}
									}

									output+=systemResult.getName()+";DECOR;"+instance.getPackageName()+";PROMISCUOUS-PACKAGE;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";
								}
							}

						} else if(file.getName().contains("results-eagerTest")) {
							ArrayList<EagerTestCandidateInstance> candidates = parser.getEagerTestCandidates(file);

							for(EagerTestCandidateInstance instance: candidates) {
								if(instance.getSmellynessProbability() > 0.6) {

									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) {
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}
									}

									output+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";EAGER-TEST;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";

								} else if(instance.isStructuralOutput()) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) {
												changeProneness++;

												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}
									}

									output+=systemResult.getName()+";DECOR;"+instance.getPackageName()+";EAGER-TEST;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";

								}
							}
						} else if(file.getName().contains("results-general-texture")) {
							ArrayList<GeneralTextureCandidateInstance> candidates = parser.getGeneralTextureCandidates(file);

							for(GeneralTextureCandidateInstance instance: candidates) {
								if(instance.isTacoOutput()) {

									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) {
												changeProneness++;
												
												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}
									}

									output+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";GENERAL-FIXTURE;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";
									
								} else if(instance.isStructuralOutput()) {
									int changeProneness=0;
									int faultProneness=0;
									int refactoringProneness=0;

									String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();

									for(Commit commit:commits) {
										for(String resource: commit.getCommittedResources()) {
											if(resource.contains(instanceFormatted)) {
												changeProneness++;
												
												if(Runner.isBugFixing(commit)) 
													faultProneness++;

												if(Runner.isRefactoring(commit)) 
													refactoringProneness++;
											}
										}
									}

									output+=systemResult.getName()+";DECOR;"+instance.getPackageName()+";GENERAL-FIXTURE;"+changeProneness+";"+faultProneness+";"+
											refactoringProneness+"\n";
								}
							}
						}
					}
				}
			}
		}

		FileUtility.writeFile(output, "/Users/fabiopalomba/Desktop/historical-perspective.csv");
	}

	private static boolean isBugFixing(Commit pCommit) {
		String commitMessage = pCommit.getCommitMessage();

		if( (commitMessage.toLowerCase().contains("fix")) || (commitMessage.toLowerCase().contains("repair"))
				|| (commitMessage.toLowerCase().contains("error")) || (commitMessage.toLowerCase().contains("avoid"))
				|| (commitMessage.toLowerCase().contains("can ")) || (commitMessage.toLowerCase().contains("bug "))
				|| (commitMessage.toLowerCase().contains("issue ")) || (commitMessage.toLowerCase().contains("#"))
				|| (commitMessage.toLowerCase().contains("exception"))) {
			return true;
		}

		return false;
	}

	private static boolean isRefactoring(Commit pCommit) {
		String commitMessage = pCommit.getCommitMessage();

		if( (commitMessage.toLowerCase().contains("renam")) || (commitMessage.toLowerCase().contains("reorganiz"))
				|| (commitMessage.toLowerCase().contains("refactor")) || (commitMessage.toLowerCase().contains("clean"))
				|| (commitMessage.toLowerCase().contains("polish")) || (commitMessage.toLowerCase().contains("typo"))
				|| (commitMessage.toLowerCase().contains("move")) || (commitMessage.toLowerCase().contains("extract"))
				|| (commitMessage.toLowerCase().contains("reorder")) || (commitMessage.toLowerCase().contains("re-order"))) {
			return true;
		}

		return false;
	}
}
