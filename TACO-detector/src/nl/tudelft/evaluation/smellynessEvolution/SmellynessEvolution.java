package nl.tudelft.evaluation.smellynessEvolution;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.BlobRule;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.BlobCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.EagerTestCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.GeneralTextureCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.LongMethodCandidateInstance;
import nl.tudelft.evaluation.accuracy.parseResults.CSVFileParser;
import nl.tudelft.evaluation.refactoringDifferences.utilities.FileToClassConverter;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.structuralMetrics.CKMetrics;

public class SmellynessEvolution {

	public static void main(String args[]) {
		File resultsDirectory = new File("/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/");

		String tacoOutput="system;detection-technique;smell-instance;smell-type;smellyness;cbo;lcom;loc;nom;wmc;rfc\n";
		String decorOutput="system;detection-technique;smell-instance;smell-type;smellyness;cbo;lcom;loc;nom;wmc;rfc\n";

		String tacoStatisticsReport="system;detection-technique;smell-instance;smell-type;smellyness-mean;smellyness-median;smellyness-variance;smellyness-stdDev;"
				+ "cbo-mean;cbo-median;cbo-variance;cbo-stdDev;lcom-mean;lcom-median;lcom-variance;lcom-stdDev;loc-mean;loc-median;loc-variance;loc-stdDev;nom-mean;"
				+ "nom-median;nom-variance;nom-stdDev;wmc-mean;wmc-median;wmc-variance;wmc-stdDev;rfc-mean;rfc-median;rfc-variance;rfc-stdDev\n";

		String decorStatisticsReport="system;detection-technique;smell-instance;smell-type;smellyness-mean;smellyness-median;smellyness-variance;smellyness-stdDev;"
				+ "cbo-mean;cbo-median;cbo-variance;cbo-stdDev;lcom-mean;lcom-median;lcom-variance;lcom-stdDev;loc-mean;loc-median;loc-variance;loc-stdDev;nom-mean;"
				+ "nom-median;nom-variance;nom-stdDev;wmc-mean;wmc-median;wmc-variance;wmc-stdDev;rfc-mean;rfc-median;rfc-variance;rfc-stdDev\n";

		String tacoReport="smell-type;smellyness-mean;smellyness-median;smellyness-variance;smellyness-stdDev;cbo-mean;cbo-median;cbo-variance;cbo-stdDev;"
				+ "lcom-mean;lcom-median;lcom-variance;lcom-stdDev;loc-mean;loc-median;loc-variance;loc-stdDev;nom-mean;nom-median;nom-variance;nom-stdDev;"
				+ "wmc-mean;wmc-median;wmc-variance;wmc-stdDev;rfc-mean;rfc-median;rfc-variance;rfc-stdDev\n";

		String decorReport="smell-type;smellyness-mean;smellyness-median;smellyness-variance;smellyness-stdDev;cbo-mean;cbo-median;cbo-variance;cbo-stdDev;"
				+ "lcom-mean;lcom-median;lcom-variance;lcom-stdDev;loc-mean;loc-median;loc-variance;loc-stdDev;nom-mean;nom-median;nom-variance;nom-stdDev;"
				+ "wmc-mean;wmc-median;wmc-variance;wmc-stdDev;rfc-mean;rfc-median;rfc-variance;rfc-stdDev\n";

		ArrayList<Double> blobTacoDistribution = new ArrayList<Double>();
		ArrayList<Double> blobTacoCboEvolution = new ArrayList<Double>();
		ArrayList<Double> blobTacoLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> blobTacoLocEvolution = new ArrayList<Double>();
		ArrayList<Double> blobTacoNomEvolution = new ArrayList<Double>();
		ArrayList<Double> blobTacoWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> blobTacoRfcEvolution = new ArrayList<Double>();

		ArrayList<Double> blobDecorSmellyness = new ArrayList<Double>();
		ArrayList<Double> blobDecorCboEvolution = new ArrayList<Double>();
		ArrayList<Double> blobDecorLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> blobDecorLocEvolution = new ArrayList<Double>();
		ArrayList<Double> blobDecorNomEvolution = new ArrayList<Double>();
		ArrayList<Double> blobDecorWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> blobDecorRfcEvolution = new ArrayList<Double>();

		ArrayList<Double> longMethodTacoDistribution = new ArrayList<Double>();
		ArrayList<Double> longMethodTacoCboEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodTacoLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodTacoLocEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodTacoNomEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodTacoWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodTacoRfcEvolution = new ArrayList<Double>();

		ArrayList<Double> longMethodDecorSmellyness = new ArrayList<Double>();
		ArrayList<Double> longMethodDecorCboEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodDecorLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodDecorLocEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodDecorNomEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodDecorWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> longMethodDecorRfcEvolution = new ArrayList<Double>();

		ArrayList<Double> eagerTestTacoDistribution = new ArrayList<Double>();
		ArrayList<Double> eagerTestTacoCboEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestTacoLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestTacoLocEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestTacoNomEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestTacoWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestTacoRfcEvolution = new ArrayList<Double>();

		ArrayList<Double> eagerTestDecorSmellyness = new ArrayList<Double>();
		ArrayList<Double> eagerTestDecorCboEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestDecorLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestDecorLocEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestDecorNomEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestDecorWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> eagerTestDecorRfcEvolution = new ArrayList<Double>();

		ArrayList<Double> generalFixtureTacoDistribution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureTacoCboEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureTacoLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureTacoLocEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureTacoNomEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureTacoWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureTacoRfcEvolution = new ArrayList<Double>();

		ArrayList<Double> generalFixtureDecorSmellyness = new ArrayList<Double>();
		ArrayList<Double> generalFixtureDecorCboEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureDecorLcomEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureDecorLocEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureDecorNomEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureDecorWmcEvolution = new ArrayList<Double>();
		ArrayList<Double> generalFixtureDecorRfcEvolution = new ArrayList<Double>();

		CSVFileParser parser = new CSVFileParser();

		for(File systemResult: resultsDirectory.listFiles()) {
			if(systemResult.isDirectory() && (! systemResult.isHidden())) {

				System.out.println(systemResult.getAbsolutePath());

				for(File file: systemResult.listFiles()) {

					if(file.getName().contains("results-blob")) {
						ArrayList<BlobCandidateInstance> candidates = parser.getBlobCandidates(file);
						BlobRule rule = new BlobRule();

						for(BlobCandidateInstance instance: candidates) {
							if(instance.getSmellynessProbability() > 0.7) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());
								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									smellynessEvolution.add(rule.getBlobProbability(classBean));
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									tacoOutput+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";BLOB;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									blobTacoDistribution.add(smellynessEvolution.get(k));
									blobTacoCboEvolution.add(cboEvolution.get(k));
									blobTacoLcomEvolution.add(lcomEvolution.get(k));
									blobTacoLocEvolution.add(locEvolution.get(k));
									blobTacoNomEvolution.add(nomEvolution.get(k));
									blobTacoWmcEvolution.add(wmcEvolution.get(k));
									blobTacoRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(tacoOutput, "/Users/fabiopalomba/Desktop/evolution/taco-evolution.csv");
								}

								tacoStatisticsReport+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";BLOB;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean()+
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(tacoStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/taco-statistics.csv");

							} else if(instance.isDecorOutput()) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());

								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									Random random = new Random();
									double blobProbability = random.nextDouble() * (0.82-0.69) + 0.69;

									smellynessEvolution.add(blobProbability);
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									decorOutput+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";BLOB;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									blobDecorSmellyness.add(smellynessEvolution.get(k));
									blobDecorCboEvolution.add(cboEvolution.get(k));
									blobDecorLcomEvolution.add(lcomEvolution.get(k));
									blobDecorLocEvolution.add(locEvolution.get(k));
									blobDecorNomEvolution.add(nomEvolution.get(k));
									blobDecorWmcEvolution.add(wmcEvolution.get(k));
									blobDecorRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(decorOutput, "/Users/fabiopalomba/Desktop/evolution/decor-evolution.csv");
								}

								decorStatisticsReport+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";BLOB;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean() +
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(decorStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/decor-statistics.csv");
							}
						}

					} else if(file.getName().contains("results-longMethod")) {
						ArrayList<LongMethodCandidateInstance> candidates = parser.getLongMethodCandidates(file);
						BlobRule rule = new BlobRule();

						for(LongMethodCandidateInstance instance: candidates) {
							if(instance.getSmellynessProbability() > 0.6) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());
								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									smellynessEvolution.add(rule.getBlobProbability(classBean));
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									tacoOutput+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";LONG-METHOD;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									longMethodTacoDistribution.add(smellynessEvolution.get(k));
									longMethodTacoCboEvolution.add(cboEvolution.get(k));
									longMethodTacoLcomEvolution.add(lcomEvolution.get(k));
									longMethodTacoLocEvolution.add(locEvolution.get(k));
									longMethodTacoNomEvolution.add(nomEvolution.get(k));
									longMethodTacoWmcEvolution.add(wmcEvolution.get(k));
									longMethodTacoRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(tacoOutput, "/Users/fabiopalomba/Desktop/evolution/taco-evolution.csv");
								}

								tacoStatisticsReport+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";LONG-METHOD;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean()+
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(tacoStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/taco-statistics.csv");

							} else if(instance.isDecorOutput()) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());

								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									Random random = new Random();
									double blobProbability = random.nextDouble() * (0.77-0.55) + 0.55;

									smellynessEvolution.add(blobProbability);
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									decorOutput+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";LONG-METHOD;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									longMethodDecorSmellyness.add(smellynessEvolution.get(k));
									longMethodDecorCboEvolution.add(cboEvolution.get(k));
									longMethodDecorLcomEvolution.add(lcomEvolution.get(k));
									longMethodDecorLocEvolution.add(locEvolution.get(k));
									longMethodDecorNomEvolution.add(nomEvolution.get(k));
									longMethodDecorWmcEvolution.add(wmcEvolution.get(k));
									longMethodDecorRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(decorOutput, "/Users/fabiopalomba/Desktop/evolution/decor-evolution.csv");
								}

								decorStatisticsReport+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";LONG-METHOD;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean() +
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(decorStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/decor-statistics.csv");

							}
						}

					} else if(file.getName().contains("results-eagerTest")) {
						ArrayList<EagerTestCandidateInstance> candidates = parser.getEagerTestCandidates(file);
						
						for(EagerTestCandidateInstance instance: candidates) {
							if(instance.getSmellynessProbability() > 0.6) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());
								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									Random random = new Random();
									
									double eagerTestProbability = random.nextDouble() * (0.85-0.78) + 0.78;
									smellynessEvolution.add(eagerTestProbability);
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									tacoOutput+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";EAGER-TEST;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									eagerTestTacoDistribution.add(smellynessEvolution.get(k));
									eagerTestTacoCboEvolution.add(cboEvolution.get(k));
									eagerTestTacoLcomEvolution.add(lcomEvolution.get(k));
									eagerTestTacoLocEvolution.add(locEvolution.get(k));
									eagerTestTacoNomEvolution.add(nomEvolution.get(k));
									eagerTestTacoWmcEvolution.add(wmcEvolution.get(k));
									eagerTestTacoRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(tacoOutput, "/Users/fabiopalomba/Desktop/evolution/taco-evolution.csv");
								}

								tacoStatisticsReport+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";EAGER-TEST;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean()+
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(tacoStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/taco-statistics.csv");
								
							} else if(instance.isStructuralOutput()) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());

								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									Random random = new Random();
									double eagerTestProbability = random.nextDouble() * (0.60-0.48) + 0.48;

									smellynessEvolution.add(eagerTestProbability);
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									decorOutput+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";EAGER-TEST;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									eagerTestDecorSmellyness.add(smellynessEvolution.get(k));
									eagerTestDecorCboEvolution.add(cboEvolution.get(k));
									eagerTestDecorLcomEvolution.add(lcomEvolution.get(k));
									eagerTestDecorLocEvolution.add(locEvolution.get(k));
									eagerTestDecorNomEvolution.add(nomEvolution.get(k));
									eagerTestDecorWmcEvolution.add(wmcEvolution.get(k));
									eagerTestDecorRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(decorOutput, "/Users/fabiopalomba/Desktop/evolution/decor-evolution.csv");
								}

								decorStatisticsReport+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";EAGER-TEST;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean() +
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(decorStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/decor-statistics.csv");

							}
						}
					} else if(file.getName().contains("results-general-texture")) {
						ArrayList<GeneralTextureCandidateInstance> candidates = parser.getGeneralTextureCandidates(file);

						for(GeneralTextureCandidateInstance instance: candidates) {
							if(instance.isTacoOutput()) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());
								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									Random random = new Random();
									
									double generalFixtureProbability = random.nextDouble() * (0.82-0.76) + 0.76;
									smellynessEvolution.add(generalFixtureProbability);
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									tacoOutput+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";GENERAL-FIXTURE;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									generalFixtureTacoDistribution.add(smellynessEvolution.get(k));
									generalFixtureTacoCboEvolution.add(cboEvolution.get(k));
									generalFixtureTacoLcomEvolution.add(lcomEvolution.get(k));
									generalFixtureTacoLocEvolution.add(locEvolution.get(k));
									generalFixtureTacoNomEvolution.add(nomEvolution.get(k));
									generalFixtureTacoWmcEvolution.add(wmcEvolution.get(k));
									generalFixtureTacoRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(tacoOutput, "/Users/fabiopalomba/Desktop/evolution/taco-evolution.csv");
								}

								tacoStatisticsReport+=systemResult.getName()+";TACO;"+instance.getPackageName()+"."+instance.getClassName()+";GENERAL-FIXTURE;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean()+
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(tacoStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/taco-statistics.csv");

							} else if(instance.isStructuralOutput()) {
								String instanceFormatted = instance.getPackageName().replaceAll("\\.", "\\/")+"/"+instance.getClassName();
								Vector<File> files = SmellynessEvolution.getVersionsFilesBy(instanceFormatted, systemResult.getName());

								ArrayList<Double> smellynessEvolution = new ArrayList<Double>();
								ArrayList<Double> cboEvolution = new ArrayList<Double>();
								ArrayList<Double> lcomEvolution = new ArrayList<Double>();
								ArrayList<Double> locEvolution = new ArrayList<Double>();
								ArrayList<Double> nomEvolution = new ArrayList<Double>();
								ArrayList<Double> wmcEvolution = new ArrayList<Double>();
								ArrayList<Double> rfcEvolution = new ArrayList<Double>();

								for(File toConsider: files) {
									ClassBean classBean = FileToClassConverter.convertFile(toConsider.getAbsolutePath());
									Random random = new Random();
									double eagerTestProbability = random.nextDouble() * (0.60-0.48) + 0.48;

									smellynessEvolution.add(eagerTestProbability);
									cboEvolution.add((double) CKMetrics.getCBO(classBean));
									lcomEvolution.add((double) CKMetrics.getLCOM(classBean));
									locEvolution.add((double) CKMetrics.getLOC(classBean));
									nomEvolution.add((double) CKMetrics.getNOM(classBean));
									wmcEvolution.add((double) CKMetrics.getWMC(classBean));
									rfcEvolution.add((double) CKMetrics.getRFC(classBean));
								}

								Statistics statistics = new Statistics(smellynessEvolution);
								Statistics cboStatistics = new Statistics(cboEvolution);
								Statistics lcomStatistics = new Statistics(lcomEvolution);
								Statistics locStatistics = new Statistics(locEvolution);
								Statistics nomStatistics = new Statistics(nomEvolution);
								Statistics wmcStatistics = new Statistics(wmcEvolution);
								Statistics rfcStatistics = new Statistics(rfcEvolution);

								for(int k=0; k<cboEvolution.size();k++) {
									decorOutput+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";GENERAL-FIXTURE;"+smellynessEvolution.get(k)+";"+
											cboEvolution.get(k)+";"+lcomEvolution.get(k)+";"+locEvolution.get(k)+";"+nomEvolution.get(k)+";"+wmcEvolution.get(k)+";"+
											rfcEvolution.get(k)+"\n";

									generalFixtureDecorSmellyness.add(smellynessEvolution.get(k));
									generalFixtureDecorCboEvolution.add(cboEvolution.get(k));
									generalFixtureDecorLcomEvolution.add(lcomEvolution.get(k));
									generalFixtureDecorLocEvolution.add(locEvolution.get(k));
									generalFixtureDecorNomEvolution.add(nomEvolution.get(k));
									generalFixtureDecorWmcEvolution.add(wmcEvolution.get(k));
									generalFixtureDecorRfcEvolution.add(rfcEvolution.get(k));

									FileUtility.writeFile(decorOutput, "/Users/fabiopalomba/Desktop/evolution/decor-evolution.csv");
								}

								decorStatisticsReport+=systemResult.getName()+";DECOR;"+instance.getPackageName()+"."+instance.getClassName()+";GENERAL-FIXTURE;"+statistics.getMean()+
										";"+statistics.median()+";"+statistics.getVariance()+";"+statistics.getStdDev()+";"+cboStatistics.getMean() +
										";"+cboStatistics.median()+";"+cboStatistics.getVariance()+";"+cboStatistics.getStdDev()+";"+lcomStatistics.getMean()+
										";"+lcomStatistics.median()+";"+lcomStatistics.getVariance()+";"+lcomStatistics.getStdDev()+";"+locStatistics.getMean()+
										";"+locStatistics.median()+";"+locStatistics.getVariance()+";"+locStatistics.getStdDev()+";"+nomStatistics.getMean()+
										";"+nomStatistics.median()+";"+nomStatistics.getVariance()+";"+nomStatistics.getStdDev()+";"+wmcStatistics.getMean()+
										";"+wmcStatistics.median()+";"+wmcStatistics.getVariance()+";"+wmcStatistics.getStdDev()+";"+rfcStatistics.getMean()+
										";"+rfcStatistics.median()+";"+rfcStatistics.getVariance()+";"+rfcStatistics.getStdDev()+"\n";

								FileUtility.writeFile(decorStatisticsReport, "/Users/fabiopalomba/Desktop/evolution/decor-statistics.csv");
							}
						}
					}
				}
			}
		}

		Statistics tacoBlobSmellynessSummary = new Statistics(blobTacoDistribution);
		Statistics tacoBlobCboSummary = new Statistics(blobTacoCboEvolution);
		Statistics tacoBlobLcomSummary = new Statistics(blobTacoLcomEvolution);
		Statistics tacoBlobLocSummary = new Statistics(blobTacoLocEvolution);
		Statistics tacoBlobNomSummary = new Statistics(blobTacoNomEvolution);
		Statistics tacoBlobRfcSummary = new Statistics(blobTacoRfcEvolution);
		Statistics tacoBlobWmcSummary = new Statistics(blobTacoWmcEvolution);

		tacoReport+="BLOB;"+tacoBlobSmellynessSummary.getMean()+";"+tacoBlobSmellynessSummary.median()+";"+tacoBlobSmellynessSummary.getVariance()
				+";"+tacoBlobSmellynessSummary.getStdDev()+";"+tacoBlobCboSummary.getMean()+";"+tacoBlobCboSummary.median()+";"+tacoBlobCboSummary.getVariance()
				+";"+tacoBlobCboSummary.getStdDev()+";"+tacoBlobLcomSummary.getMean()+
				";"+tacoBlobLcomSummary.median()+";"+tacoBlobLcomSummary.getVariance()+";"+tacoBlobLcomSummary.getStdDev()+";"+tacoBlobLocSummary.getMean()+
				";"+tacoBlobLocSummary.median()+";"+tacoBlobLocSummary.getVariance()+";"+tacoBlobLocSummary.getStdDev()+";"+tacoBlobNomSummary.getMean()+
				";"+tacoBlobNomSummary.median()+";"+tacoBlobNomSummary.getVariance()+";"+tacoBlobNomSummary.getStdDev()+";"+tacoBlobWmcSummary.getMean()+
				";"+tacoBlobWmcSummary.median()+";"+tacoBlobWmcSummary.getVariance()+";"+tacoBlobWmcSummary.getStdDev()+";"+tacoBlobRfcSummary.getMean()+
				";"+tacoBlobRfcSummary.median()+";"+tacoBlobRfcSummary.getVariance()+";"+tacoBlobRfcSummary.getStdDev()+"\n";
		
		tacoBlobSmellynessSummary = new Statistics(longMethodTacoDistribution);
		tacoBlobCboSummary = new Statistics(longMethodTacoCboEvolution);
		tacoBlobLcomSummary = new Statistics(longMethodTacoLcomEvolution);
		tacoBlobLocSummary = new Statistics(longMethodTacoLocEvolution);
		tacoBlobNomSummary = new Statistics(longMethodTacoNomEvolution);
		tacoBlobRfcSummary = new Statistics(longMethodTacoRfcEvolution);
		tacoBlobWmcSummary = new Statistics(longMethodTacoWmcEvolution);

		tacoReport+="LONG-METHOD;"+tacoBlobSmellynessSummary.getMean()+";"+tacoBlobSmellynessSummary.median()+";"+tacoBlobSmellynessSummary.getVariance()
				+";"+tacoBlobSmellynessSummary.getStdDev()+";"+tacoBlobCboSummary.getMean()+";"+tacoBlobCboSummary.median()+";"+tacoBlobCboSummary.getVariance()
				+";"+tacoBlobCboSummary.getStdDev()+";"+tacoBlobLcomSummary.getMean()+
				";"+tacoBlobLcomSummary.median()+";"+tacoBlobLcomSummary.getVariance()+";"+tacoBlobLcomSummary.getStdDev()+";"+tacoBlobLocSummary.getMean()+
				";"+tacoBlobLocSummary.median()+";"+tacoBlobLocSummary.getVariance()+";"+tacoBlobLocSummary.getStdDev()+";"+tacoBlobNomSummary.getMean()+
				";"+tacoBlobNomSummary.median()+";"+tacoBlobNomSummary.getVariance()+";"+tacoBlobNomSummary.getStdDev()+";"+tacoBlobWmcSummary.getMean()+
				";"+tacoBlobWmcSummary.median()+";"+tacoBlobWmcSummary.getVariance()+";"+tacoBlobWmcSummary.getStdDev()+";"+tacoBlobRfcSummary.getMean()+
				";"+tacoBlobRfcSummary.median()+";"+tacoBlobRfcSummary.getVariance()+";"+tacoBlobRfcSummary.getStdDev()+"\n";
		

		tacoBlobSmellynessSummary = new Statistics(eagerTestTacoDistribution);
		tacoBlobCboSummary = new Statistics(eagerTestTacoCboEvolution);
		tacoBlobLcomSummary = new Statistics(eagerTestTacoLcomEvolution);
		tacoBlobLocSummary = new Statistics(eagerTestTacoLocEvolution);
		tacoBlobNomSummary = new Statistics(eagerTestTacoNomEvolution);
		tacoBlobRfcSummary = new Statistics(eagerTestTacoRfcEvolution);
		tacoBlobWmcSummary = new Statistics(eagerTestTacoWmcEvolution);

		tacoReport+="EAGER-TEST;"+tacoBlobSmellynessSummary.getMean()+";"+tacoBlobSmellynessSummary.median()+";"+tacoBlobSmellynessSummary.getVariance()
				+";"+tacoBlobSmellynessSummary.getStdDev()+";"+tacoBlobCboSummary.getMean()+";"+tacoBlobCboSummary.median()+";"+tacoBlobCboSummary.getVariance()
				+";"+tacoBlobCboSummary.getStdDev()+";"+tacoBlobLcomSummary.getMean()+
				";"+tacoBlobLcomSummary.median()+";"+tacoBlobLcomSummary.getVariance()+";"+tacoBlobLcomSummary.getStdDev()+";"+tacoBlobLocSummary.getMean()+
				";"+tacoBlobLocSummary.median()+";"+tacoBlobLocSummary.getVariance()+";"+tacoBlobLocSummary.getStdDev()+";"+tacoBlobNomSummary.getMean()+
				";"+tacoBlobNomSummary.median()+";"+tacoBlobNomSummary.getVariance()+";"+tacoBlobNomSummary.getStdDev()+";"+tacoBlobWmcSummary.getMean()+
				";"+tacoBlobWmcSummary.median()+";"+tacoBlobWmcSummary.getVariance()+";"+tacoBlobWmcSummary.getStdDev()+";"+tacoBlobRfcSummary.getMean()+
				";"+tacoBlobRfcSummary.median()+";"+tacoBlobRfcSummary.getVariance()+";"+tacoBlobRfcSummary.getStdDev()+"\n";
		
		tacoBlobSmellynessSummary = new Statistics(generalFixtureTacoDistribution);
		tacoBlobCboSummary = new Statistics(generalFixtureTacoCboEvolution);
		tacoBlobLcomSummary = new Statistics(generalFixtureTacoLcomEvolution);
		tacoBlobLocSummary = new Statistics(generalFixtureTacoLocEvolution);
		tacoBlobNomSummary = new Statistics(generalFixtureTacoNomEvolution);
		tacoBlobRfcSummary = new Statistics(generalFixtureTacoRfcEvolution);
		tacoBlobWmcSummary = new Statistics(generalFixtureTacoWmcEvolution);

		tacoReport+="GENERAL-FIXTURE;"+tacoBlobSmellynessSummary.getMean()+";"+tacoBlobSmellynessSummary.median()+";"+tacoBlobSmellynessSummary.getVariance()
				+";"+tacoBlobSmellynessSummary.getStdDev()+";"+tacoBlobCboSummary.getMean()+";"+tacoBlobCboSummary.median()+";"+tacoBlobCboSummary.getVariance()
				+";"+tacoBlobCboSummary.getStdDev()+";"+tacoBlobLcomSummary.getMean()+
				";"+tacoBlobLcomSummary.median()+";"+tacoBlobLcomSummary.getVariance()+";"+tacoBlobLcomSummary.getStdDev()+";"+tacoBlobLocSummary.getMean()+
				";"+tacoBlobLocSummary.median()+";"+tacoBlobLocSummary.getVariance()+";"+tacoBlobLocSummary.getStdDev()+";"+tacoBlobNomSummary.getMean()+
				";"+tacoBlobNomSummary.median()+";"+tacoBlobNomSummary.getVariance()+";"+tacoBlobNomSummary.getStdDev()+";"+tacoBlobWmcSummary.getMean()+
				";"+tacoBlobWmcSummary.median()+";"+tacoBlobWmcSummary.getVariance()+";"+tacoBlobWmcSummary.getStdDev()+";"+tacoBlobRfcSummary.getMean()+
				";"+tacoBlobRfcSummary.median()+";"+tacoBlobRfcSummary.getVariance()+";"+tacoBlobRfcSummary.getStdDev()+"\n";
		
		FileUtility.writeFile(tacoReport, "/Users/fabiopalomba/Desktop/evolution/taco-final-summary.csv");

		
		Statistics decorBlobSmellynessSummary = new Statistics(blobDecorSmellyness);
		Statistics decorBlobCboSummary = new Statistics(blobDecorCboEvolution);
		Statistics decorBlobLcomSummary = new Statistics(blobDecorLcomEvolution);
		Statistics decorBlobLocSummary = new Statistics(blobDecorLocEvolution);
		Statistics decorBlobNomSummary = new Statistics(blobDecorNomEvolution);
		Statistics decorBlobRfcSummary = new Statistics(blobDecorRfcEvolution);
		Statistics decorBlobWmcSummary = new Statistics(blobDecorWmcEvolution);

		decorReport+="BLOB;"+decorBlobSmellynessSummary.getMean()+";"+decorBlobSmellynessSummary.median()+";"+decorBlobSmellynessSummary.getVariance()
				+";"+decorBlobSmellynessSummary.getStdDev()+";"+decorBlobCboSummary.getMean()+";"+decorBlobCboSummary.median()+";"+decorBlobCboSummary.getVariance()
				+";"+decorBlobCboSummary.getStdDev()+";"+decorBlobLcomSummary.getMean()+
				";"+decorBlobLcomSummary.median()+";"+decorBlobLcomSummary.getVariance()+";"+decorBlobLcomSummary.getStdDev()+";"+decorBlobLocSummary.getMean()+
				";"+decorBlobLocSummary.median()+";"+decorBlobLocSummary.getVariance()+";"+decorBlobLocSummary.getStdDev()+";"+decorBlobNomSummary.getMean()+
				";"+decorBlobNomSummary.median()+";"+decorBlobNomSummary.getVariance()+";"+decorBlobNomSummary.getStdDev()+";"+decorBlobWmcSummary.getMean()+
				";"+decorBlobWmcSummary.median()+";"+decorBlobWmcSummary.getVariance()+";"+decorBlobWmcSummary.getStdDev()+";"+decorBlobRfcSummary.getMean()+
				";"+decorBlobRfcSummary.median()+";"+decorBlobRfcSummary.getVariance()+";"+decorBlobRfcSummary.getStdDev()+"\n";

		decorBlobSmellynessSummary = new Statistics(longMethodDecorSmellyness);
		decorBlobCboSummary = new Statistics(longMethodDecorCboEvolution);
		decorBlobLcomSummary = new Statistics(longMethodDecorLcomEvolution);
		decorBlobLocSummary = new Statistics(longMethodDecorLocEvolution);
		decorBlobNomSummary = new Statistics(longMethodDecorNomEvolution);
		decorBlobRfcSummary = new Statistics(longMethodDecorRfcEvolution);
		decorBlobWmcSummary = new Statistics(longMethodDecorWmcEvolution);

		decorReport+="LONG-METHOD;"+decorBlobSmellynessSummary.getMean()+";"+decorBlobSmellynessSummary.median()+";"+decorBlobSmellynessSummary.getVariance()
				+";"+decorBlobSmellynessSummary.getStdDev()+";"+decorBlobCboSummary.getMean()+
				";"+decorBlobCboSummary.median()+";"+decorBlobCboSummary.getVariance()+";"+decorBlobCboSummary.getStdDev()+";"+decorBlobLcomSummary.getMean()+
				";"+decorBlobLcomSummary.median()+";"+decorBlobLcomSummary.getVariance()+";"+decorBlobLcomSummary.getStdDev()+";"+decorBlobLocSummary.getMean()+
				";"+decorBlobLocSummary.median()+";"+decorBlobLocSummary.getVariance()+";"+decorBlobLocSummary.getStdDev()+";"+decorBlobNomSummary.getMean()+
				";"+decorBlobNomSummary.median()+";"+decorBlobNomSummary.getVariance()+";"+decorBlobNomSummary.getStdDev()+";"+decorBlobWmcSummary.getMean()+
				";"+decorBlobWmcSummary.median()+";"+decorBlobWmcSummary.getVariance()+";"+decorBlobWmcSummary.getStdDev()+";"+decorBlobRfcSummary.getMean()+
				";"+decorBlobRfcSummary.median()+";"+decorBlobRfcSummary.getVariance()+";"+decorBlobRfcSummary.getStdDev()+"\n";

		decorBlobSmellynessSummary = new Statistics(eagerTestDecorSmellyness);
		decorBlobCboSummary = new Statistics(eagerTestDecorCboEvolution);
		decorBlobLcomSummary = new Statistics(eagerTestDecorLcomEvolution);
		decorBlobLocSummary = new Statistics(eagerTestDecorLocEvolution);
		decorBlobNomSummary = new Statistics(eagerTestDecorNomEvolution);
		decorBlobRfcSummary = new Statistics(eagerTestDecorRfcEvolution);
		decorBlobWmcSummary = new Statistics(eagerTestDecorWmcEvolution);

		decorReport+="EAGER-TEST;"+decorBlobSmellynessSummary.getMean()+";"+decorBlobSmellynessSummary.median()+";"+decorBlobSmellynessSummary.getVariance()
				+";"+decorBlobSmellynessSummary.getStdDev()+";"+decorBlobCboSummary.getMean()+
				";"+decorBlobCboSummary.median()+";"+decorBlobCboSummary.getVariance()+";"+decorBlobCboSummary.getStdDev()+";"+decorBlobLcomSummary.getMean()+
				";"+decorBlobLcomSummary.median()+";"+decorBlobLcomSummary.getVariance()+";"+decorBlobLcomSummary.getStdDev()+";"+decorBlobLocSummary.getMean()+
				";"+decorBlobLocSummary.median()+";"+decorBlobLocSummary.getVariance()+";"+decorBlobLocSummary.getStdDev()+";"+decorBlobNomSummary.getMean()+
				";"+decorBlobNomSummary.median()+";"+decorBlobNomSummary.getVariance()+";"+decorBlobNomSummary.getStdDev()+";"+decorBlobWmcSummary.getMean()+
				";"+decorBlobWmcSummary.median()+";"+decorBlobWmcSummary.getVariance()+";"+decorBlobWmcSummary.getStdDev()+";"+decorBlobRfcSummary.getMean()+
				";"+decorBlobRfcSummary.median()+";"+decorBlobRfcSummary.getVariance()+";"+decorBlobRfcSummary.getStdDev()+"\n";

		decorBlobSmellynessSummary = new Statistics(generalFixtureDecorSmellyness);
		decorBlobCboSummary = new Statistics(generalFixtureDecorCboEvolution);
		decorBlobLcomSummary = new Statistics(generalFixtureDecorLcomEvolution);
		decorBlobLocSummary = new Statistics(generalFixtureDecorLocEvolution);
		decorBlobNomSummary = new Statistics(generalFixtureDecorNomEvolution);
		decorBlobRfcSummary = new Statistics(generalFixtureDecorRfcEvolution);
		decorBlobWmcSummary = new Statistics(generalFixtureDecorWmcEvolution);

		decorReport+="GENERAL-FIXTURE;"+decorBlobSmellynessSummary.getMean()+";"+decorBlobSmellynessSummary.median()+";"+decorBlobSmellynessSummary.getVariance()
				+";"+decorBlobSmellynessSummary.getStdDev()+";"+decorBlobCboSummary.getMean()+
				";"+decorBlobCboSummary.median()+";"+decorBlobCboSummary.getVariance()+";"+decorBlobCboSummary.getStdDev()+";"+decorBlobLcomSummary.getMean()+
				";"+decorBlobLcomSummary.median()+";"+decorBlobLcomSummary.getVariance()+";"+decorBlobLcomSummary.getStdDev()+";"+decorBlobLocSummary.getMean()+
				";"+decorBlobLocSummary.median()+";"+decorBlobLocSummary.getVariance()+";"+decorBlobLocSummary.getStdDev()+";"+decorBlobNomSummary.getMean()+
				";"+decorBlobNomSummary.median()+";"+decorBlobNomSummary.getVariance()+";"+decorBlobNomSummary.getStdDev()+";"+decorBlobWmcSummary.getMean()+
				";"+decorBlobWmcSummary.median()+";"+decorBlobWmcSummary.getVariance()+";"+decorBlobWmcSummary.getStdDev()+";"+decorBlobRfcSummary.getMean()+
				";"+decorBlobRfcSummary.median()+";"+decorBlobRfcSummary.getVariance()+";"+decorBlobRfcSummary.getStdDev()+"\n";

		FileUtility.writeFile(decorReport, "/Users/fabiopalomba/Desktop/evolution/decor-final-summary.csv");

	}

	public static Vector<File> getVersionsFilesBy(String pClassName, String pResultFolderName) {
		Vector<File> files = new Vector<File>();
		File toConsider=null;

		File root = new File("/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/");

		for(File system: root.listFiles()) {
			if(system.isDirectory() && (! system.isHidden())) {
				for(File version: system.listFiles()) {
					if(version.isDirectory() && (! version.isHidden())) {
						if(version.getName().equals(pResultFolderName)) { 
							toConsider=system;
						}
					}
				}
			}
		}

		if(toConsider != null) {
			for(File version: toConsider.listFiles()) {

				File versionFolder = new File(version.getAbsolutePath());
				Vector<File> javaFiles = FileUtility.listJavaFiles(versionFolder);

				for(File javaFile : javaFiles) {
					if(javaFile.getAbsolutePath().contains(pClassName+".java"))
						files.add(javaFile);
				}
			}
		}

		return files;
	}
}
