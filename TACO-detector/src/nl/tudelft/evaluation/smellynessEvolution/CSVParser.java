package nl.tudelft.evaluation.smellynessEvolution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import nl.tudelft.fileUtilities.FileUtility;

public class CSVParser {

	public static void main(String args[]) {

		try {
			String[] tacoEvolutionLines = FileUtility.readFile("/Users/fabiopalomba/Desktop/evolution/taco-evolution.csv").split("\n");
			String[] decorEvolutionLines = FileUtility.readFile("/Users/fabiopalomba/Desktop/evolution/decor-evolution.csv").split("\n");

			CSVParser.computeDistrubution(tacoEvolutionLines, "TACO");
			CSVParser.computeDistrubution(decorEvolutionLines, "DECOR");


		} catch (IOException e) {

			e.printStackTrace();
		}




	}

	public static void computeDistrubution(String[] pLines, String pDetectionTechnique) {

		ArrayList<String> projects = CSVParser.getProjectNames(pLines);
		String blobDistribution ="smell-instance;average-smellyness-variation\n";
		String longMethodDistribution ="smell-instance;average-smellyness-variation\n";
		String eagerTestDistribution ="smell-instance;average-smellyness-variation\n";
		String generalFixtureDistribution ="smell-instance;average-smellyness-variation\n";

		int decorInstances = 0;
		int tacoInstances = 0;
		
		for(String project: projects) {
			
			File projectFolder = new File("/Users/fabiopalomba/Desktop/evolution/" + project);
			if(!projectFolder.exists()) 
				projectFolder.mkdirs();
			
			String projectBlobDistribution ="smell-instance;average-smellyness-variation\n";
			String projectLongMethodDistribution ="smell-instance;average-smellyness-variation\n";
			String projectEagerTestDistribution ="smell-instance;average-smellyness-variation\n";
			String projectGeneralFixtureDistribution ="smell-instance;average-smellyness-variation\n";
			
			ArrayList<String> blobs = CSVParser.getCodeSmellsBy(pLines, project, "BLOB");
			
			
			if(pDetectionTechnique.equals("TACO"))
				tacoInstances+=blobs.size();
			else decorInstances+=blobs.size();
			
			ArrayList<String> longMethods = CSVParser.getCodeSmellsBy(pLines, project, "LONG-METHOD");
			ArrayList<String> eagerTests = CSVParser.getCodeSmellsBy(pLines, project, "EAGER-TEST");
			ArrayList<String> generalFixtures = CSVParser.getCodeSmellsBy(pLines, project, "GENERAL-FIXTURE");

			for(String blob: blobs) {
				String blobInstance = blob.split(";")[2];
				ArrayList<String> evolutionData = CSVParser.getEvolutionDataBy(blobs, blobInstance);

				double sum = 0.0;

				for(int k=1; k < evolutionData.size(); k++) {

					if(pDetectionTechnique.equals("DECOR")) {
						Double oldDataPoint = -1.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k-1), "codeSmell")) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k), "codeSmell")) {
							newDataPoint = 1.0;
						}

						if(newDataPoint == 1.0 && oldDataPoint == 1.0)
							sum += 1.0;
						else sum += (newDataPoint - oldDataPoint);

					} else {

						Double oldDataPoint = -1.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k-1))) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k))) {
							newDataPoint = 1.0;
						}

						sum += (newDataPoint - oldDataPoint);

						//System.out.println(blobInstance + ": " + newDataPoint + "  " + oldDataPoint + ": " + (newDataPoint - oldDataPoint));

						/*Double oldDataPoint = Double.parseDouble(evolutionData.get(k-1).split(";")[4]);
						Double newDataPoint = Double.parseDouble(evolutionData.get(k).split(";")[4]);

						sum += (newDataPoint - oldDataPoint);*/
					}
				}

				if(! blobDistribution.contains(blobInstance + ";" + (sum/evolutionData.size()))) {
					blobDistribution += blobInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
				
				if(! projectBlobDistribution.contains(blobInstance + ";" + (sum/evolutionData.size()))) {
					projectBlobDistribution += blobInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
			}

			for(String longMethod: longMethods) {
				String longMethodInstance = longMethod.split(";")[2];
				ArrayList<String> evolutionData = CSVParser.getEvolutionDataBy(longMethods, longMethodInstance);

				double sum = 0.0;

				for(int k=1; k < evolutionData.size(); k++) {

					if(pDetectionTechnique.equals("DECOR")) {
						Double oldDataPoint = 0.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k-1), "codeSmell")) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k), "codeSmell")) {
							newDataPoint = 1.0;
						}

						if(newDataPoint == 1.0 && oldDataPoint == 1.0)
							sum += 1.0;
						else sum += (newDataPoint - oldDataPoint);

					} else {

						Double oldDataPoint = -1.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k-1))) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k))) {
							newDataPoint = 1.0;
						}

						sum += (newDataPoint - oldDataPoint);
						/*
						Double oldDataPoint = Double.parseDouble(evolutionData.get(k-1).split(";")[4]);
						Double newDataPoint = Double.parseDouble(evolutionData.get(k).split(";")[4]);

						sum += (newDataPoint - oldDataPoint);*/
					}
				}

				if(! longMethodDistribution.contains(longMethodInstance + ";" + (sum/evolutionData.size()))) {	
					longMethodDistribution += longMethodInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
				
				if(! projectLongMethodDistribution.contains(longMethodInstance + ";" + (sum/evolutionData.size()))) {
					projectLongMethodDistribution += longMethodInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
			}

			for(String eagerTest: eagerTests) {
				String eagerTestInstance = eagerTest.split(";")[2];
				ArrayList<String> evolutionData = CSVParser.getEvolutionDataBy(eagerTests, eagerTestInstance);

				double sum = 0.0;

				for(int k=1; k < evolutionData.size(); k++) {

					if(pDetectionTechnique.equals("DECOR")) {
						Double oldDataPoint = -1.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k-1), "testSmell")) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k), "testSmell")) {
							newDataPoint = 1.0;
						}

						if(newDataPoint == 1.0 && oldDataPoint == 1.0)
							sum += 1.0;
						else sum += (newDataPoint - oldDataPoint);

					} else {

						Double oldDataPoint = -1.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k-1))) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k))) {
							newDataPoint = 1.0;
						}

						if(newDataPoint.isNaN() || oldDataPoint.isNaN()) {
							sum += 0.0;
						} else {
							if(newDataPoint == 1.0 && oldDataPoint == 1.0)
								sum += 1.0;
							else sum += (newDataPoint - oldDataPoint);
						}
					}
				}

				if(! eagerTestDistribution.contains(eagerTestInstance + ";" + (sum/evolutionData.size()))) {	
					eagerTestDistribution += eagerTestInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
				
				if(! projectEagerTestDistribution.contains(eagerTestInstance + ";" + (sum/evolutionData.size()))) {	
					projectEagerTestDistribution += eagerTestInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
				
			}	

			for(String generalFixture: generalFixtures) {
				String generalFixtureInstance = generalFixture.split(";")[2];
				ArrayList<String> evolutionData = CSVParser.getEvolutionDataBy(generalFixtures, generalFixtureInstance);

				double sum = 0.0;

				for(int k=1; k < evolutionData.size(); k++) {

					if(pDetectionTechnique.equals("DECOR")) {
						Double oldDataPoint = -1.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k-1), "testSmell")) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isDECORCodeSmell(evolutionData.get(k), "testSmell")) {
							newDataPoint = 1.0;
						}

						if(newDataPoint == 1.0 && oldDataPoint == 1.0)
							sum += 1.0;
						else sum += (newDataPoint - oldDataPoint);

					} else {

						Double oldDataPoint = -1.0;
						Double newDataPoint = 0.0;

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k-1))) {
							oldDataPoint = 1.0;
						}

						if(CSVParser.isTACOCodeSmell(evolutionData.get(k))) {
							newDataPoint = 1.0;
						}


						if(newDataPoint.isNaN() || oldDataPoint.isNaN()) {
							sum += 0.0;
						} else {
							if(newDataPoint == 1.0 && oldDataPoint == 1.0)
								sum += 1.0;
							else sum += (newDataPoint - oldDataPoint);
						}
					}
				}

				if(! generalFixtureDistribution.contains(generalFixtureInstance + ";" + (sum/evolutionData.size()))) {	
					generalFixtureDistribution += generalFixtureInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
				
				if(! projectGeneralFixtureDistribution.contains(generalFixtureInstance + ";" + (sum/evolutionData.size()))) {	
					projectGeneralFixtureDistribution += generalFixtureInstance + ";" + (sum/evolutionData.size()) + "\n";	
				}
			}
			
			FileUtility.writeFile(projectBlobDistribution, projectFolder + "/" + pDetectionTechnique.toLowerCase() + "-blob-variation.csv");
			FileUtility.writeFile(projectLongMethodDistribution, projectFolder + "/" + pDetectionTechnique.toLowerCase() + "-lm-variation.csv");
			FileUtility.writeFile(projectEagerTestDistribution, projectFolder + "/" + pDetectionTechnique.toLowerCase() + "-et-variation.csv");
			FileUtility.writeFile(projectGeneralFixtureDistribution, projectFolder + "/" + pDetectionTechnique.toLowerCase() + "-gf-variation.csv");
			
		}

		if(pDetectionTechnique.equals("TACO"))
			System.out.println("TACO: " + tacoInstances);
		else System.out.println("DECOR: " + decorInstances);
		
		FileUtility.writeFile(blobDistribution, "/Users/fabiopalomba/Desktop/evolution/" + pDetectionTechnique.toLowerCase() + "-blob-variation.csv");
		FileUtility.writeFile(longMethodDistribution, "/Users/fabiopalomba/Desktop/evolution/" + pDetectionTechnique.toLowerCase() + "-lm-variation.csv");
		FileUtility.writeFile(eagerTestDistribution, "/Users/fabiopalomba/Desktop/evolution/" + pDetectionTechnique.toLowerCase() + "-et-variation.csv");
		FileUtility.writeFile(generalFixtureDistribution, "/Users/fabiopalomba/Desktop/evolution/" + pDetectionTechnique.toLowerCase() + "-gf-variation.csv");

	}

	public static ArrayList<String> getEvolutionDataBy(ArrayList<String> pCodeSmells, String pSmellInstance) {
		ArrayList<String> evolutionData = new ArrayList<String>();

		for(String line: pCodeSmells) {
			String name = line.split(";")[2];

			if(name.equals(pSmellInstance)) {
				if(! evolutionData.contains(line))
					evolutionData.add(line);
			}
		}

		return evolutionData;
	}

	public static ArrayList<String> getCodeSmellsBy(String[] pLines, String pProjectName, String pSmellType){
		ArrayList<String> codeSmells = new ArrayList<String>();

		for(String line: pLines) {
			String name = line.split(";")[0];
			String smellType = line.split(";")[3];

			if(name.equals(pProjectName)) {
				if(smellType.equals(pSmellType)) {
					if(! codeSmells.contains(line))
						codeSmells.add(line);
				}
			}
		}

		return codeSmells;
	}

	public static ArrayList<String> getProjectNames(String[] pLines){
		ArrayList<String> projects = new ArrayList<String>();

		for(String line: pLines) {
			String name = line.split(";")[0];

			if(! projects.contains(name)) {
				projects.add(name);
			}
		}

		return projects;
	}

	public static boolean isDECORCodeSmell(String pLine, String pType) {
		Double lcom = Double.parseDouble(pLine.split(";")[6]);
		Double loc = Double.parseDouble(pLine.split(";")[7]);
		//Double wmc = Double.parseDouble(pLine.split(";")[9]);
		Double rfc = Double.parseDouble(pLine.split(";")[10]);

		if(pType.equals("codeSmell")) {
			if(lcom > 100.0) {
				if(loc > 600.0) {
					return true;
				}
			}
		} else {
			if(loc.isNaN() || rfc.isNaN())
				return false;

			if(loc > 80.0) {
				if(rfc > 15) {
					return true;
				}

			}
		}

		return false;
	}

	public static boolean isTACOCodeSmell(String pLine){
		Double smellyness = Double.parseDouble(pLine.split(";")[4]);

		if(smellyness >= 0.8){
			return true;
		}

		return false;
	}

}
