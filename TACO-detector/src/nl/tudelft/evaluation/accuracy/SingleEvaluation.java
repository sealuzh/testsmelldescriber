package nl.tudelft.evaluation.accuracy;

public class SingleEvaluation {
	private String evaluationName;
	private String techniqueName;
	private Double techniqueValue;
	private double truePositive;
	private double falsePositive;
	private double oracleSize;
	
	public SingleEvaluation(String pEvaluationType, String pKey, Double pValue) {
		this.setEvaluationName(pEvaluationType);
		this.setTechniqueName(pKey);
		this.setTechniqueValue(pValue);
	}

	public String getTechniqueName() {
		return techniqueName;
	}

	public void setTechniqueName(String techniqueName) {
		this.techniqueName = techniqueName;
	}

	public Double getTechniqueValue() {
		return techniqueValue;
	}

	public void setTechniqueValue(Double techniqueValue) {
		this.techniqueValue = techniqueValue;
	}

	public String getEvaluationName() {
		return evaluationName;
	}

	public void setEvaluationName(String evaluationName) {
		this.evaluationName = evaluationName;
	}

	public double getTruePositive() {
		return truePositive;
	}

	public void setTruePositive(double truePositive) {
		this.truePositive = truePositive;
	}

	public double getFalsePositive() {
		return falsePositive;
	}

	public void setFalsePositive(double falsePositive) {
		this.falsePositive = falsePositive;
	}

	public double getOracleSize() {
		return oracleSize;
	}

	public void setOracleSize(double oracleSize) {
		this.oracleSize = oracleSize;
	}	
	
}