package nl.tudelft.evaluation.accuracy.parseResults;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import nl.tudelft.evaluation.accuracy.SingleEvaluation;
import nl.tudelft.fileUtilities.FileUtility;

public class ResultsWriter {
	
	String globalFile="smell;system;threshold;oracle-size;taco-identified;taco-tp;taco-fp;taco-precision;taco-recall;taco-fmeasure;decor-identified;decor-tp;decor-fp;decor-precision;decor-recall;decor-fmeasure\n";
	
	public void writeResults(ArrayList<SingleEvaluation> pWhat, File pWhere, double pThreshold, File pSystem) {
		String fileContent = "";

		double oracleSize = 0.0;
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;
		
		double tacoPrecision = 0.0;
		double tacoRecall = 0.0;
		double tacoFMeasure = 0.0;

		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		
		double decorPrecision = 0.0;
		double decorRecall = 0.0;
		double decorFMeasure = 0.0;

		if(pWhere.exists()) {
			try {
				fileContent = FileUtility.readFile(pWhere.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			fileContent+="threshold;oracle-size;taco-identified;taco-tp;taco-fp;taco-precision;taco-recall;taco-fmeasure;decor-identified;decor-tp;decor-fp;decor-precision;decor-recall;decor-fmeasure\n";
		}

		for(SingleEvaluation singleEvaluation: pWhat) {
			if(singleEvaluation.getEvaluationName().equals("precision")) {

				if(singleEvaluation.getTechniqueName().equals("TACO")) {
					tacoPrecision = singleEvaluation.getTechniqueValue();
					tacoTruePositive = singleEvaluation.getTruePositive();
					tacoFalsePositive = singleEvaluation.getFalsePositive();
					oracleSize = singleEvaluation.getOracleSize();
					
				}
				else {
					decorPrecision = singleEvaluation.getTechniqueValue();
					decorTruePositive = singleEvaluation.getTruePositive();
					decorFalsePositive = singleEvaluation.getFalsePositive();
				}

			} else if(singleEvaluation.getEvaluationName().equals("recall")) {

				if(singleEvaluation.getTechniqueName().equals("TACO")) 
					tacoRecall = singleEvaluation.getTechniqueValue();
				else decorRecall = singleEvaluation.getTechniqueValue();
				
			} else if(singleEvaluation.getEvaluationName().equals("f-measure")) {

				if(singleEvaluation.getTechniqueName().equals("TACO")) 
					tacoFMeasure = singleEvaluation.getTechniqueValue();
				else decorFMeasure = singleEvaluation.getTechniqueValue();
			}
		}

		fileContent+=pThreshold+";"+oracleSize+";"+(tacoTruePositive+tacoFalsePositive)+";"+tacoTruePositive+";"+tacoFalsePositive+";"+tacoPrecision+";"+tacoRecall+";"+tacoFMeasure+";"
				+(decorTruePositive+decorFalsePositive)+";"+decorTruePositive+";"+decorFalsePositive+";"+decorPrecision+";"+decorRecall+";"+decorFMeasure+"\n";
		
		String codeSmell = "";
		if(pWhere.getName().contains("blob")) {
			codeSmell="blob";
		} else if(pWhere.getName().contains("longMethod")) {
			codeSmell="long-method";
		} else if(pWhere.getName().contains("promiscuous")) {
			codeSmell="promiscuous-package";
		} else if(pWhere.getName().contains("eager")) {
			codeSmell="eager-test";
		} else if(pWhere.getName().contains("general")) {
			codeSmell="general-texture";
		} else if(pWhere.getName().contains("feature")) {
			codeSmell="feature-envy";
		} else if(pWhere.getName().contains("misplaced")) {
			codeSmell="misplaced-class";
		}
		
		globalFile+=codeSmell+";"+pSystem.getName()+";"+pThreshold+";"+oracleSize+";"+(tacoTruePositive+tacoFalsePositive)+";"+tacoTruePositive+";"+tacoFalsePositive+";"+tacoPrecision+";"+tacoRecall+";"+tacoFMeasure+";"
				+(decorTruePositive+decorFalsePositive)+";"+decorTruePositive+";"+decorFalsePositive+";"+decorPrecision+";"+decorRecall+";"+decorFMeasure+"\n";
		
		FileUtility.writeFile(fileContent, pWhere.getAbsolutePath());
		FileUtility.writeFile(globalFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/summary.csv");
		
	}
}
