package nl.tudelft.evaluation.accuracy.parseResults;

import nl.tudelft.evaluation.accuracy.codeSmellInstances.BlobCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.EagerTestCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.FeatureEnvyCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.GeneralTextureCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.LongMethodCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.MisplacedClassCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.PromiscuousPackageCandidateInstance;

public class OracleFinder {

	public static boolean getOracle(BlobCandidateInstance pInstance) {

		int loc = pInstance.getLoc();
		int lcom = pInstance.getLcom();
		int numberOfMethods = pInstance.getNumberOfMethods();
		double smellyness = pInstance.getSmellynessProbability();

		if(loc > 500) {
			if(numberOfMethods > 40) {
				if(lcom > 200) {
					if(smellyness > 0.7)
						 return true;
				}
			}
		}

		return false;
	}

	public static  boolean getOracle(PromiscuousPackageCandidateInstance pInstance) {
		int numberOfClasses = pInstance.getNumberOfClasses();
		double smellyness = pInstance.getSmellynessProbability();

		if(numberOfClasses > 20) {
			if(smellyness > 0.70) 
				return true;
		}

		return false;
	}

	public static  boolean getOracle(LongMethodCandidateInstance pInstance) {
		int loc = pInstance.getLoc();
		double smellyness = pInstance.getSmellynessProbability();

		if(loc >= 60) {
			if(smellyness > 0.70) 
				return true;
		}

		return false;
	}

	public static  boolean getOracle(GeneralTextureCandidateInstance pInstance) {
		int setUpLoc = pInstance.getSetUpLOC();
		int numberOfMethods = pInstance.getNumberOfMethods();

		if(setUpLoc >= 10) {
			if(numberOfMethods > 10)
				return true;
		}

		return false;
	}

	public static  boolean getOracle(EagerTestCandidateInstance pInstance) {
		double smellyness = pInstance.getSmellynessProbability();

		if(smellyness >= 0.75) {
			return true;
		}

		return false;
	}

	public static boolean getOracle(FeatureEnvyCandidateInstance pInstance) {

		if(pInstance.getTacoEnviedClassName().equals(pInstance.getClassName())) {
			if(pInstance.getDecorEnviedClassName().equals(pInstance.getClassName()))
				return false;
		}

		double actualClassDependencies = pInstance.getDependenciesWithActualClass();
		double enviedClassDependencies = pInstance.getDependenciesWithEnviedClass();
		double enviedClassSimilarity = pInstance.getSimilarity();

		if( (enviedClassDependencies - actualClassDependencies) > 30 ) {
			return true;
		} else if( enviedClassSimilarity > 0.77) { 
			return true;
		}

		return false;
	}
	
	public static boolean getOracle(MisplacedClassCandidateInstance pInstance) {

		if(pInstance.getTacoEnviedPackageName().equals(pInstance.getPackageName())) {
			if(pInstance.getDecorEnviedPackageName().equals(pInstance.getPackageName()))
				return false;
		}

		double actualClassDependencies = pInstance.getDependenciesWithActualPackage();
		double enviedClassDependencies = pInstance.getDependenciesWithEnviedPackage();
		double enviedClassSimilarity = pInstance.getSimilarityWithEnviedPackage();

		if( (enviedClassDependencies - actualClassDependencies) > 30 ) {
			return true;
		} else if( enviedClassSimilarity > 0.77) { 
			return true;
		}

		return false;
	}
	
}
