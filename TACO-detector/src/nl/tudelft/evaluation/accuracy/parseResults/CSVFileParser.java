package nl.tudelft.evaluation.accuracy.parseResults;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import nl.tudelft.evaluation.accuracy.codeSmellInstances.BlobCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.EagerTestCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.FeatureEnvyCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.GeneralTextureCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.LongMethodCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.MisplacedClassCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.PromiscuousPackageCandidateInstance;

public class CSVFileParser {

	public ArrayList<BlobCandidateInstance> getBlobCandidates(File pCSVFile) {
		ArrayList<BlobCandidateInstance> candidates = new ArrayList<BlobCandidateInstance>();

		ArrayList<String> lines = this.readFileLineByLine(pCSVFile);

		int index=0;

		for(String line: lines) {

			if(index > 0) {

				BlobCandidateInstance instance = new BlobCandidateInstance();
				String fields[] = line.split(";");

				instance.setPackageName(fields[0]);
				instance.setClassName(fields[1]);
				instance.setNumberOfMethods(Integer.parseInt(fields[2]));
				instance.setLoc(Integer.parseInt(fields[3]));
				instance.setLcom(Integer.parseInt(fields[4]));
				instance.setSmellynessProbability(Double.parseDouble(fields[5]));
				instance.setDecorOutput(Boolean.parseBoolean(fields[6]));

				candidates.add(instance);
			} else {
				index++;
			}
		}

		return candidates;
	}

	public ArrayList<PromiscuousPackageCandidateInstance> getPromiscuousPackageCandidates(File pCSVFile) {
		ArrayList<PromiscuousPackageCandidateInstance> candidates = new ArrayList<PromiscuousPackageCandidateInstance>();

		ArrayList<String> lines = this.readFileLineByLine(pCSVFile);

		int index=0;

		for(String line: lines) {
			if(index > 0) {

				PromiscuousPackageCandidateInstance instance = new PromiscuousPackageCandidateInstance();
				String fields[] = line.split(";");

				instance.setPackageName(fields[0]);
				instance.setNumberOfClasses(Integer.parseInt(fields[1]));
				instance.setSmellynessProbability(Double.parseDouble(fields[2]));
				instance.setDecorOutput(Boolean.parseBoolean(fields[3]));

				candidates.add(instance);
			} else {
				index++;
			}
		}

		return candidates;
	}

	public ArrayList<LongMethodCandidateInstance> getLongMethodCandidates(File pCSVFile) {
		ArrayList<LongMethodCandidateInstance> candidates = new ArrayList<LongMethodCandidateInstance>();

		ArrayList<String> lines = this.readFileLineByLine(pCSVFile);
		int index=0;

		for(String line: lines) {
			if(index > 0) {
				LongMethodCandidateInstance instance = new LongMethodCandidateInstance();
				String fields[] = line.split(";");

				instance.setPackageName(fields[0]);
				instance.setClassName(fields[1]);
				instance.setMethodName(fields[2]);
				instance.setLoc(Integer.parseInt(fields[3]));
				instance.setSmellynessProbability(Double.parseDouble(fields[4]));
				instance.setDecorOutput(Boolean.parseBoolean(fields[5]));

				candidates.add(instance);
			} else {
				index++;
			}
		}

		return candidates;
	}

	public ArrayList<EagerTestCandidateInstance> getEagerTestCandidates(File pCSVFile) {
		ArrayList<EagerTestCandidateInstance> candidates = new ArrayList<EagerTestCandidateInstance>();

		ArrayList<String> lines = this.readFileLineByLine(pCSVFile);
		int index=0;

		for(String line: lines) {
			if(index > 0) {
				EagerTestCandidateInstance instance = new EagerTestCandidateInstance();
				String fields[] = line.split(";");

				instance.setPackageName(fields[0]);
				instance.setClassName(fields[1]);
				instance.setSmellynessProbability(Double.parseDouble(fields[2]));
				instance.setStructuralOutput(Boolean.parseBoolean(fields[3]));

				candidates.add(instance);
			} else {
				index++;
			}
		}

		return candidates;
	}

	public ArrayList<GeneralTextureCandidateInstance> getGeneralTextureCandidates(File pCSVFile) {
		ArrayList<GeneralTextureCandidateInstance> candidates = new ArrayList<GeneralTextureCandidateInstance>();

		ArrayList<String> lines = this.readFileLineByLine(pCSVFile);
		int index=0;

		for(String line: lines) {

			if(index > 0) {
				GeneralTextureCandidateInstance instance = new GeneralTextureCandidateInstance();
				String fields[] = line.split(";");

				instance.setPackageName(fields[0]);
				instance.setClassName(fields[1]);
				instance.setNumberOfMethods(Integer.parseInt(fields[2]));
				instance.setSetUpLOC(Integer.parseInt(fields[3]));
				instance.setTacoOutput(Boolean.parseBoolean(fields[4]));
				instance.setStructuralOutput(Boolean.parseBoolean(fields[5]));

				candidates.add(instance);
			} else {
				index++;
			}
		}

		return candidates;
	}

	public ArrayList<FeatureEnvyCandidateInstance> getFeatureEnvyCandidates(File pCSVFile) {
		ArrayList<FeatureEnvyCandidateInstance> candidates = new ArrayList<FeatureEnvyCandidateInstance>();

		ArrayList<String> lines = this.readFileLineByLine(pCSVFile);
		int index=0;

		for(String line: lines) {
			if(index > 0) {	
				FeatureEnvyCandidateInstance instance = new FeatureEnvyCandidateInstance();
				String fields[] = line.split(";");

				instance.setPackageName(fields[0]);
				instance.setClassName(fields[1]);
				instance.setMethodName(fields[2]);
				instance.setTacoEnviedClassPackageName(fields[3]);
				instance.setTacoEnviedClassName(fields[4]);
				instance.setSimilarity(Double.parseDouble(fields[5]));
				instance.setDecorEnviedClassName(fields[6]);
				instance.setDependenciesWithActualClass(Integer.parseInt(fields[7]));
				instance.setDependenciesWithEnviedClass(Integer.parseInt(fields[8]));

				candidates.add(instance);
			} else {
				index++;
			}
		}

		return candidates;
	}

	public ArrayList<MisplacedClassCandidateInstance> getMisplacedClassCandidates(File pCSVFile) {
		ArrayList<MisplacedClassCandidateInstance> candidates = new ArrayList<MisplacedClassCandidateInstance>();

		ArrayList<String> lines = this.readFileLineByLine(pCSVFile);
		int index=0;

		for(String line: lines) {
			if(index > 0) {
				MisplacedClassCandidateInstance instance = new MisplacedClassCandidateInstance();
				String fields[] = line.split(";");

				instance.setPackageName(fields[0]);
				instance.setClassName(fields[1]);
				instance.setTacoEnviedPackageName(fields[2]);
				instance.setSimilarityWithEnviedPackage(Double.parseDouble(fields[3]));
				instance.setDecorEnviedPackageName(fields[4]);
				instance.setDependenciesWithActualPackage(Integer.parseInt(fields[5]));
				instance.setDependenciesWithEnviedPackage(Integer.parseInt(fields[6]));

				candidates.add(instance);
			} else {
				index++;
			}
		}

		return candidates;
	}

	private ArrayList<String> readFileLineByLine(File pFile) {
		ArrayList<String> lines = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(pFile))) {
			String line;
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lines;
	}
}