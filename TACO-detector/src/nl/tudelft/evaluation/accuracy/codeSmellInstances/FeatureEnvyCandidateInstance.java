package nl.tudelft.evaluation.accuracy.codeSmellInstances;

public class FeatureEnvyCandidateInstance {

	private String packageName;
	private String className;
	private String methodName;
	private String tacoEnviedClassPackageName;
	private String tacoEnviedClassName;
	private double similarity;
	private String decorEnviedClassName;
	private double dependenciesWithActualClass;
	private double dependenciesWithEnviedClass;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getTacoEnviedClassPackageName() {
		return tacoEnviedClassPackageName;
	}
	public void setTacoEnviedClassPackageName(String tacoEnviedClassPackageName) {
		this.tacoEnviedClassPackageName = tacoEnviedClassPackageName;
	}
	public String getTacoEnviedClassName() {
		return tacoEnviedClassName;
	}
	public void setTacoEnviedClassName(String tacoEnviedClassName) {
		this.tacoEnviedClassName = tacoEnviedClassName;
	}
	public double getSimilarity() {
		return similarity;
	}
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
	public String getDecorEnviedClassName() {
		return decorEnviedClassName;
	}
	public void setDecorEnviedClassName(String decorEnviedClassName) {
		this.decorEnviedClassName = decorEnviedClassName;
	}
	public double getDependenciesWithActualClass() {
		return dependenciesWithActualClass;
	}
	public void setDependenciesWithActualClass(double dependenciesWithActualClass) {
		this.dependenciesWithActualClass = dependenciesWithActualClass;
	}
	public double getDependenciesWithEnviedClass() {
		return dependenciesWithEnviedClass;
	}
	public void setDependenciesWithEnviedClass(double dependenciesWithEnviedClass) {
		this.dependenciesWithEnviedClass = dependenciesWithEnviedClass;
	}	
}