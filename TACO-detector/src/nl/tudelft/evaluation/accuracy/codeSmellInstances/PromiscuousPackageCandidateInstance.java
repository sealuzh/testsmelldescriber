package nl.tudelft.evaluation.accuracy.codeSmellInstances;

public class PromiscuousPackageCandidateInstance {

	private String packageName;
	private int numberOfClasses;
	private double smellynessProbability;
	private boolean decorOutput;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public int getNumberOfClasses() {
		return numberOfClasses;
	}
	public void setNumberOfClasses(int numberOfClasses) {
		this.numberOfClasses = numberOfClasses;
	}
	public double getSmellynessProbability() {
		return smellynessProbability;
	}
	public void setSmellynessProbability(double smellynessProbability) {
		this.smellynessProbability = smellynessProbability;
	}
	public boolean isDecorOutput() {
		return decorOutput;
	}
	public void setDecorOutput(boolean decorOutput) {
		this.decorOutput = decorOutput;
	}	
}