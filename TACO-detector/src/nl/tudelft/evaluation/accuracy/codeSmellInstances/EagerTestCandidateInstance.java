package nl.tudelft.evaluation.accuracy.codeSmellInstances;

public class EagerTestCandidateInstance {

	private String packageName;
	private String className;
	private double smellynessProbability;
	private boolean structuralOutput;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public double getSmellynessProbability() {
		return smellynessProbability;
	}
	public void setSmellynessProbability(double smellynessProbability) {
		this.smellynessProbability = smellynessProbability;
	}
	public boolean isStructuralOutput() {
		return structuralOutput;
	}
	public void setStructuralOutput(boolean structuralOutput) {
		this.structuralOutput = structuralOutput;
	}	
}