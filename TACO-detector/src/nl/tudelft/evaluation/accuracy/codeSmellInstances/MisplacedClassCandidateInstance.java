package nl.tudelft.evaluation.accuracy.codeSmellInstances;

public class MisplacedClassCandidateInstance {

	private String packageName;
	private String className;
	private String tacoEnviedPackageName;
	private double similarityWithEnviedPackage;
	private String decorEnviedPackageName;
	private double dependenciesWithActualPackage;
	private double dependenciesWithEnviedPackage;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getTacoEnviedPackageName() {
		return tacoEnviedPackageName;
	}
	public void setTacoEnviedPackageName(String tacoEnviedPackageName) {
		this.tacoEnviedPackageName = tacoEnviedPackageName;
	}
	public double getSimilarityWithEnviedPackage() {
		return similarityWithEnviedPackage;
	}
	public void setSimilarityWithEnviedPackage(double similarityWithEnviedPackage) {
		this.similarityWithEnviedPackage = similarityWithEnviedPackage;
	}
	public String getDecorEnviedPackageName() {
		return decorEnviedPackageName;
	}
	public void setDecorEnviedPackageName(String decorEnviedPackageName) {
		this.decorEnviedPackageName = decorEnviedPackageName;
	}
	public double getDependenciesWithActualPackage() {
		return dependenciesWithActualPackage;
	}
	public void setDependenciesWithActualPackage(
			double dependenciesWithActualPackage) {
		this.dependenciesWithActualPackage = dependenciesWithActualPackage;
	}
	public double getDependenciesWithEnviedPackage() {
		return dependenciesWithEnviedPackage;
	}
	public void setDependenciesWithEnviedPackage(
			double dependenciesWithEnviedPackage) {
		this.dependenciesWithEnviedPackage = dependenciesWithEnviedPackage;
	}	
}