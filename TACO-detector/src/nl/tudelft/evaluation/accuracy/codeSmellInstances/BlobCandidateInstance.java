package nl.tudelft.evaluation.accuracy.codeSmellInstances;

public class BlobCandidateInstance {

	private String packageName;
	private String className;
	private int numberOfMethods;
	private int loc;
	private int lcom;
	private double smellynessProbability;
	private boolean decorOutput;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public int getNumberOfMethods() {
		return numberOfMethods;
	}
	public void setNumberOfMethods(int numberOfMethods) {
		this.numberOfMethods = numberOfMethods;
	}
	public int getLoc() {
		return loc;
	}
	public void setLoc(int loc) {
		this.loc = loc;
	}
	public int getLcom() {
		return lcom;
	}
	public void setLcom(int lcom) {
		this.lcom = lcom;
	}
	public double getSmellynessProbability() {
		return smellynessProbability;
	}
	public void setSmellynessProbability(double smellynessProbability) {
		this.smellynessProbability = smellynessProbability;
	}
	public boolean isDecorOutput() {
		return decorOutput;
	}
	public void setDecorOutput(boolean decorOutput) {
		this.decorOutput = decorOutput;
	}
}