package nl.tudelft.evaluation.accuracy.codeSmellInstances;

public class LongMethodCandidateInstance {

	private String packageName;
	private String className;
	private String methodName;
	private int loc;
	private double smellynessProbability;
	private boolean decorOutput;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public int getLoc() {
		return loc;
	}
	public void setLoc(int loc) {
		this.loc = loc;
	}
	public double getSmellynessProbability() {
		return smellynessProbability;
	}
	public void setSmellynessProbability(double smellynessProbability) {
		this.smellynessProbability = smellynessProbability;
	}
	public boolean isDecorOutput() {
		return decorOutput;
	}
	public void setDecorOutput(boolean decorOutput) {
		this.decorOutput = decorOutput;
	}
}