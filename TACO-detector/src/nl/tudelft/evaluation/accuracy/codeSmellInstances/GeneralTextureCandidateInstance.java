package nl.tudelft.evaluation.accuracy.codeSmellInstances;

public class GeneralTextureCandidateInstance {

	private String packageName;
	private String className;
	private int numberOfMethods;
	private int setUpLOC;
	private boolean tacoOutput;
	private boolean structuralOutput;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public int getNumberOfMethods() {
		return numberOfMethods;
	}
	public void setNumberOfMethods(int numberOfMethods) {
		this.numberOfMethods = numberOfMethods;
	}
	public int getSetUpLOC() {
		return setUpLOC;
	}
	public void setSetUpLOC(int setUpLOC) {
		this.setUpLOC = setUpLOC;
	}
	public boolean isTacoOutput() {
		return tacoOutput;
	}
	public void setTacoOutput(boolean tacoOutput) {
		this.tacoOutput = tacoOutput;
	}
	public boolean isStructuralOutput() {
		return structuralOutput;
	}
	public void setStructuralOutput(boolean structuralOutput) {
		this.structuralOutput = structuralOutput;
	}	
}