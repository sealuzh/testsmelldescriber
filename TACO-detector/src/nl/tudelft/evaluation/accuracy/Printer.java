package nl.tudelft.evaluation.accuracy;

import java.io.File;
import java.util.ArrayList;

import nl.tudelft.evaluation.accuracy.codeSmellInstances.EagerTestCandidateInstance;
import nl.tudelft.evaluation.accuracy.parseResults.CSVFileParser;
import nl.tudelft.fileUtilities.FileUtility;

public class Printer {

	public static void main(String args[]) {
		File resultsDirectory = new File("/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/");
		CSVFileParser parser = new CSVFileParser();
		
		String outputFile="project;eager-test;smellyness-probability\n";
		
		for(File systemResult: resultsDirectory.listFiles()) {
			if(systemResult.isDirectory() && (! systemResult.isHidden())) {
				for(File file: systemResult.listFiles()) {

					if(file.getName().contains("results-eagerTest")) {
						ArrayList<EagerTestCandidateInstance> candidates = parser.getEagerTestCandidates(file);
						
						for(EagerTestCandidateInstance instance: candidates) {
							if(instance.getSmellynessProbability() >= 0.8) {
								outputFile+=systemResult+";"+instance.getPackageName()+"."+instance.getClassName()
									+";"+instance.getSmellynessProbability()+"\n";
							}
						}
						
						FileUtility.writeFile(outputFile, "/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/textual-smell-detection/taco-eagerTest.csv");
					}
				}
			}
		}
	}
}
