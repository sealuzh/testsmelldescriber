package nl.tudelft.evaluation.accuracy;

import java.io.File;
import java.util.ArrayList;

import nl.tudelft.evaluation.accuracy.codeSmellInstances.BlobCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.EagerTestCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.GeneralTextureCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.LongMethodCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.PromiscuousPackageCandidateInstance;
import nl.tudelft.evaluation.accuracy.parseResults.CSVFileParser;
import nl.tudelft.evaluation.accuracy.parseResults.ResultsWriter;

public class Runner {

	public static void main(String args[]) {
		File resultsDirectory = new File("/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/");
		File resultsSummaryDirectory = new File("/Users/fabiopalomba/Documents/PhD/Papers/Working/Antipatterns/TACO/tu-delft-experiment/Summary/");

		AccuraryAnalyzer analyzer = new AccuraryAnalyzer();
		CSVFileParser parser = new CSVFileParser();
		ResultsWriter resultsWriter = new ResultsWriter();

		for(File systemResult: resultsDirectory.listFiles()) {
			if(systemResult.isDirectory() && (! systemResult.isHidden())) {

				File systemSummaryFile = new File(resultsSummaryDirectory+"/" + systemResult.getName());
				if(! systemSummaryFile.exists())
					systemSummaryFile.mkdirs();

				for(File file: systemResult.listFiles()) {

					if(file.getName().contains("results-blob")) {

						File systemBlobSummary = new File(systemSummaryFile + "/blobAccuracy.csv");

						for(double index = 1.0; index < 10.0; index++) {
							ArrayList<BlobCandidateInstance> candidates = parser.getBlobCandidates(file);
							ArrayList<SingleEvaluation> evaluations = analyzer.getBlobDetectionAccuracy(candidates, (index/10) );
							resultsWriter.writeResults(evaluations, systemBlobSummary, (index/10), systemResult );
						}
					} else if(file.getName().contains("results-longMethod")) {

						File systemLongMethodSummary = new File(systemSummaryFile + "/longMethodAccuracy.csv");

						for(double index = 1.0; index < 10.0; index++) {
							ArrayList<LongMethodCandidateInstance> candidates = parser.getLongMethodCandidates(file);
							ArrayList<SingleEvaluation> evaluations = analyzer.getLongMethodDetectionAccuracy(candidates, (index/10) );
							resultsWriter.writeResults(evaluations, systemLongMethodSummary, (index/10), systemResult );
						}
					} else if(file.getName().contains("results-promiscuousPackage")) {

						File systemPromiscuousPackageSummary = new File(systemSummaryFile + "/promiscuousPackageAccuracy.csv");

						for(double index = 1.0; index < 10.0; index++) {
							ArrayList<PromiscuousPackageCandidateInstance> candidates = parser.getPromiscuousPackageCandidates(file);
							ArrayList<SingleEvaluation> evaluations = analyzer.getPromiscuousPackageDetectionAccuracy( candidates, (index/10) );
							resultsWriter.writeResults(evaluations, systemPromiscuousPackageSummary, (index/10), systemResult );
						}
					} else if(file.getName().contains("results-eagerTest")) {

						File systemEagerTestSummary = new File(systemSummaryFile + "/eagerTestAccuracy.csv");

						for(double index = 1.0; index < 10.0; index++) {
							ArrayList<EagerTestCandidateInstance> candidates = parser.getEagerTestCandidates(file);
							ArrayList<SingleEvaluation> evaluations = analyzer.getEagerTestDetectionAccuracy( candidates, (index/10) );
							resultsWriter.writeResults(evaluations, systemEagerTestSummary, (index/10), systemResult );
						}
					} else if(file.getName().contains("results-general-texture")) {

						File systemGeneraltextureSummary = new File(systemSummaryFile + "/generalTextureAccuracy.csv");

						for(double index = 1.0; index < 10.0; index++) {
							ArrayList<GeneralTextureCandidateInstance> candidates = parser.getGeneralTextureCandidates(file);
							ArrayList<SingleEvaluation> evaluations = analyzer.getGeneralTextureDetectionAccuracy(candidates);
							resultsWriter.writeResults(evaluations, systemGeneraltextureSummary, (index/10), systemResult );
						}
					}
				}
			}
		}
	}
}
