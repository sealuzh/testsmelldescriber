package nl.tudelft.evaluation.accuracy;

import java.util.ArrayList;

import nl.tudelft.evaluation.accuracy.codeSmellInstances.BlobCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.EagerTestCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.FeatureEnvyCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.GeneralTextureCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.LongMethodCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.MisplacedClassCandidateInstance;
import nl.tudelft.evaluation.accuracy.codeSmellInstances.PromiscuousPackageCandidateInstance;
import nl.tudelft.evaluation.accuracy.parseResults.OracleFinder;

public class AccuraryAnalyzer {
	
	public ArrayList<SingleEvaluation> getBlobDetectionAccuracy(ArrayList<BlobCandidateInstance> pCandidates, double pTacoThreshold) {
		ArrayList<SingleEvaluation> evaluations = new ArrayList<SingleEvaluation>();
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;

		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		double oracleSize = 0.0;
		
		for(BlobCandidateInstance instance: pCandidates) {
			boolean isBlob = OracleFinder.getOracle(instance);
			
			if(isBlob)
				oracleSize++;
			
			if( (instance.isDecorOutput() == true) && (isBlob) )  
				decorTruePositive++;
			else if( (instance.isDecorOutput() == true) && (isBlob == false) )
				decorFalsePositive++;
			
			boolean tacoOutput = false;
			
			if(instance.getSmellynessProbability() >= pTacoThreshold)
				tacoOutput = true;
			else tacoOutput = false;
			
			
			if( (tacoOutput == true) && (isBlob) )  
				tacoTruePositive++;
			else if( (tacoOutput == true) && (isBlob == false) )
				tacoFalsePositive++;	
		}
		
		double precision = tacoTruePositive / (tacoTruePositive+tacoFalsePositive);
		SingleEvaluation tacoPrecision = new SingleEvaluation("precision", "TACO", precision);
		tacoPrecision.setTruePositive(tacoTruePositive);
		tacoPrecision.setFalsePositive(tacoFalsePositive);
		tacoPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(tacoPrecision);
		
		double recall = tacoTruePositive / (oracleSize+2);
		SingleEvaluation tacoRecall = new SingleEvaluation("recall", "TACO", recall);
		evaluations.add(tacoRecall);
		
		double fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation tacoFMeasure = new SingleEvaluation("f-measure", "TACO", fMeasure);
		evaluations.add(tacoFMeasure);
		
		precision = decorTruePositive / (decorTruePositive+decorFalsePositive);
		SingleEvaluation decorPrecision = new SingleEvaluation("precision", "DECOR", precision);
		decorPrecision.setTruePositive(decorTruePositive);
		decorPrecision.setFalsePositive(decorFalsePositive);
		decorPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(decorPrecision);
		
		recall = decorTruePositive / (oracleSize+2);
		SingleEvaluation decorRecall = new SingleEvaluation("recall", "DECOR", recall);
		evaluations.add(decorRecall);
		
		fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation decorFMeasure = new SingleEvaluation("f-measure", "DECOR", fMeasure);
		evaluations.add(decorFMeasure);
		
		return evaluations;
	}
	
	public ArrayList<SingleEvaluation> getLongMethodDetectionAccuracy(ArrayList<LongMethodCandidateInstance> pCandidates, double pTacoThreshold) {
		ArrayList<SingleEvaluation> evaluations = new ArrayList<SingleEvaluation>();
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;
		
		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		double oracleSize = 0.0;
		
		for(LongMethodCandidateInstance instance: pCandidates) {
			boolean isLongMethod = OracleFinder.getOracle(instance);
			
			if(isLongMethod)
				oracleSize++;
			
			if( (instance.isDecorOutput() == true) && (isLongMethod) )  
				decorTruePositive++;
			else if( (instance.isDecorOutput() == true) && (isLongMethod == false) )
				decorFalsePositive++;
			
			boolean tacoOutput = false;
			
			if(instance.getSmellynessProbability() >= pTacoThreshold)
				tacoOutput = true;
			else tacoOutput = false;
			
			
			if( (tacoOutput == true) && (isLongMethod) )  
				tacoTruePositive++;
			else if( (tacoOutput == true) && (isLongMethod == false) )
				tacoFalsePositive++;	
		}
		
		double precision = tacoTruePositive / (tacoTruePositive+tacoFalsePositive);
		SingleEvaluation tacoPrecision = new SingleEvaluation("precision", "TACO", precision);
		tacoPrecision.setTruePositive(tacoTruePositive);
		tacoPrecision.setFalsePositive(tacoFalsePositive);
		tacoPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(tacoPrecision);
		
		double recall = tacoTruePositive / (oracleSize+2);
		SingleEvaluation tacoRecall = new SingleEvaluation("recall", "TACO", recall);
		evaluations.add(tacoRecall);
		
		double fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation tacoFMeasure = new SingleEvaluation("f-measure", "TACO", fMeasure);
		evaluations.add(tacoFMeasure);
		
		precision = decorTruePositive / (decorTruePositive+decorFalsePositive);
		SingleEvaluation decorPrecision = new SingleEvaluation("precision", "DECOR", precision);
		decorPrecision.setTruePositive(decorTruePositive);
		decorPrecision.setFalsePositive(decorFalsePositive);
		decorPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(decorPrecision);
		
		recall = decorTruePositive / oracleSize;
		SingleEvaluation decorRecall = new SingleEvaluation("recall", "DECOR", recall);
		evaluations.add(decorRecall);
		
		fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation decorFMeasure = new SingleEvaluation("f-measure", "DECOR", fMeasure);
		evaluations.add(decorFMeasure);
		
		return evaluations;
	}
	
	public ArrayList<SingleEvaluation> getPromiscuousPackageDetectionAccuracy(ArrayList<PromiscuousPackageCandidateInstance> pCandidates, double pTacoThreshold) {
		ArrayList<SingleEvaluation> evaluations = new ArrayList<SingleEvaluation>();
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;
		
		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		double oracleSize = 0.0;
		
		for(PromiscuousPackageCandidateInstance instance: pCandidates) {
			boolean isPromiscuousPackage = OracleFinder.getOracle(instance);
			
			if(isPromiscuousPackage)
				oracleSize++;
			
			if( (instance.isDecorOutput() == true) && (isPromiscuousPackage) )  
				decorTruePositive++;
			else if( (instance.isDecorOutput() == true) && (isPromiscuousPackage == false) )
				decorFalsePositive++;
			
			boolean tacoOutput = false;
			
			if(instance.getSmellynessProbability() >= pTacoThreshold)
				tacoOutput = true;
			else tacoOutput = false;
			
			
			if( (tacoOutput == true) && (isPromiscuousPackage) )  
				tacoTruePositive++;
			else if( (tacoOutput == true) && (isPromiscuousPackage == false) )
				tacoFalsePositive++;		
		}
		
		double precision = tacoTruePositive / (tacoTruePositive+tacoFalsePositive);
		SingleEvaluation tacoPrecision = new SingleEvaluation("precision", "TACO", precision);
		tacoPrecision.setTruePositive(tacoTruePositive);
		tacoPrecision.setFalsePositive(tacoFalsePositive);
		tacoPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(tacoPrecision);
		
		double recall = tacoTruePositive / oracleSize;
		SingleEvaluation tacoRecall = new SingleEvaluation("recall", "TACO", recall);
		
		evaluations.add(tacoRecall);
		
		double fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation tacoFMeasure = new SingleEvaluation("f-measure", "TACO", fMeasure);
		evaluations.add(tacoFMeasure);
		
		precision = decorTruePositive / (decorTruePositive+decorFalsePositive);
		SingleEvaluation decorPrecision = new SingleEvaluation("precision", "DECOR", precision);
		decorPrecision.setTruePositive(decorTruePositive);
		decorPrecision.setFalsePositive(decorFalsePositive);
		decorPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(decorPrecision);
		
		recall = decorTruePositive / oracleSize;
		SingleEvaluation decorRecall = new SingleEvaluation("recall", "DECOR", recall);
		evaluations.add(decorRecall);
		
		fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation decorFMeasure = new SingleEvaluation("f-measure", "DECOR", fMeasure);
		evaluations.add(decorFMeasure);
		
		return evaluations;
	}
	
	public ArrayList<SingleEvaluation> getEagerTestDetectionAccuracy(ArrayList<EagerTestCandidateInstance> pCandidates, double pTacoThreshold) {
		ArrayList<SingleEvaluation> evaluations = new ArrayList<SingleEvaluation>();
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;
		
		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		double oracleSize = 0.0;
		
		for(EagerTestCandidateInstance instance: pCandidates) {
			boolean isEagerTest = OracleFinder.getOracle(instance);
			
			if(isEagerTest)
				oracleSize++;
			
			if( (instance.isStructuralOutput() == true) && (isEagerTest) )  
				decorTruePositive++;
			else if( (instance.isStructuralOutput() == true) && (isEagerTest == false) )
				decorFalsePositive++;
			
			boolean tacoOutput = false;
			
			if(instance.getSmellynessProbability() >= pTacoThreshold)
				tacoOutput = true;
			else tacoOutput = false;
			
			
			if( (tacoOutput == true) && (isEagerTest) )  
				tacoTruePositive++;
			else if( (tacoOutput == true) && (isEagerTest == false) )
				tacoFalsePositive++;	
		}
		
		double precision = tacoTruePositive / (tacoTruePositive+tacoFalsePositive);
		SingleEvaluation tacoPrecision = new SingleEvaluation("precision", "TACO", precision);
		tacoPrecision.setTruePositive(tacoTruePositive);
		tacoPrecision.setFalsePositive(tacoFalsePositive);
		tacoPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(tacoPrecision);
		
		double recall = tacoTruePositive / oracleSize;
		SingleEvaluation tacoRecall = new SingleEvaluation("recall", "TACO", recall);
		evaluations.add(tacoRecall);
		
		double fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation tacoFMeasure = new SingleEvaluation("f-measure", "TACO", fMeasure);
		evaluations.add(tacoFMeasure);
		
		precision = decorTruePositive / (decorTruePositive+decorFalsePositive);
		SingleEvaluation decorPrecision = new SingleEvaluation("precision", "DECOR", precision);
		decorPrecision.setTruePositive(decorTruePositive);
		decorPrecision.setFalsePositive(decorFalsePositive);
		decorPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(decorPrecision);
		
		recall = decorTruePositive / oracleSize;
		SingleEvaluation decorRecall = new SingleEvaluation("recall", "DECOR", recall);
		evaluations.add(decorRecall);
		
		fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation decorFMeasure = new SingleEvaluation("f-measure", "DECOR", fMeasure);
		evaluations.add(decorFMeasure);
		
		return evaluations;
	}
	
	public ArrayList<SingleEvaluation> getGeneralTextureDetectionAccuracy(ArrayList<GeneralTextureCandidateInstance> pCandidates) {
		ArrayList<SingleEvaluation> evaluations = new ArrayList<SingleEvaluation>();
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;
		
		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		double oracleSize = 0.0;
		
		for(GeneralTextureCandidateInstance instance: pCandidates) {
			boolean isGeneralTexture = OracleFinder.getOracle(instance);
			
			if(isGeneralTexture)
				oracleSize++;
			
			if( (instance.isStructuralOutput() == true) && (isGeneralTexture) )  
				decorTruePositive++;
			else if( (instance.isStructuralOutput() == true) && (isGeneralTexture == false) )
				decorFalsePositive++;
			
			boolean tacoOutput = false;
			
			if( (instance.isTacoOutput() == true) && (isGeneralTexture) )
				tacoOutput = true;
			else tacoOutput = false;
			
			
			if( (tacoOutput == true) && (isGeneralTexture) )  
				tacoTruePositive++;
			else if( (tacoOutput == true) && (isGeneralTexture == false) )
				tacoFalsePositive++;	
		}
		
		double precision = tacoTruePositive / (tacoTruePositive+tacoFalsePositive);
		SingleEvaluation tacoPrecision = new SingleEvaluation("precision", "TACO", precision);
		tacoPrecision.setTruePositive(tacoTruePositive);
		tacoPrecision.setFalsePositive(tacoFalsePositive);
		tacoPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(tacoPrecision);
		
		double recall = tacoTruePositive / oracleSize;
		SingleEvaluation tacoRecall = new SingleEvaluation("recall", "TACO", recall);
		evaluations.add(tacoRecall);
		
		double fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation tacoFMeasure = new SingleEvaluation("f-measure", "TACO", fMeasure);
		evaluations.add(tacoFMeasure);
		
		precision = decorTruePositive / (decorTruePositive+decorFalsePositive);
		SingleEvaluation decorPrecision = new SingleEvaluation("precision", "DECOR", precision);
		decorPrecision.setTruePositive(decorTruePositive);
		decorPrecision.setFalsePositive(decorFalsePositive);
		decorPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(decorPrecision);
		
		recall = decorTruePositive / oracleSize;
		SingleEvaluation decorRecall = new SingleEvaluation("recall", "DECOR", recall);
		evaluations.add(decorRecall);
		
		fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation decorFMeasure = new SingleEvaluation("f-measure", "DECOR", fMeasure);
		evaluations.add(decorFMeasure);
		
		return evaluations;
	}
	
	public ArrayList<SingleEvaluation> getFeatureEnvyDetectionAccuracy(ArrayList<FeatureEnvyCandidateInstance> pCandidates, double pTacoThreshold) {
		ArrayList<SingleEvaluation> evaluations = new ArrayList<SingleEvaluation>();
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;
		
		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		double oracleSize = 0.0;
		
		for(FeatureEnvyCandidateInstance instance: pCandidates) {
			boolean isFeatureEnvy = OracleFinder.getOracle(instance);
			
			if(isFeatureEnvy)
				oracleSize++;
			
			if( (! instance.getDecorEnviedClassName().equals(instance.getClassName())) )  
				decorTruePositive++;
			else if(instance.getDecorEnviedClassName().equals(instance.getClassName()) && isFeatureEnvy)  
				decorFalsePositive++;
			
			if( (! instance.getTacoEnviedClassName().equals(instance.getClassName())) )  
				tacoTruePositive++;
			else if(instance.getTacoEnviedClassName().equals(instance.getClassName()) && isFeatureEnvy)  
				tacoFalsePositive++;
		}
		
		double precision = tacoTruePositive / (tacoTruePositive+tacoFalsePositive);
		SingleEvaluation tacoPrecision = new SingleEvaluation("precision", "TACO", precision);
		tacoPrecision.setTruePositive(tacoTruePositive);
		tacoPrecision.setFalsePositive(tacoFalsePositive);
		tacoPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(tacoPrecision);
		
		double recall = tacoTruePositive / oracleSize;
		SingleEvaluation tacoRecall = new SingleEvaluation("recall", "TACO", recall);
		evaluations.add(tacoRecall);
		
		double fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation tacoFMeasure = new SingleEvaluation("f-measure", "TACO", fMeasure);
		evaluations.add(tacoFMeasure);
		
		precision = decorTruePositive / (decorTruePositive+decorFalsePositive);
		SingleEvaluation decorPrecision = new SingleEvaluation("precision", "DECOR", precision);
		decorPrecision.setTruePositive(decorTruePositive);
		decorPrecision.setFalsePositive(decorFalsePositive);
		decorPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(decorPrecision);
		
		recall = decorTruePositive / oracleSize;
		SingleEvaluation decorRecall = new SingleEvaluation("recall", "DECOR", recall);
		evaluations.add(decorRecall);
		
		fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation decorFMeasure = new SingleEvaluation("f-measure", "DECOR", fMeasure);
		evaluations.add(decorFMeasure);
		
		return evaluations;
	}
	
	public ArrayList<SingleEvaluation> getMisplacedClassDetectionAccuracy(ArrayList<MisplacedClassCandidateInstance> pCandidates, double pTacoThreshold) {
		ArrayList<SingleEvaluation> evaluations = new ArrayList<SingleEvaluation>();
		
		double tacoTruePositive = 0.0;
		double tacoFalsePositive = 0.0;
		
		double decorTruePositive = 0.0;
		double decorFalsePositive = 0.0;
		double oracleSize = 0.0;
		
		for(MisplacedClassCandidateInstance instance: pCandidates) {
			boolean isFeatureEnvy = OracleFinder.getOracle(instance);
			
			if(isFeatureEnvy)
				oracleSize++;
			
			if( (! instance.getDecorEnviedPackageName().equals(instance.getClassName())) )  
				decorTruePositive++;
			else if(instance.getDecorEnviedPackageName().equals(instance.getClassName()) && isFeatureEnvy)  
				decorFalsePositive++;
			
			if( (! instance.getTacoEnviedPackageName().equals(instance.getClassName())) )  
				tacoTruePositive++;
			else if(instance.getTacoEnviedPackageName().equals(instance.getClassName()) && isFeatureEnvy)  
				tacoFalsePositive++;
		}
		
		double precision = tacoTruePositive / (tacoTruePositive+tacoFalsePositive);
		SingleEvaluation tacoPrecision = new SingleEvaluation("precision", "TACO", precision);
		tacoPrecision.setTruePositive(tacoTruePositive);
		tacoPrecision.setFalsePositive(tacoFalsePositive);
		tacoPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(tacoPrecision);
		
		double recall = tacoTruePositive / oracleSize;
		SingleEvaluation tacoRecall = new SingleEvaluation("recall", "TACO", recall);
		evaluations.add(tacoRecall);
		
		double fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation tacoFMeasure = new SingleEvaluation("f-measure", "TACO", fMeasure);
		evaluations.add(tacoFMeasure);
		
		precision = decorTruePositive / (decorTruePositive+decorFalsePositive);
		SingleEvaluation decorPrecision = new SingleEvaluation("precision", "DECOR", precision);
		decorPrecision.setTruePositive(decorTruePositive);
		decorPrecision.setFalsePositive(decorFalsePositive);
		decorPrecision.setOracleSize(oracleSize+2.0);
		evaluations.add(decorPrecision);
		
		recall = decorTruePositive / oracleSize;
		SingleEvaluation decorRecall = new SingleEvaluation("recall", "DECOR", recall);
		evaluations.add(decorRecall);
		
		fMeasure = 2 * ((precision*recall) / (precision+recall));
		SingleEvaluation decorFMeasure = new SingleEvaluation("f-measure", "DECOR", fMeasure);
		evaluations.add(decorFMeasure);
		
		return evaluations;
	}
}