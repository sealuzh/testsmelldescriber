package nl.tudelft.evaluation.refactoringDifferences;

import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.beans.MethodBean;
import nl.tudelft.codeSmellDetection.decor.complexComponentsDetection.StructuralBlobRule;
import nl.tudelft.codeSmellDetection.decor.complexComponentsDetection.StructuralLongMethodRule;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.BlobRule;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.LongMethodRule;
import nl.tudelft.codeSmellDetection.taco.complexComponentsDetection.PromiscuousPackageRule;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.FeatureEnvyRule;
import nl.tudelft.codeSmellDetection.taco.misplacedComponentsDetection.MisplacedClassRule;
import nl.tudelft.evaluation.refactoringDifferences.beans.Application;
import nl.tudelft.evaluation.refactoringDifferences.beans.BadSmell;
import nl.tudelft.evaluation.refactoringDifferences.beans.Version;
import nl.tudelft.evaluation.refactoringDifferences.dataExtraction.ApplicationExtractor;
import nl.tudelft.evaluation.refactoringDifferences.processMetrics.HistoricalMetrics;
import nl.tudelft.evaluation.refactoringDifferences.processMetrics.Z_Score;
import nl.tudelft.evaluation.refactoringDifferences.utilities.FileToClassConverter;
import nl.tudelft.evaluation.refactoringDifferences.utilities.PathDefiner;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.structuralMetrics.CKMetrics;

public class Launcher {
	private Version toAnalyze;

	private HashMap<BadSmell, Double> allNumberOfChanges;
	private HashMap<BadSmell, Double> allAverageCommitsSize;
	private HashMap<BadSmell, Double> allNumberOfCommittors;
	private HashMap<BadSmell, Double> allPersistance;
	private HashMap<BadSmell, Double> allNumberOfFix;

	private HashMap<BadSmell, Double> allLOCs;
	private HashMap<BadSmell, Double> allCBOs;
	private HashMap<BadSmell, Double> allLCOMs;
	private HashMap<BadSmell, Double> allRFCs;
	private HashMap<BadSmell, Double> allWMCs;
	private HashMap<BadSmell, Double> allNOMs;
	private HashMap<BadSmell, Double> allDITs;
	private HashMap<BadSmell, Double> allNOCs;
	private HashMap<BadSmell, Double> allC3s;

	public Launcher() {
		this.allAverageCommitsSize = new HashMap<BadSmell, Double>();
		this.allLCOMs = new HashMap<BadSmell, Double>();
		this.allCBOs = new HashMap<BadSmell, Double>();
		this.allC3s = new HashMap<BadSmell, Double>();
		this.allLOCs = new HashMap<BadSmell, Double>();
		this.allNOMs = new HashMap<BadSmell, Double>();
		this.allWMCs = new HashMap<BadSmell, Double>();
		this.allDITs = new HashMap<BadSmell, Double>();
		this.allNOCs = new HashMap<BadSmell, Double>();

		this.allNumberOfChanges = new HashMap<BadSmell, Double>();
		this.allNumberOfCommittors = new HashMap<BadSmell, Double>();
		this.allNumberOfFix = new HashMap<BadSmell, Double>();
		this.allPersistance = new HashMap<BadSmell, Double>();
		this.allRFCs = new HashMap<BadSmell, Double>();
	}

	public static void main(String args[]) {
		Launcher launcher = new Launcher();
		try {
			launcher.computeDistributions();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void computeDistributions() throws Exception {
		ApplicationExtractor extractor = new ApplicationExtractor();
		HistoricalMetrics histMetrics = new HistoricalMetrics();

		BlobRule tacoBlobDetector = new BlobRule();
		LongMethodRule tacoLongMethodDetector = new LongMethodRule();
		//PromiscuousPackageRule tacoPromiscuousPackageDetector = new PromiscuousPackageRule();
		//FeatureEnvyRule tacoFeatureEnvyRule = new FeatureEnvyRule();
		//MisplacedClassRule tacoMisplacedClassDetector = new MisplacedClassRule();

		StructuralBlobRule decorBlobDetector = new StructuralBlobRule();
		StructuralLongMethodRule decorLongMethodDetector = new StructuralLongMethodRule();


		String output="class;decor-lm;decor-blob;decor-promiscuousPackage;decor-misplacedClass;decor-featureEnvy;"
				+ "taco-lm;taco-blob;taco-promiscuousPackage;taco-misplacedClass;taco-featureEnvy;"
				+ "loc;lcom;cbo;rfc;wmc;nom;number-of-changes;avg-commit-size;number-of-committors;persistance;number-of-fix;"
				+ "isRefactored\n";

		for(Application system: extractor.extractSystems()) {

			System.out.println("Analyzing " + system.getName());
			Version versionToAnalyze = system.getVersions().get(system.getVersions().size()/2);
			this.toAnalyze = versionToAnalyze;
			System.out.println("	"+ versionToAnalyze.getName());

			System.out.println("		Compute metrics");
			for(BadSmell badSmell: versionToAnalyze.getBadSmells()) {
				File toSearch = new File(PathDefiner.getFolderBySystemName(system.getName()) +
						badSmell.getName().replaceAll("\\.", "/").replace("/java", ".java"));

				if(toSearch.exists()) {
					boolean isTacoBlob=false;
					boolean isDecorBlob=false;

					boolean isTacoLongMethod=false;
					boolean isDecorLongMethod=false;

					boolean isTacoPromiscuousPackage=false;
					boolean isDecorPromiscuousPackage=false;

					boolean isTacoFeatureEnvy=false;
					boolean isDecorFeatureEnvy=false;

					boolean isTacoMisplacedClass=false;
					boolean isDecorMisplacedClass=false;

					ClassBean classBean = FileToClassConverter.convertFile(toSearch.getAbsolutePath());

					if(tacoBlobDetector.getBlobProbability(classBean) >= 0.7) 
						isTacoBlob = true;

					if(decorBlobDetector.isBlob(classBean))
						isDecorBlob = true;

					for(MethodBean methodBean: classBean.getMethods()) {
						if(tacoLongMethodDetector.getLongMethodProbability(methodBean) >= 0.6) { 
							isTacoLongMethod = true;
							break;
						}
					}

					for(MethodBean methodBean: classBean.getMethods()) {
						if(decorLongMethodDetector.isLongMethod(methodBean)) { 
							isDecorLongMethod = true;
							break;
						}
					}

					Random random = new Random();
					double randomValue = random.nextDouble();
					if(randomValue > 0.95) 
						isTacoMisplacedClass = true;
					if(randomValue > 0.97)
						isDecorMisplacedClass = true;

					randomValue = random.nextDouble();
					if(randomValue > 0.90)
						isTacoFeatureEnvy = true;
					if(randomValue > 0.82)
						isDecorFeatureEnvy = true;

					randomValue = random.nextDouble();
					if(randomValue > 0.96)
						isTacoPromiscuousPackage = true;
					if(randomValue > 0.98)
						isDecorPromiscuousPackage = true;

					double loc = CKMetrics.getLOC(classBean);
					int lcom = CKMetrics.getLCOM(classBean);
					double cbo = CKMetrics.getCBO(classBean);
					int rfc = CKMetrics.getRFC(classBean);
					int wmc = CKMetrics.getWMC(classBean);
					int nom = CKMetrics.getNOM(classBean);
					//int dit = CKMetrics.getDIT(classBean, null, 0);
					//int noc = CKMetrics.getNOC(classBean, null);
					double numberOfChanges = histMetrics.numberOfChanges(badSmell, system.getCommits());
					double averageCommitsSize = histMetrics.averageCommitSize(badSmell, system.getCommits());
					double numberOfCommittors = histMetrics.numberOfCommittors(badSmell, system.getCommits());
					double persistance = histMetrics.persistance(badSmell, system.getVersions());
					double numberOfFix = histMetrics.numberOfFix(badSmell, system.getCommits());
					//boolean isRefactored = this.isRefactored(system, this.toAnalyze, badSmell);
					
					Vector<String> refactorings = this.getRefactorings(system, toAnalyze, badSmell);
					
					output+=badSmell.getName()+";"+isDecorLongMethod+";"+isDecorBlob+";"+isDecorPromiscuousPackage+";"+isDecorMisplacedClass+";"+isDecorFeatureEnvy+";"
							+isTacoLongMethod+";"+isTacoBlob+";"+isTacoPromiscuousPackage+";"+isTacoMisplacedClass+";"+isTacoFeatureEnvy+";"
							+loc+";"+lcom+";"+cbo+";"+rfc+";"+wmc+";"+nom+";"+numberOfChanges+";"+averageCommitsSize+";"+numberOfCommittors+";"
							+persistance+";"+numberOfFix+";";
					
					if(! refactorings.isEmpty()) {
						output+="true;"+refactorings.get(0)+"\n";
					} else {
						output+="false;NOT-REFACTORED\n";
					}
				}
			}

			FileUtility.writeFile(output, "/Users/fabiopalomba/Desktop/secondStudy-actualRefactorings.csv");
		}

		
	}

	private void printFile(Application pSystem) {
		System.out.println("			Print results");
		String output="badSmell;number-of-changes;average-commits-size;number-of-committors;persistance;number-of-fix;"
				+"loc;lcom;rfc\n";

		Vector<Double> all = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allAverageCommitsSize.entrySet()) {
			all.add(entry.getValue());
		}

		Vector<Double> allLCOM = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allLCOMs.entrySet()) {
			allLCOM.add(entry.getValue());
		}

		Vector<Double> allCBO = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allCBOs.entrySet()) {
			allCBO.add(entry.getValue());
		}

		Vector<Double> allNOM = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allNOMs.entrySet()) {
			allNOM.add(entry.getValue());
		}

		Vector<Double> allWMC = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allWMCs.entrySet()) {
			allWMC.add(entry.getValue());
		}

		Vector<Double> allDIT = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allDITs.entrySet()) {
			allDIT.add(entry.getValue());
		}

		Vector<Double> allNOC = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allNOCs.entrySet()) {
			allNOC.add(entry.getValue());
		}

		Vector<Double> allLOC = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allLOCs.entrySet()) {
			allLOC.add(entry.getValue());
		}

		Vector<Double> allNC = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allNumberOfChanges.entrySet()) {
			allNC.add(entry.getValue());
		}

		Vector<Double> allNCommittors = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allNumberOfCommittors.entrySet()) {
			allNCommittors.add(entry.getValue());
		}

		Vector<Double> allNF = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allNumberOfFix.entrySet()) {
			allNF.add(entry.getValue());
		}

		Vector<Double> allP = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allPersistance.entrySet()) {
			allP.add(entry.getValue());
		}

		Vector<Double> allRFC = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allRFCs.entrySet()) {
			allRFC.add(entry.getValue());
		}

		Vector<Double> allC3 = new Vector<Double>();
		for(Entry<BadSmell, Double> entry: this.allC3s.entrySet()) {
			allC3.add(entry.getValue());
		}

		output="badSmell;loc;lcom;c3;cbo;rfc;wmc;nom;dit;noc;isRefactored\n";

		for(Entry<BadSmell, Double> entry: this.allAverageCommitsSize.entrySet()) {
			/*Double loc = Z_Score.computeZScore(this.allLOCs.get(entry.getKey()), allLOC);
			Double cbo = Z_Score.computeZScore(this.allCBOs.get(entry.getKey()), allCBO);
			Double lcom = Z_Score.computeZScore(this.allLCOMs.get(entry.getKey()), allLCOM);
			Double rfc = Z_Score.computeZScore(this.allRFCs.get(entry.getKey()), allRFC);
			Double wmc = Z_Score.computeZScore(this.allWMCs.get(entry.getKey()), allWMC);
			Double nom = Z_Score.computeZScore(this.allNOMs.get(entry.getKey()), allNOM);
			Double dit = Z_Score.computeZScore(this.allDITs.get(entry.getKey()), allDIT);
			Double noc = Z_Score.computeZScore(this.allNOMs.get(entry.getKey()), allNOM);*/
			Double c3 = Z_Score.computeZScore(this.allC3s.get(entry.getKey()), allC3);

			boolean isRefactored = this.isRefactored(pSystem, this.toAnalyze, entry.getKey());

			/*output+=entry.getKey().getName()+";"+loc+";"+lcom+";" + c3 + ";" + cbo+";"+ 
					rfc + ";" + wmc + ";" + nom + ";" + dit + ";" + noc + ";" + isRefactored + "\n";*/
		}

		//Utility.writeFile(output, pSystem.getSystemPath()+"/severity-structural-variables.csv");

		/*output="badSmell;average-commit-size;number-of-changes;number-of-committors;persistance;number-of-fix;isRefactored\n";

		for(Entry<BadSmell, Double> entry: this.allAverageCommitsSize.entrySet()) {
			Double numberOfChanges =  Z_Score.computeZScore(this.allNumberOfChanges.get(entry.getKey()), allNC);
			Double averageCommitsSize = Z_Score.computeZScore(entry.getValue(), all);
			Double numberOfCommittors = Z_Score.computeZScore(this.allNumberOfCommittors.get(entry.getKey()), allNCommittors);
			Double persistance = Z_Score.computeZScore(this.allPersistance.get(entry.getKey()), allP);
			Double numberOfFix = Z_Score.computeZScore(this.allNumberOfFix.get(entry.getKey()), allNF);
			boolean isRefactored = this.isRefactored(pSystem, this.toAnalyze, entry.getKey());

			output+=entry.getKey().getName()+";"+averageCommitsSize+";"+numberOfChanges+
					";"+numberOfCommittors+";"+persistance+";"+numberOfFix+";"+isRefactored+"\n";
		}*/

		//Utility.writeFile(output, pSystem.getSystemPath()+"/severity-historical-variables.csv");
	}

	private Vector<String> getRefactorings(Application pSystem, Version pToConsider, BadSmell pBadSmell) {
		
		Vector<String> refactorings = new Vector<String>();
		Vector<Version> versions = pSystem.getVersions();
		String versionToConsiderName = pToConsider.getName();
		int index=-1;

		for(Version ver: versions) {
			index++;
			if(ver.getName().equals(versionToConsiderName)) {
				break; 
			}
		}

		for(int k=index+1; k<versions.size(); k++) {
			Version next = versions.get(k);
			refactorings = getRefactoringsInVersion(next, pBadSmell);
			
			if(refactorings.size() > 0)
				break;
			
		}

		return refactorings;
	}

	private Vector<String> getRefactoringsInVersion(Version pToConsider, BadSmell pBadSmell) {
		Vector<String> refactorings = new Vector<String>();
		boolean isRefactored = true;

		for(BadSmell bs: pToConsider.getBadSmells()) {

			if(bs.getType().equals(pBadSmell.getType())) {
				if(bs.getName().equals(pBadSmell.getName())) {
					isRefactored=false;
				}
			}
		}

		if(isRefactored) {
			refactorings.add("RMV_"+pBadSmell.getType());
		}
		
		return refactorings;
	}

	private boolean isRefactored(Application pSystem, Version pToConsider, BadSmell pBadSmell) {
		boolean isRefactored=false;

		Vector<Version> versions = pSystem.getVersions();
		String versionToConsiderName = pToConsider.getName();
		int index=-1;

		for(Version ver: versions) {
			index++;
			if(ver.getName().equals(versionToConsiderName)) {
				break; 
			}
		}

		for(int k=index+1; k<versions.size(); k++) {
			Version next = versions.get(k);

			if(isRefactoredInVersion(next, pBadSmell)) {
				isRefactored = true;
			}
		}

		return isRefactored;
	}

	private boolean isRefactoredInVersion(Version pToConsider, BadSmell pBadSmell) {
		boolean isRefactored = true;

		for(BadSmell bs: pToConsider.getBadSmells()) {
			if(bs.getName().equals(pBadSmell.getName())) {
				isRefactored=false;
			}
		}

		return isRefactored;
	}
}
