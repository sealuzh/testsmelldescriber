package nl.tudelft.evaluation.refactoringDifferences.processMetrics;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import nl.tudelft.evaluation.refactoringDifferences.beans.BadSmell;
import nl.tudelft.evaluation.refactoringDifferences.beans.Commit;
import nl.tudelft.evaluation.refactoringDifferences.beans.Version;


public class HistoricalMetrics {

	public double numberOfChanges(BadSmell pBadSmell, Vector<Commit> pHistory) {
		double changes=0.0;

		long start = pHistory.get(0).getCommitDate().getTime();
		long end = pHistory.get(pHistory.size()-1).getCommitDate().getTime();

		Date middle = new Date(start + (end - start)/2);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(middle);
		calendar.add(Calendar.DAY_OF_YEAR, -360);

		Date previousDate = calendar.getTime();

		for(Commit commit: pHistory) {
			if(commit.getCommitDate().after(previousDate) && commit.getCommitDate().before(middle)) {
				for(String resource: commit.getCommittedResources()) {
					if(resource.contains(pBadSmell.getName().replaceAll("\\.", "/").replace("/java", ".java"))) 
						changes++;
				}
			}
		}

		return changes;
	}

	public double numberOfCommittors(BadSmell pBadSmell, Vector<Commit> pHistory) {
		Vector<String> committors = new Vector<String>();

		long start = pHistory.get(0).getCommitDate().getTime();
		long end = pHistory.get(pHistory.size()-1).getCommitDate().getTime();

		Date middle = new Date(start + (end - start)/2);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(middle);
		calendar.add(Calendar.DAY_OF_YEAR, -360);

		Date previousDate = calendar.getTime();

		for(Commit commit: pHistory) {
			if(commit.getCommitDate().after(previousDate) && commit.getCommitDate().before(middle)) {
				for(String resource: commit.getCommittedResources()) {
					if(resource.contains(pBadSmell.getName().replaceAll("\\.", "/").replace("/java", ".java"))) {
						if(! committors.contains(commit.getCommitter())) 
							committors.add(commit.getCommitter());
					}
				}
			}
		}

		return committors.size();
	}

	public double persistance(BadSmell pBadSmell, Vector<Version> pVersions) {
		int persistance=0;

		for(Version version: pVersions) {
			for(BadSmell bd: version.getBadSmells()) {
				if(bd.getName().equals(pBadSmell.getName()))
					persistance++;
			}
		}

		return persistance;
	}

	public double averageCommitSize(BadSmell pBadSmell, Vector<Commit> pHistory) {
		int commitSize=0;
		int commitsInvolvingBadSmell=0;

		long start = pHistory.get(0).getCommitDate().getTime();
		long end = pHistory.get(pHistory.size()-1).getCommitDate().getTime();

		Date middle = new Date(start + (end - start)/2);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(middle);
		calendar.add(Calendar.DAY_OF_YEAR, -360);

		Date previousDate = calendar.getTime();

		for(Commit commit: pHistory) {
			if(commit.getCommitDate().after(previousDate) && commit.getCommitDate().before(middle)) { 
				for(String resource: commit.getCommittedResources()) {
					if(resource.contains(pBadSmell.getName().replaceAll("\\.", "/").replace("/java", ".java"))) {
						commitSize+=commit.getCommittedResources().size();
						commitsInvolvingBadSmell++;
					}
				}
			}
		}

		if(commitsInvolvingBadSmell > 0)
			return (commitSize/commitsInvolvingBadSmell);

		else return 0.0;
	}

	public double numberOfFix(BadSmell pBadSmell, Vector<Commit> pHistory) {
		double numberOfFix=0.0;

		long start = pHistory.get(0).getCommitDate().getTime();
		long end = pHistory.get(pHistory.size()-1).getCommitDate().getTime();

		Date middle = new Date(start + (end - start)/2);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(middle);
		calendar.add(Calendar.DAY_OF_YEAR, -360);

		Date previousDate = calendar.getTime();

		for(Commit commit: pHistory) {
			if(commit.getCommitDate().after(previousDate) && commit.getCommitDate().before(middle)) {
				for(String resource: commit.getCommittedResources()) {
					if(resource.contains(pBadSmell.getName().replaceAll("\\.", "/").replace("/java", ".java"))) {
						if( (commit.getCommitMessage().toLowerCase().contains("fix")) || (commit.getCommitMessage().toLowerCase().contains("bug")) 
								|| (commit.getCommitMessage().toLowerCase().contains("resolved")) 
								|| (commit.getCommitMessage().toLowerCase().contains("solved"))
								|| (commit.getCommitMessage().toLowerCase().contains("resolution"))) {
							numberOfFix++;
						}

					}
				}
			}
		}

		return numberOfFix;
	}
}
