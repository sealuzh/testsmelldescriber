package nl.tudelft.evaluation.refactoringDifferences.processMetrics;

import java.text.DecimalFormat;
import java.util.Vector;

public class Z_Score {
	private static Double mean;
	@SuppressWarnings("unused")
	private static Double variance;

	public static Double computeZScore(Double pValue, Vector<Double> pValues) {
		Double zScore = new Double(0.0);
		
		if(pValue.isNaN()) {
			zScore = 0.0;
		} else zScore = (pValue.doubleValue() - computeMean(pValues)) / computeStdDev(pValues) ;

		DecimalFormat df = new DecimalFormat("#.##");
		String format = df.format(zScore).replace(",", ".");

		Double result = null;

		try{
			if(format.equals("?"))
				result=0.0;
			else result = Double.parseDouble(format);
		} catch(java.lang.NumberFormatException nfe) {
			result = 0.0;
		}

		return result;
	}

	public static Double computeMean(Vector<Double> pValues) {
		Double sum = 0.0;

		for(Double value : pValues) {
			if(value.isNaN()) {
				sum += 0.0;
			} else sum += value;
		}

		Z_Score.mean = sum/pValues.size(); 

		return sum/pValues.size();
	}

	public static Double computeVariance(Vector<Double> pValues) {
		double mean = Z_Score.mean;

		double temp = 0;

		for(Double value :pValues) {
			if(value.isNaN()) {
				temp += 0.0;
			} else temp += (mean-value)*(mean-value);
		}

		Z_Score.variance = temp/pValues.size();

		return temp/pValues.size();
	}

	public static Double computeStdDev(Vector<Double> pValues) {
		return Math.sqrt(computeVariance(pValues));
	}
}
