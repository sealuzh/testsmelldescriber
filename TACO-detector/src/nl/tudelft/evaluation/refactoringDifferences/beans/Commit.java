package nl.tudelft.evaluation.refactoringDifferences.beans;

import java.util.Date;
import java.util.Vector;

public class Commit {
	private String ID_COMMIT;
	private String ID;
	private Vector<String> committedResources;
	private String commitMessage;
	private String committer;
	private Date commitDate;
	
	public Commit() {
		this.committedResources=new Vector<String>();
	}
	
	public String getID_COMMIT() {
		return ID_COMMIT;
	}
	public void setID_COMMIT(String iD_COMMIT) {
		ID_COMMIT = iD_COMMIT;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public Vector<String> getCommittedResources() {
		return committedResources;
	}
	public void addResource(String pResource) {
		this.committedResources.add(pResource);
	}
	public void setCommittedResources(Vector<String> committedResources) {
		this.committedResources = committedResources;
	}
	public String getCommitMessage() {
		return commitMessage;
	}
	public void setCommitMessage(String commitMessage) {
		this.commitMessage = commitMessage;
	}
	public String getCommitter() {
		return committer;
	}
	public void setCommitter(String committer) {
		this.committer = committer;
	}
	public Date getCommitDate() {
		return commitDate;
	}
	public void setCommitDate(Date commitDate) {
		this.commitDate = commitDate;
	}	
}
