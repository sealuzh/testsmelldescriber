package nl.tudelft.evaluation.refactoringDifferences.beans;

import java.util.Vector;

public class Version {
	private String name;
	private String systemPath;
	private String versionPath;
	private Vector<BadSmell> badSmells;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vector<BadSmell> getBadSmells() {
		return badSmells;
	}
	public void setBadSmells(Vector<BadSmell> badSmells) {
		this.badSmells = badSmells;
	}
	public String getSystemPath() {
		return systemPath;
	}
	public void setSystemPath(String systemPath) {
		this.systemPath = systemPath;
	}
	public String getVersionPath() {
		return versionPath;
	}
	public void setVersionPath(String versionPath) {
		this.versionPath = versionPath;
	}
	
	
}
