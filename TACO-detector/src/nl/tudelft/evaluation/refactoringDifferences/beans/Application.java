package nl.tudelft.evaluation.refactoringDifferences.beans;

import java.util.Vector;

public class Application {
	private String name;
	private String systemPath;
	private Vector<Version> versions;
	private Vector<Commit> commits;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vector<Version> getVersions() {
		return versions;
	}
	public void setVersions(Vector<Version> versions) {
		this.versions = versions;
	}
	public String getSystemPath() {
		return systemPath;
	}
	public void setSystemPath(String systemPath) {
		this.systemPath = systemPath;
	}
	public Vector<Commit> getCommits() {
		return commits;
	}
	public void setCommits(Vector<Commit> commits) {
		this.commits = commits;
	}
}
