package nl.tudelft.evaluation.refactoringDifferences.utilities;

public class PathDefiner {
	public static String root="/Users/fabiopalomba/Dropbox/Antipattern Detection/";

	public static String apacheAntRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/ApacheAnt/apache_1.6.1/src/main/";
	public static String apacheCassandraRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Cassandra/apache-cassandra-0.7.2/src/java/";
	public static String apacheHiveRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Hive/itisaid-hive-0.6/serde/src/java/";
	public static String apacheIncubatingRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Incubating/tatsuyaoiw-manifoldcf-0.6/connectors/livelink/connector/src/main/java/";
	public static String apacheHBaseRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/HBase/optimistdk-hbase-0.8.9/src/main/java/";
	public static String apacheIvyRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Ivy/ivy_2.0.0RC1/src/java/";
	public static String apacheKarafRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Karaf/apache-karaf-2.1/main/src/main/java/";
	public static String xercesRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/xerces/xerces-1.4.0/src/";
	public static String jFreeChartRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/JFreeChart/jfreechart-0.9.2/source/";
	public static String jHotDrawRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/JHotDraw/JHotDraw_7.0.9/src/main/java/";
	public static String jEditRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/JEdit/jedit_4.0.3/src/";
	public static String hsqldbRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/hsqldb/hsqldb_2.2.0/src/";
	public static String eclipseRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/eclipse/org.eclipse.jdt.core_3.2.1/";
	public static String aTunesRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/aTunes/aTunes_1.5.1/src/";
	public static String jvtlRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/jVTL/jvlt-1.1.3/src/";
	public static String jBossRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/jBoss/jboss-3.2.8-src/server/src/main/";
	public static String elasticSearchRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/ElasticSearch/elasticsearch-elasticsearch-0.16/modules/elasticsearch/src/main/java/";
	public static String nutchRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Nutch/danielfagerstrom-nutch-1.1/src/java/";
	public static String pigRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Pig/cloudera-pig-0.5/src/";
	public static String strutsRoot="/Users/fabiopalomba/Documents/PhD/Papers/Utility/Software systems/Apache Struts/apache-struts 2.2.3.1/core/src/main/java/";

	public static String getFolderBySystemName(String pSystemName) {
		
		if(pSystemName.equals("Apache Ant_OK")) {
			return apacheAntRoot;
		} else if(pSystemName.equals("Apache Cassandra_OK")) {
			return apacheCassandraRoot;
		} else if(pSystemName.equals("Apache Hive_OK")) {
			return apacheHiveRoot;
		} else if(pSystemName.equals("Apache Incubating_OK")) {
			return apacheIncubatingRoot;
		} else if(pSystemName.equals("Apache HBase_OK")) {
			return apacheHBaseRoot;
		} else if(pSystemName.equals("Apache Ivy_OK")) {
			return apacheIvyRoot;
		} else if(pSystemName.equals("Apache Karaf_OK")) {
			return apacheKarafRoot;
		} else if(pSystemName.equals("xerces_OK")) {
			return xercesRoot;
		} else if(pSystemName.equals("JFreeChart_OK")) {
			return jFreeChartRoot;
		} else if(pSystemName.equals("JHotDraw_OK")) {
			return jHotDrawRoot;
		} else if(pSystemName.equals("JEdit_OK")) {
			return jEditRoot;
		} else if(pSystemName.equals("hsqldb_OK")) {
			return hsqldbRoot;
		} else if(pSystemName.equals("Eclipse_OK")) {
			return eclipseRoot;
		} else if(pSystemName.equals("aTunes_OK")) {
			return aTunesRoot;
		} else if(pSystemName.equals("jVLT_OK")) {
			return jvtlRoot;
		} else if(pSystemName.equals("jBoss_OK")) {
			return jBossRoot;
		} else if(pSystemName.equals("Elastic Search_OK")) {
			return elasticSearchRoot;
		} else if(pSystemName.equals("Apache Nutch_OK")) {
			return nutchRoot;
		} else if(pSystemName.equals("Apache Pig_OK")) {
			return pigRoot;
		} else if(pSystemName.equals("Apache Struts_OK")) {
			return strutsRoot;
		} 
		
		return "";
	}
}