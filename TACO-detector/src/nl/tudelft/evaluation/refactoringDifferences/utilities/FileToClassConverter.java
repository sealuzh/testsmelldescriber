package nl.tudelft.evaluation.refactoringDifferences.utilities;


import java.io.IOException;
import java.util.Vector;

import nl.tudelft.beans.ClassBean;
import nl.tudelft.fileUtilities.FileUtility;
import nl.tudelft.parser.ClassParser;
import nl.tudelft.parser.CodeParser;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class FileToClassConverter {

	public static ClassBean convertFile(String pPathToClass) {
		ClassBean classBean = new ClassBean();
		CodeParser parser = new CodeParser();
		
		try {
			CompilationUnit compilationUnit = parser.createParser(FileUtility.readFile(pPathToClass));
			Vector<String> imports = new Vector<String>();
			
			for(Object o: compilationUnit.imports()) {
				imports.add(o.toString());
			}
			
			TypeDeclaration typeDeclaration = (TypeDeclaration)compilationUnit.types().get(0);
			classBean = ClassParser.parse(typeDeclaration, "package", imports);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return classBean;
	}	
}
