package nl.tudelft.evaluation.refactoringDifferences.dataExtraction;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Pattern;

import nl.tudelft.evaluation.refactoringDifferences.beans.BadSmell;
import nl.tudelft.evaluation.refactoringDifferences.utilities.PathDefiner;
import nl.tudelft.fileUtilities.FileUtility;

public class BadSmellExtractor {

	public static void main(String args[]) {
		BadSmellExtractor extractor = new BadSmellExtractor();
		String path = PathDefiner.root+"Apache Ant_OK/";
		
		Vector<BadSmell> badSmells = extractor.extractBadSmellsByVersion(path, "apache_1.8.1");
		
		for(BadSmell bd: badSmells) {
			System.out.println("type: " + bd.getType());
			System.out.println("bad smell: " + bd.getName());
			
		}
		
	}
	
	public Vector<BadSmell> extractBadSmellsByVersion(String pSystemPath, String pSystemVersionName) {
		Vector<BadSmell> badSmells = new Vector<BadSmell>();
		File versionDir = new File(pSystemPath+"/"+pSystemVersionName+"/Validated/");
		
		for(File bad: versionDir.listFiles()) {
			
			String type = this.getTypeByFileName(bad);
			Vector<String> classes = this.getNamesByFileName(bad, type);
			
			for(String clazz: classes) {
				BadSmell badSmell = new BadSmell();
				badSmell.setType(type);
				badSmell.setName(clazz);
				badSmells.add(badSmell);
			}
		}
		
		return badSmells;
		
	}
	
	private Vector<String> getNamesByFileName(File pFile, String pType) {
		Vector<String> names = new Vector<String>();
		
		try {
			String content = FileUtility.readFile(pFile.getAbsolutePath());
			Pattern newLine = Pattern.compile("\n");
			Pattern comma = Pattern.compile(";");
			
			if(! content.isEmpty()) {
				String lines[] = newLine.split(content);
				
				if(pType.equals("CDSBP")) {
					for(String line: lines) {
						try {
							String name = comma.split(line)[1] +"." + comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						} catch(ArrayIndexOutOfBoundsException ex) {
							String name = comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						}
					}
				} else if(pType.equals("CC")) {
					for(String line: lines) {
						try {
							String name = comma.split(line)[1] +"." + comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						} catch(ArrayIndexOutOfBoundsException ex) {
							String name = comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						}
					}
				} else if(pType.equals("FE")) {
					for(String line: lines) {
						String name = comma.split(line)[2] +"." + comma.split(line)[1];
						names.add(name.replaceAll(" ", ""));
					}
				} else if(pType.equals("II")) {
					for(String line: lines) {
						String name = comma.split(line)[1] +"." + comma.split(line)[0];
						names.add(name.replaceAll(" ", ""));
					}
				} else if(pType.equals("BLOB")) {
					for(String line: lines) {
						String name = comma.split(line)[1] +"." + comma.split(line)[0];
						names.add(name.replaceAll(" ", ""));
					}
				} else if(pType.equals("LC")) {
					for(String line: lines) {
						try {
							String name = comma.split(line)[1] +"." + comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						} catch(ArrayIndexOutOfBoundsException ex) {
							String name = comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						}
					}
				} else if(pType.equals("LM")) {
					for(String line: lines) {
						String name = comma.split(line)[2] +"." + comma.split(line)[1];
						names.add(name.replaceAll(" ", ""));
					}
				} else if(pType.equals("LPL")) {
					for(String line: lines) {
						String name = comma.split(line)[2] +"." + comma.split(line)[1] +".";
						names.add(name.replaceAll(" ", ""));
					}
				} else if(pType.equals("MC")) {
					for(String line: lines) {
						String name = comma.split(line)[1] +"." + comma.split(line)[0];
						names.add(name.replaceAll(" ", ""));
					}
				} else if(pType.equals("MM")) {
					for(String line: lines) {
						try {
							String name = comma.split(line)[1] +"." + comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						} catch(ArrayIndexOutOfBoundsException ex) {
							String name = comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						}
					}
				} else if(pType.equals("RB")) {
					for(String line: lines) {
						String name = comma.split(line)[1] +"." + comma.split(line)[0];
						names.add(name.replaceAll(" ", ""));
					}
				} else if(pType.equals("SC")) {
					for(String line: lines) {
						try {
							String name = comma.split(line)[1] +"." + comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						} catch(ArrayIndexOutOfBoundsException ex) {
							String name = comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						}
					}
				} else if(pType.equals("SG")) {
					for(String line: lines) {
						try {
							String name = comma.split(line)[1] +"." + comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						} catch(ArrayIndexOutOfBoundsException ex) {
							String name = comma.split(line)[0];
							names.add(name.replaceAll(" ", ""));
						}
					}
				} 
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return names;
	}
	
	private String getTypeByFileName(File pFile) {
		String type="";
		
		if(pFile.getName().equals("candidate_Class_Data_Should_Be_Private.csv"))
			return "CDSBP";
		else if(pFile.getName().equals("candidate_Complex_Class.csv"))
			return "CC";
		else if(pFile.getName().equals("candidate_Feature_Envy.csv"))
			return "FE";
		else if(pFile.getName().equals("candidate_Inappropriate_Intimacy.csv"))
			return "II";
		else if(pFile.getName().equals("candidate_Large_Class.csv"))
			return "BLOB";
		else if(pFile.getName().equals("candidate_Lazy_Class.csv"))
			return "LC";
		else if(pFile.getName().equals("candidate_Long_Methods.csv"))
			return "LM";
		else if(pFile.getName().equals("candidate_Long_Parameter_List.csv"))
			return "LPL";
		else if(pFile.getName().equals("candidate_Message_Chains.csv"))
			return "MC";
		else if(pFile.getName().equals("candidate_Middle_Man.csv"))
			return "MM";
		else if(pFile.getName().equals("candidate_Refused_Bequest.csv"))
			return "RB";
		else if(pFile.getName().equals("candidate_Spaghetti_Code.csv"))
			return "SC";
		else if(pFile.getName().equals("candidate_Speculative_Generality.csv"))
			return "SG";
		
		return type;
	}
}
