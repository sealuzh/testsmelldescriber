package nl.tudelft.evaluation.refactoringDifferences.dataExtraction;


import java.io.File;
import java.util.Vector;

import nl.tudelft.evaluation.refactoringDifferences.beans.BadSmell;
import nl.tudelft.evaluation.refactoringDifferences.beans.Version;
import nl.tudelft.evaluation.refactoringDifferences.utilities.PathDefiner;

public class VersionExtractor {
	private BadSmellExtractor badSmellExtractor;
	
	public static void main(String args[]) {
		VersionExtractor extractor = new VersionExtractor();
		File systemDir = new File(PathDefiner.root+"Apache Ant_OK");
		//System.out.println(systemDir.getAbsolutePath());
		Vector<Version> versions = extractor.extractVersionBySystem(systemDir);
		
		for(Version version: versions) {
			System.out.println(version.getName());
			System.out.println(version.getSystemPath());
			System.out.println(version.getVersionPath());
			
			for(BadSmell bd: version.getBadSmells()) {
				System.out.println("	type: " + bd.getType());
				System.out.println("	name: " + bd.getName());
			}
		}
	}
	
	public VersionExtractor() {
		this.badSmellExtractor = new BadSmellExtractor();
	}
	
	public Vector<Version> extractVersionBySystem(File pSystemDir) {
		Vector<Version> versions = new Vector<Version>();
		
		for(File file: pSystemDir.listFiles()) {
			if(file.isDirectory()) {
				Version version = new Version();
				
				version.setSystemPath(pSystemDir.getAbsolutePath());
				version.setVersionPath(file.getAbsolutePath());
				version.setName(file.getName());
				version.setBadSmells(badSmellExtractor.extractBadSmellsByVersion(version.getSystemPath(), version.getName()));
				
				versions.add(version);
			}
		}
	
		return versions;
	}
}
