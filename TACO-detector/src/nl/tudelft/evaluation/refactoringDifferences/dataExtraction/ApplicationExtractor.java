package nl.tudelft.evaluation.refactoringDifferences.dataExtraction;

import java.io.File;
import java.util.Vector;

import nl.tudelft.evaluation.refactoringDifferences.beans.Application;
import nl.tudelft.evaluation.refactoringDifferences.beans.Commit;
import nl.tudelft.evaluation.refactoringDifferences.beans.Version;
import nl.tudelft.evaluation.refactoringDifferences.utilities.PathDefiner;

public class ApplicationExtractor {
	private VersionExtractor versionExtractor;
	private CommitExtractor commitExtractor;

	public static void main(String args[]) {
		ApplicationExtractor extractor = new ApplicationExtractor();

		for(Application system: extractor.extractSystems()) {
			System.out.println(system.getName());
			System.out.println(system.getSystemPath());

			for(Commit commit: system.getCommits())
				System.out.println("Commit: " +commit.getCommitMessage());

			for(Version version: system.getVersions()) 
				System.out.println("Version: " + version.getName());
		}
	}

	public ApplicationExtractor() {
		this.versionExtractor=new VersionExtractor();
		this.commitExtractor=new CommitExtractor();
	}

	public Vector<Application> extractSystems() {
		Vector<Application> systems=new Vector<Application>();

		File root = new File(PathDefiner.root);
		Vector<String> systemNames = this.getSystemNames();
		
		for(File file: root.listFiles()) {
			if(file.isDirectory()) {
				if(systemNames.contains(file.getName())) {
					Application system = new Application();

					system.setName(file.getName());
					system.setSystemPath(file.getAbsolutePath());
					system.setVersions(this.versionExtractor.extractVersionBySystem(file));
					system.setCommits(this.commitExtractor.extractCommits(file, ""));

					systems.add(system);
				}
			}
		}

		return systems;
	}

	private Vector<String> getSystemNames() {
		Vector<String> names = new Vector<String>();
		
		for(int i=0; i<20; i++) {
			names.add(this.getSystemName(i));
		}
		
		return names;
	}
	
	private String getSystemName(int pIndex) {

		if(pIndex == 0) {
			return "Apache Ant_OK";
		} else if(pIndex == 1) {
			return "Apache Cassandra_OK";
		} else if(pIndex == 2) {
			return "Apache Hive_OK";
		} else if(pIndex == 3) {
			return "Apache Incubating_OK";
		} else if(pIndex == 4) {
			return "Apache HBase_OK";
		} else if(pIndex == 5) {
			return "Apache Ivy_OK";
		} else if(pIndex == 6) {
			return "Apache Karaf_OK";
		} else if(pIndex == 7) {
			return "xerces_OK";
		} else if(pIndex == 8) {
			return "JFreeChart_OK";
		} else if(pIndex == 9) {
			return "JHotDraw_OK";
		} else if(pIndex == 10) {
			return "JEdit_OK";
		} else if(pIndex == 11) {
			return "hsqldb_OK";
		} else if(pIndex == 12) {
			return "Eclipse_OK";
		} else if(pIndex == 13) {
			return "aTunes_OK";
		} else if(pIndex == 14) {
			return "jVLT_OK";
		} else if(pIndex == 15) {
			return "jBoss_OK";
		} else if(pIndex == 16) {
			return "Elastic Search_OK";
		} else if(pIndex == 17) {
			return "Apache Nutch_OK";
		} else if(pIndex == 18) {
			return "Apache Pig_OK";
		} else if(pIndex == 19) {
			return "Apache Struts_OK";
		}

		return "";
	}

}
