package nl.tudelft.evaluation.refactoringDifferences.dataExtraction;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Pattern;

import nl.tudelft.evaluation.refactoringDifferences.beans.Commit;
import nl.tudelft.fileUtilities.FileUtility;

public class CommitExtractor {

	public Vector<Commit> extractCommits(File pSystemDir, String pSystem) {
		Vector<Commit> commits = new Vector<Commit>();
		if(pSystemDir.exists()) {

			for(File file: pSystemDir.listFiles()) {
				if(file.getName().startsWith("log")) {

					Pattern newLine=Pattern.compile("\n");
					Pattern comma=Pattern.compile(";");

					try {
						String[] lines = newLine.split(FileUtility.readFile(file.getAbsolutePath()));
						//String[] lines = newLine.split(MyFile.readStringFromFile(file.getAbsolutePath()));

						for (String line: lines) {
							String stringDate = comma.split(line)[2];

							SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
							Date date = this.convertDate(formatter.parse(stringDate));

							if(! this.isAlreadyInserted(commits, comma.split(line)[5])) {
								Commit commit = new Commit();		
								commit.addResource(comma.split(line)[0]);
								commit.setID(comma.split(line)[1]);
								commit.setCommitDate(date);
								commit.setCommitter(comma.split(line)[3]);
								commit.setCommitMessage(comma.split(line)[5]);
							
								commits.add(commit);

							} else {
								commits.get(commits.size()-1).addResource(comma.split(line)[0]);
							}
						}
					} catch (ArrayIndexOutOfBoundsException e) {
						e.printStackTrace();
					} catch (ParseException e) {
						
					} catch (IOException e) {
						e.printStackTrace();
					} 					

				}
			}
		}

		return commits;
	}

	private Date convertDate(Date pDate) {
		SimpleDateFormat sdfSource = new SimpleDateFormat("dd/MM/yy");
		String newDate = sdfSource.format(pDate);

		try {
			Date date = sdfSource.parse(newDate);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}
	private boolean isAlreadyInserted(Vector<Commit> pCommits, String pCommitMessage) {

		for(Commit commit: pCommits) {
			if(commit.getCommitMessage().equals(pCommitMessage))
				return true;
		}


		return false;
	}
}
