package nl.tudelft.luceneElements;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.LockObtainFailedException;

public class Indexer {

	//public static final String FILES_TO_INDEX_DIRECTORY = "/Users/fabiopalomba/Desktop/dir/it.unisa.featureExtraction/LDA/main";
	public static final String INDEX_DIRECTORY = "/Users/fabiopalomba/Desktop/indexDirectory";

	public static final String FIELD_PATH = "path";
	public static final String FIELD_CONTENTS = "contents";

	public static void main(String[] args) {
		Indexer indexer = new Indexer();
		try {
			indexer.indexAll("/Users/fabiopalomba/Desktop/dir/");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void indexAll2(String pPathToProjects) throws CorruptIndexException, LockObtainFailedException, IOException {
		File mainDir = new File(pPathToProjects);
		
		Indexer.createIndex(mainDir.getAbsolutePath()+"/FeatureExtractionFromAndroidReviews", "/Users/fabiopalomba/Desktop/indexDirectory/FeatureExtractionFromAndroidReviews2", true);
		
	}
	
	public void indexAll(String pPathToProjects) throws CorruptIndexException, LockObtainFailedException, IOException {
		File mainDir = new File(pPathToProjects);

		for(File project: mainDir.listFiles()) {
			if(! project.isHidden()) {
				if(project.isDirectory()) {

					File projectDir = new File(project.getAbsolutePath());

					File indexProjectDirectory = new File(INDEX_DIRECTORY+"/"+project.getName());

					if(!indexProjectDirectory.exists()) {
						indexProjectDirectory.mkdirs();
					}
					
					for(File packageDir: projectDir.listFiles()) {
						if(! packageDir.isHidden()) {
							if(packageDir.isDirectory()) {
								this.indexPackage(packageDir.getAbsolutePath(), indexProjectDirectory.getAbsolutePath());
							}
						}
					}
					Indexer.createIndex(project.getAbsolutePath(), indexProjectDirectory.getAbsolutePath(), true);
				}
			}
		}
	}

	public void indexProject(String pPathToProject) throws CorruptIndexException, LockObtainFailedException, IOException {
		File projectDir = new File(pPathToProject);

		File indexProjectDirectory = new File(INDEX_DIRECTORY+"/"+projectDir.getName());

		if(!indexProjectDirectory.exists()) {
			indexProjectDirectory.mkdirs();
		}


		for(File packageDir: projectDir.listFiles()) {
			if(! packageDir.isHidden()) {
				if(packageDir.isDirectory()) {
					this.indexPackage(packageDir.getAbsolutePath(), indexProjectDirectory.getAbsolutePath());
				}
			}
			Indexer.createIndex(projectDir.getAbsolutePath(), indexProjectDirectory.getAbsolutePath(), true);
		}
	}

	public void indexPackage(String pPathToPackage, String pIndexProjectPath) throws CorruptIndexException, LockObtainFailedException, IOException {
		File packageFolder = new File(pPathToPackage);

		File indexPackageDirectory = new File(pIndexProjectPath+"/"+packageFolder.getName());

		if(!indexPackageDirectory.exists()) {
			indexPackageDirectory.mkdirs();
		}

		for(File file: packageFolder.listFiles()) {
			if(! file.isHidden()) {

				if(file.isDirectory()) {
					if(file.getName().endsWith(".java")) {
						
						File indexClassDirectory = new File(indexPackageDirectory+"/"+file.getName());

						if(!indexClassDirectory.exists()) {
							indexClassDirectory.mkdirs();
						}
						Indexer.createIndex(file.getAbsolutePath(), indexClassDirectory.getAbsolutePath(), true);
						
					} else {
						this.indexClassFolder(file.getAbsolutePath(), indexPackageDirectory.getAbsolutePath());
						//Indexer.createIndex(file.getAbsolutePath(), indexClassDirectory.getAbsolutePath(), true);
					}
				} 
			}
		}

		Indexer.createIndex(packageFolder.getAbsolutePath(), indexPackageDirectory.getAbsolutePath(), true);

	}

	public void indexClassFolder(String pPathToClass, String pIndexPackagePath) throws CorruptIndexException, LockObtainFailedException, IOException {
		File classFolder = new File(pPathToClass);

		File indexClassDirectory = new File(pIndexPackagePath+"/"+classFolder.getName());

		if(!indexClassDirectory.exists()) {
			indexClassDirectory.mkdirs();
		}

		File indexClassFileDirectory = new File(pIndexPackagePath+"/"+classFolder.getName()+".java");

		if(!indexClassFileDirectory.exists()) {
			indexClassFileDirectory.mkdirs();
		}

		for(File file: classFolder.listFiles()) {
			if(! file.isHidden()) {
				if(file.isDirectory()) {
					if(file.getName().endsWith(".java")) {
						
						File indexMethodDirectory = new File(indexClassDirectory+"/"+file.getName());

						if(!indexMethodDirectory.exists()) {
							indexMethodDirectory.mkdirs();
						}
						Indexer.createIndex(file.getAbsolutePath(), indexMethodDirectory.getAbsolutePath(), true);
					}
					else {
						int blockNumber=0;

						File indexMethodBlocksDirectory = new File(indexClassDirectory.getAbsolutePath()+"/" + file.getName());

						if(!indexMethodBlocksDirectory.exists()) {
							indexMethodBlocksDirectory.mkdirs();
						}

						
						for(File block: file.listFiles()) {
							if(block.isDirectory()) {
								blockNumber++;
								this.indexMethodBlocks(block.getAbsolutePath(), indexMethodBlocksDirectory.getAbsolutePath(), blockNumber);
							}
						}
						
						Indexer.createIndex(file.getAbsolutePath(), indexMethodBlocksDirectory.getAbsolutePath(), true);
						
					}
				}
			}
		}

		Indexer.createIndex(classFolder.getAbsolutePath(), indexClassDirectory.getAbsolutePath(), true);
	}

	public void indexMethodBlocks(String pPathToMethodBlocks, String pIndexMethodDirectory, int pBlockNumber) throws CorruptIndexException, LockObtainFailedException, IOException {
		File indexMethodFileDirectory = new File(pIndexMethodDirectory+"/block_"+pBlockNumber);

		if(!indexMethodFileDirectory.exists()) {
			indexMethodFileDirectory.mkdirs();
		}

		Indexer.createIndex(pPathToMethodBlocks, indexMethodFileDirectory.getAbsolutePath(), true);
	}

	@SuppressWarnings("deprecation")
	public static void createIndex(String pFilesToIndex, String pIndexDirectory, boolean pRecreateIndexIfExists) throws CorruptIndexException, LockObtainFailedException, IOException {
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriter indexWriter = new IndexWriter(pIndexDirectory, analyzer, pRecreateIndexIfExists);
		File dir = new File(pFilesToIndex);
		File[] files = dir.listFiles();

		if(files == null)
			System.out.println(dir.getAbsolutePath());

		for (File file : files) {
			if(! file.isDirectory()) {
				Document document = new Document();

				String path = file.getCanonicalPath();
				document.add(new Field(FIELD_PATH, path, Field.Store.YES, Field.Index.UN_TOKENIZED));

				Reader reader = new FileReader(file);
				document.add(new Field(FIELD_CONTENTS, reader));

				indexWriter.addDocument(document);
			}
		}
		indexWriter.optimize();
		indexWriter.close();
	}
}
