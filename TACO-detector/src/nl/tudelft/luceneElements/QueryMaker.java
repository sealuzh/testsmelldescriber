package nl.tudelft.luceneElements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import nl.tudelft.similarityComputation.CosineSimilarity;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hit;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

@SuppressWarnings("deprecation")
public class QueryMaker {

	public static final String FIELD_PATH = "path";
	public static final String FIELD_CONTENTS = "contents";

	public static void main(String args[]) {
		
		/*try {
			QueryMaker.searchIndex("resource", "/Users/fabiopalomba/Desktop/indexDirectory/MOGA/it.unisa.moga.handlers/MOGAHandler");
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}*/
		
		
		try {
			Map<String,Double> firstClassTerms = QueryMaker.getTermsFrequency("/Users/fabiopalomba/Desktop/indexDirectory/FeatureExtractionFromAndroidReviews/it.unisa.featureExtractor.utilities/Utilities.java");
			Map<String,Double> secondClassTerms = QueryMaker.getTermsFrequency("/Users/fabiopalomba/Desktop/indexDirectory/FeatureExtractionFromAndroidReviews/it.unisa.featureExtractor.utilities/Utilities.java");
			
			CosineSimilarity cosineSimilarity = new CosineSimilarity();
			
			String[] doc1=new String[2];
			doc1[0] = "document_1";
			
			String[] doc2=new String[2];
			doc2[0] = "document_2";
			
			for(Entry<String,Double> entry: firstClassTerms.entrySet()) {
				doc1[1]+=entry.getKey() + " ";
			}
			
			for(Entry<String,Double> entry: secondClassTerms.entrySet()) {
				doc2[1]+=entry.getKey() + " ";
			}
			
			double similarity = cosineSimilarity.computeSimilarity(doc1, doc2);
			
			System.out.println("Similarity between the two vectors is: " + similarity);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Map<String,Double> getTermsFrequency(String pPath) throws CorruptIndexException, IOException {
		Map<String,Double> termsFrequency = new HashMap<String, Double>(); 
		ArrayList<String> terms = QueryMaker.getAllTerms(pPath);
		IndexReader ir = IndexReader.open(pPath); 

		for(String term: terms) {
			TermDocs termDocs = ir.termDocs(new Term(FIELD_CONTENTS, term));
			double count = 0.0;
			while (termDocs.next()) {
				count += termDocs.freq();
			}

			termsFrequency.put(term, count);
		}

		return termsFrequency;
	}

	public static ArrayList<String> getAllTerms(String pPath) throws CorruptIndexException, IOException {
		IndexReader indexReader = IndexReader.open(pPath); 
		TermEnum termEnum = indexReader.terms(); 
		ArrayList<String> terms = new ArrayList<String>();

		while (termEnum.next()) { 
			Term term = termEnum.term(); 
			terms.add(term.text()); 
		}
		termEnum.close(); 
		indexReader.close(); 

		return terms;
	}

	public static void searchIndex(String searchString, String pIndexDirectory) throws IOException, ParseException {
		System.out.println("Searching for '" + searchString + "'");
		Directory directory = FSDirectory.getDirectory(pIndexDirectory);
		IndexReader indexReader = IndexReader.open(directory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);

		Analyzer analyzer = new StandardAnalyzer();
		QueryParser queryParser = new QueryParser(FIELD_CONTENTS, analyzer);
		Query query = queryParser.parse(searchString);
		Hits hits = indexSearcher.search(query);

		System.out.println("Number of hits: " + hits.length());

		@SuppressWarnings("unchecked")
		Iterator<Hit> it = hits.iterator();
		while (it.hasNext()) {
			Hit hit = it.next();
			Document document = hit.getDocument();
			String path = document.get(FIELD_PATH);
			System.out.println("Hit: " + path);
		}
	}
}
